<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The main template file.
 * By default this template is used for all blog pages
 *
 * @package WordPress
 * @subpackage SocialFlow
 * @since SocialFlow 1.0
 */

get_theme_part( 'front-page' );