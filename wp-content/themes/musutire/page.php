<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="page-header">
			<h1 class="page-title"><?php the_title() ?></h1>
		</div>

		<div id="content-inn">
			<?php get_theme_part( 'content', 'page' ) ?>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>