<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage theme_name
 * @since theme_name 1.00
 */

get_header(); ?>

	<div class="page-header">
		<h1 class="page-title"><?php _e( 'Page not found', 'nrec' ) ?></h1>
	</div>

	<div id="content-inn">
		<?php get_theme_part( 'content', '404' ) ?>
	</div>

<?php get_footer(); ?>