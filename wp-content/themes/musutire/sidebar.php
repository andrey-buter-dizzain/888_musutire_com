<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Dizzain
 */

?>
<div id="secondary" class="widget-area blog-sidebar" role="complementary">

	<?php dynamic_sidebar( 'primary-sidebar' ); ?>
	
</div>