<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[!(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action( 'theme_after_open_body' ) ?>

	<nav id="primary-navigation" class="wrapper" role="navigation">
		<div class="navigation-inn">
			<?php wp_nav_menu( array( 
				'theme_location' => 'primary',
				'container'      => false,
				'link_before'    => '<span>',
				'link_after'     => '</span>',
				'depth'          => 1
			) ); ?>
			
		</div>
		<?php get_theme_part( 'section', 'header-cart-box' ); ?>

		<div class="account-navigation">
			<?php get_theme_part( 'section', 'login-button' ); ?>
			
			<?php if ( is_user_logged_in() AND function_exists( 'woocommerce_account_navigation' ) ): ?>
				<?php woocommerce_account_navigation() ?>
			<?php endif ?>
		</div>
	</nav>

	<?php get_theme_part( 'section', 'shop-message' ) ?>

	<div id="page">
		<header id="site-header">
			<div class="wrapper wrapper-header-footer">
				<a id="site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
					<img src="<?php echo THEME_URI .'/images/logo.svg' ?>" alt="<?php bloginfo( 'name' ); ?>" />
				</a>
				<div id="mobile-nav-toggler-wrapper">
					<div id="mobile-nav-toggler"><span></span></div>
				</div>
			</div>
		</header>

		<?php if ( function_exists( 'is_shop' ) AND is_shop() ): ?>
			<div id="shop-top-filters">
				<div class="wrapper">
					<?php wc_get_template( 'global/sidebar-top-filters.php' ); ?>
				</div>
			</div>
		<?php endif ?>

		<?php if ( function_exists( 'is_product' ) AND is_product() ): ?>
			<?php get_theme_part( 'section', 'single-product-filters' ) ?>
		<?php endif ?>


		<?php do_action( 'woocommerce_outside_notices' ); ?>

		<div id="primary">
			<?php 
				$class = 'wrapper';

				if ( is_front_page() OR ( function_exists( 'is_checkout' ) AND is_checkout() ) )
					$class = '';
			?>
			<div class="<?php echo $class ?>">
