<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage theme_name
 * @since theme_name 1.0
 */

get_header(); ?>

	<div class="content-area">
					
		<?php get_theme_part( 'content', '404' ) ?>
			
	</div>

<?php get_footer(); ?>