<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php post_class( 'col-md-4 col-xs-6' ); ?>>
	<div class="row">
		<div class="product-thumbnail col-xs-4">
			<?php woocommerce_template_loop_product_link_open() ?>

				<?php woocommerce_template_loop_product_thumbnail() ?>
			
			<?php woocommerce_template_loop_product_link_close() ?>
		</div>

		<div class="product-content-wrapper col-xs-8">
			<div class="product-header">
				<h3 class="product-title">
					<?php woocommerce_template_loop_product_link_open() ?>

						<?php the_title() ?>

					<?php woocommerce_template_loop_product_link_close() ?>
				</h3>
				<?php woocommerce_show_product_loop_sale_flash() ?>
			</div>
			<div class="mobile-product-price product-price">
				<?php woocommerce_template_loop_price() ?>
			</div>
			<div class="product-excerpt">
				<?php the_excerpt() ?>
			</div>

			<div class="product-meta">
				<div class="desktop-product-price product-price">
					<?php woocommerce_template_loop_price() ?>
				</div>
				<div class="product-link-more">
					<?php woocommerce_template_loop_product_link_open() ?>
						<?php _e( 'View', 'musutire' ) ?>
					<?php woocommerce_template_loop_product_link_close() ?>
				</div>
			</div>

		</div>
		<div class="product-quantity">
			<?php woocommerce_template_loop_add_to_cart() ?>
		</div>

	</div>
</li>
