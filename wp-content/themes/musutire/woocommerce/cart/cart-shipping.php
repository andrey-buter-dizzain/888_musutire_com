<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( is_checkout() ) {
	foreach ( (array) $available_methods as $key => $method ) {
		if ( $chosen_method != $method->id )
			continue;

		$available_methods = array( $key => $method );
		break;
	}
}

$install = module_wc_need_install_data( $chosen_method, $index );
$has_counter = $install['input_value'] ? 'has-counter' : '';

$row = is_cart() ? 'row' : '';
$col = '';

$shipping_value = array(
	'shipping' => $chosen_method,
	'install'  => $install['input_value']
);

?>
<div id="js-theme-cart-shipping" class="shipping <?php echo $row ?>">
	<?php if ( is_cart() ): ?>
		<h3 class="col-md-4"><?php /*echo wp_kses_post( $package_name );*/ ?><?php _e( 'Delivery Method' ); ?></h3>
		<?php $col = 'col-md-8' ?>
	<?php endif ?>
	
	<div data-title="<?php echo esc_attr( $package_name ); ?>" class="shipping-select-box <?php echo "$has_counter $col" ?>">
		
		<?php if ( 1 < count( $available_methods ) ) : ?>
			<select id="shipping_list" class="select2" name="shipping_list">
				<?php foreach ( $available_methods as $method ) : ?>
					<option value="<?php echo esc_attr( $method->id ); ?>" <?php selected( $method->id, $chosen_method ); ?>><?php echo wp_kses_post( wc_cart_totals_shipping_method_label( $method ) ); ?></option>
				<?php endforeach; ?>
			</select>

			<?php if ( $install['input_value'] ): // see inc/modules/woocommerce/inc/shipping/shipping.php ?>
				<div class="product-quantity">
					<div class="label"><?php _e( 'Need to install' ) ?></div>
					<div class="quantity-block">
						<?php woocommerce_quantity_input( $install, array() ); ?>
					</div>
					<div class="product-price">
						<?php $subtotal = absint( $install['subtotal'] ) + absint( $install['service'] ) ?>
						<span class="price"><?php _e( 'Service fee:' ) ?> <?php echo wc_price( $subtotal ) ?></span>
					</div>
				</div>
			<?php endif ?>

			<input class="shipping_method" data-index="<?php echo $index; ?>" type="hidden" name="shipping_method" id="shipping_method_<?php echo $index; ?>" value='<?php echo json_encode( $shipping_value ) ?>'>

		<?php elseif ( 1 === count( $available_methods ) ) :  ?> 
			<?php 
				$method = current( $available_methods ); 
				printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), wc_cart_totals_shipping_method_label( $method ) ); 
				do_action( 'woocommerce_after_shipping_rate', $method, $index ); 
			?> 
		<?php elseif ( ! WC()->customer->has_calculated_shipping() ) : ?>
			<?php echo wpautop( __( 'Shipping costs will be calculated once you have provided your address.', 'woocommerce' ) ); ?>
		<?php else : ?>
			<?php echo apply_filters( is_cart() ? 'woocommerce_cart_no_shipping_available_html' : 'woocommerce_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please double check your address, or contact us if you need any help.', 'woocommerce' ) ) ); ?>
		<?php endif; ?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		<?php /*if ( is_cart() && ! $index ) : ?>
			<?php woocommerce_shipping_calculator(); ?>
		<?php endif;*/ ?>
	</div>
</div>
