<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php /* ?>
<div class="top-cart-subtotal">
	<?php _e( 'Item Total:' ) ?> <?php wc_cart_totals_subtotal_html(); ?>
</div>
<?php */ ?>
<?php

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post" class="cart-form">

	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<div class="shop_table shop_table_responsive cart">

		<ul class="products list cart-items">
			<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
				$_product   = $cart_item['data'];
				$product_id = $cart_item['product_id'];

				if ( !( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) )
					continue;

				$product_permalink = $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '';
			?>
				<li class="product <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
					<div class="row">
						<div class="product-thumbnail col-md-4">
							<?php 
								$thumbnail = $_product->get_image();

								if ( ! $product_permalink )
									echo $thumbnail;
								 else 
									printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
							?>
						</div>
						<div class="product-content-wrapper col-md-8">
							<div class="product-header">
								<h3 class="product-title">
									<?php 
										if ( ! $product_permalink ) {
											echo $_product->get_title() . '&nbsp;';
										} else {
											echo sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() );
										} 
									?>
								</h3>
								<div class="product-price">
									<?php echo $_product->get_price_html() ?>
								</div>
							</div>
							<div class="product-excerpt">
								<?php echo wpautop( $_product->get_post_data()->post_content ) ?>
								<?php echo WC()->cart->get_item_data( $cart_item ); // Meta data ?>

								<?php if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ): // Backorder notification ?>
									<p class="backorder_notification"><?php echo esc_html__( 'Available on backorder', 'woocommerce' ) ?></p>
								<?php endif ?>
							</div>
							<div class="product-meta">
								<div class="product-link-more remove-link">
									<?php 
										echo sprintf(
											'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">%s</a>',
											esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
											__( 'Remove', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() ),
											__( 'Remove', 'woocommerce' )
										); ?>
								</div>
								<div class="product-quantity">
									<?php if ( $_product->is_sold_individually() ) : ?>
										<input type="hidden" name="cart[<?php echo $cart_item_key ?>][qty]" value="1" />
									<?php else: ?>
										<div class="quantity-block">
											<?php echo woocommerce_quantity_input( array(
												'input_name'  => "cart[{$cart_item_key}][qty]",
												'input_value' => $cart_item['quantity'],
												'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
												'min_value'   => 1
											), $_product, false ); ?>
										</div>
									<?php endif ?>
								</div>
								<div class="product-price">
									<span class="price"><?php _e('Subtotal') ?>: <?php echo WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ); ?></span>
								</div>
							</div>
						</div>
					</div>
				</li>
			<?php endforeach ?>
		</ul>
		<div class="actions">
			<input type="submit" class="button update_cart" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>">
			<?php do_action( 'woocommerce_cart_actions' ); ?>
			<?php wp_nonce_field( 'woocommerce-cart' ); ?>
		</div>
	</div>


	<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
