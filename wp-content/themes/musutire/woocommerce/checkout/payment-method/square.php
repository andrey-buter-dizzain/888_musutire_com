<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<fieldset>
	<?php
		$allowed = array(
		    'a' => array(
		        'href'  => array(),
		        'title' => array()
		    ),
		    'br' => array(),
		    'em' => array(),
		    'strong' => array(),
		    'span'	 => array(
		    	'class' => array(),
		    ),
		);
		if ( $gateway->description ) {
			echo apply_filters( 'wc_square_description', wpautop( wp_kses( $gateway->description, $allowed ) ) );
		}
	?>

	<div class="card-box">
		<div class="card-block first">
			<div class="fields">
				<p class="card-paying">Paying: <b><?php wc_cart_totals_order_total_html(); ?></b></p>

				<p class="form-row form-row-wide form-row-number">
					<label for="sq-card-number"><?php esc_html_e( 'Card Number', 'woocommerce-square' ); ?> <span class="required">*</span></label>
					<input id="sq-card-number" type="text" maxlength="20" autocomplete="off" placeholder="•••• •••• •••• ••••" name="<?php echo esc_attr( $gateway->id ); ?>-card-number" />
				</p>
				
				<p class="form-row form-row-half form-row-code first">
					<label for="sq-postal-code"><?php esc_html_e( 'Card Postal Code', 'woocommerce-square' ); ?> <span class="required">*</span></label>
					<input id="sq-postal-code" type="text" autocomplete="off" placeholder="<?php esc_attr_e( 'Card Postal Code', 'woocommerce-square' ); ?>" name="<?php echo esc_attr( $gateway->id ); ?>-card-postal-code" />
				</p>

				<p class="form-row form-row-half form-row-expire last">
					<label for="sq-expiration-date"><?php esc_html_e( 'Expiry (MM/YY)', 'woocommerce-square' ); ?> <span class="required">*</span></label>
					<input id="sq-expiration-date" type="text" autocomplete="off" placeholder="<?php esc_attr_e( 'MM / YY', 'woocommerce-square' ); ?>" name="<?php echo esc_attr( $gateway->id ); ?>-card-expiry" />
				</p>
			</div>
		</div>

		<div class="card-block last">
			<div class="fields">
				<p class="form-row form-row-last form-row-code">
					<label for="sq-cvv"><?php esc_html_e( 'Card Code', 'woocommerce-square' ); ?> <span class="required">*</span></label>
					<input id="sq-cvv" type="text" autocomplete="off" placeholder="<?php esc_attr_e( 'CVV', 'woocommerce-square' ); ?>" name="<?php echo esc_attr( $gateway->id ); ?>-card-cvv" />
					<small><?php _e( 'Last 3 or 4 digits' ) ?></small>
				</p>
			</div>
		</div>
	</div>
</fieldset>