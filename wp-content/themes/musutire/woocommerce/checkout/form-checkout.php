<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>
<form name="checkout" method="post" class="checkout woocommerce-checkout woocommerce-form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div id="order_review" class="woocommerce-checkout-review-order">
		<div class="woocommerce-checkout-review-order-inn">
			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
			<div class="wrapper">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>
			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>

		<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
			<div class="wrapper checkout-section" data-step="0">
				<div class="section-header row">
					<div class="section-title col-xs-12 col-sm-6">
						<h3><?php _e( 'Shipping' ) ?></h3>
						<div class="edit-btn" data-step-nav="0">Edit</div>
					</div>
					<div class="section-meta col-xs-12 col-sm-6">
						<p data-pattern="first_name,last_name,email,phone"></p>
						<p data-pattern="company,city,state,postcode,address_1"></p>
					</div>
				</div>
				<div class="section-content clearfix">
					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

					<div id="customer_details">
						
						<?php do_action( 'woocommerce_checkout_billing' ); ?>

						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
						
					</div>

					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="wrapper checkout-section section-payment" data-step="1">
			<div class="section-header row">
				<div class="section-title col-xs-12 col-sm-6">
					<h3><?php _e( 'Payment' ) ?></h3>
					<?php /* ?>
					<div class="edit-btn" data-step-nav="1">Edit</div>
					<?php */ ?>
				</div>
				<div class="section-meta col-xs-12 col-sm-6">
					
				</div>
			</div>
			<div class="section-content row">
				<?php woocommerce_checkout_payment() ?>
			</div>
		</div>
	</div>
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>