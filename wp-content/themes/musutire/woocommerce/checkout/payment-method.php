<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$custom = array(
	'square',
	'due_date',
	// 'partial'
);

$active = 'square' == $gateway_name ? 'active' : '';

?>
<li class="wc_payment_method payment_method_<?php echo $gateway->id; ?> <?php echo $active ?>">
	<input id="payment_method_<?php echo $gateway->id; ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />

	<label class="payment_method_label" for="payment_method_<?php echo $gateway->id; ?>">
		<?php echo $gateway->get_title(); ?> 
	</label>

	<div class="payment_method_icons">
		<?php echo $gateway->get_icon(); ?>
	</div>

	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<?php if ( in_array( $gateway_name, $custom ) ) : ?>
				<?php wc_get_template( "checkout/payment-method/{$gateway_name}.php", array( 'gateway' => $gateway ) ); ?>
			<?php else : ?>
				<?php $gateway->payment_fields(); ?>
			<?php endif; ?>
		</div>

	<?php endif; ?>
</li>
