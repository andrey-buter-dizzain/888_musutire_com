<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Dizzain
 */

if ( !is_shop() )
	return;

if ( !is_active_sidebar( 'shop-sidebar' ) )
	return;

?>
<div id="secondary" class="widget-area" role="complementary">

	<?php get_theme_part( 'section', 'mobile-filters-toggler' ) ?>

	<?php dynamic_sidebar( 'shop-sidebar' ); ?>

	<div class="mobile-filters-close">
		<button class="js-mobile-filters-toggler" type="button"><?php _e( 'Ok' ) ?></button>
	</div>
	
</div>