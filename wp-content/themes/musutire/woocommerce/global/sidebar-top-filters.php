<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Dizzain
 */

?>
<div class="shop-top-filters-sidebar">
	<?php dynamic_sidebar( 'shop-sidebar-2' ); ?>
</div>
	