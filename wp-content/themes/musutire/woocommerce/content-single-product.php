<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="page-header">
	<a class="link-back-to-shop" href="<?php echo module_wc_filters_get_url() ?>"><?php _e('Back to search results') ?></a>
</div>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class( 'row' ); ?>>

	<div class="entry-thumbnail col-sm-4 col-xs-12">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			//do_action( 'woocommerce_before_single_product_summary' );

			woocommerce_show_product_images();
		?>
	</div>

	<div class="entry-summary col-sm-8 col-xs-12">

		<?php //wc_print_notices(); ?>
		
		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */

			//woocommerce_show_product_sale_flash();
		?>
		<div class="product-header">

			<?php woocommerce_template_single_title(); ?>	

			<?php if ( $product->sale_price ): ?>
				<div class="in-sale"><span><?php _e( 'sale' ) ?></span></div>
			<?php endif ?>

			<?php if ( $product->is_in_stock() ): ?>
				<div class="in-stock"><span><?php _e( 'in stock' ) ?></span></div>
			<?php endif ?>

		</div>

		<div class="clearfix product-price">

			<?php woocommerce_template_single_price(); ?>

		</div>

		<?php $position = $product->get_attribute( 'Position' ); ?>

		<?php if ( $position ): ?>
			<div class="position-image <?php echo sanitize_title_with_dashes( $position ) ?>"></div>
		<?php endif ?>

		<div class="entry-content">
			<?php woocommerce_template_single_excerpt(); ?>

			<?php the_content(); ?>
		</div>

		<?php //do_action( 'woocommerce_single_product_summary' ); ?>

		<div class="product-quantity">
			<?php woocommerce_template_single_add_to_cart() ?>
		</div>

		<div class="additional-info">

			<h3><?php _e( 'Details' ) ?></h3>

			<?php $product->list_attributes(); ?>

		</div>

	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
