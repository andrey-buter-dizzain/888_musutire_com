<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if ( !$product->is_type( 'simple' ) )
	return;

if ( !$product->is_purchasable() )
	return;

if ( !$product->is_in_stock() )
	return;

if ( $product->is_sold_individually() )
	return;

$cart_qty  = get_product_count_in_cart( $product->id );
$stock_qty = $product->get_stock_quantity();

if ( $stock_qty == $cart_qty )
	return;

$url          = esc_url( $product->add_to_cart_url() );
$quantity     = esc_attr( isset( $quantity ) ? $quantity : 1 ) ;
$class        = esc_attr( isset( $class ) ? $class : 'button' );
$disabled_min = 1 == $quantity  ? 'disable' : '';
$disabled_max = 1 == $stock_qty ? 'disable' : '';

?>
	<div class="quantity-block">
		<div class="left-col">
			<div class="quantity">
				<span class="btn count-down <?php echo $disabled_min ?>"></span>
				<input type="text" readonly="" inputmode="numeric" pattern="[0-9]*" size="4" class="input-text qty text" value="1" name="quantity" max="<?php echo $stock_qty ?>" min="1" step="1">
				<span class="btn count-up <?php echo $disabled_max ?>"></span>
			</div>
		</div>
		<div class="right-col">
			<a rel="nofollow" href="<?php echo $url ?>" data-quantity="<?php echo $quantity ?>" data-product_id="<?php echo esc_attr( $product->id ) ?>" data-product_sku="<?php echo esc_attr( $product->get_sku() ) ?>" class="<?php echo $class ?> js-link-add-to-cart"><?php echo esc_html( $product->add_to_cart_text() ) ?></a>
		</div>
	</div>
