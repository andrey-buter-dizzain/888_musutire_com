<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$new_order = 'price';
$current   = '';

if ( isset( $_GET['filters'] ) AND false !== strpos( $_GET['filters'], 'order-price' ) ) {
	$filters = explode( '|', $_GET['filters'] );

	if ( in_array( 'order-price', $filters ) ) {
		$current   = 'price';
		$new_order = 'price-desc';
	}
	else
	if ( in_array( 'order-price-desc', $filters ) ) 
		$current = 'price-desc';
}


?>
<form class="woocommerce-ordering" method="get">
	<div id="js-wc-order-by-price" class="price-order <?php echo $orderby ?>" data-prev-order="<?php echo $current ?>" data-order="<?php echo $new_order ?>"><?php _e( 'By Price' ) ?> <span class="arrow"></span></div>
	<select name="orderby" class="orderby">
		<option value=""><?php _e( 'No Order' ) ?></option>
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php
		// Keep query string vars intact
		foreach ( $_GET as $key => $val ) {
			if ( 'orderby' === $key || 'submit' === $key ) {
				continue;
			}
			if ( is_array( $val ) ) {
				foreach( $val as $innerVal ) {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
				}
			} else {
				echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
			}
		}
	?>
</form>
