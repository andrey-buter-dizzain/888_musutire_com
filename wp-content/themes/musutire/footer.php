<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying Socialflow theme footer.
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

$contacts = get_theme_option( 'footer-contacts' );

?>
		</div>
	</div>

	<?php if ( !is_front_page() ): ?>
		<footer id="site-footer">
			<div class="wrapper wrapper-header-footer">
				<div class="row row-inline"><?php 
					?><div class="col-md-6 footer-logo">
						<?php theme_option( 'footer-logo-text' ) ?>
					</div><?php  
					?><div class="col-md-6 footer-meta">
						<div class="copyright">
							<?php theme_option( 'copyright' ) ?>
						</div>

						<?php echo wpautop( $contacts ) ?>
					</div><?php  
				?></div>
			</div>
		</footer>
	<?php endif ?>

</div>
<?php wp_footer(); ?>

</body>
</html>