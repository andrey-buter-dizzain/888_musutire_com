<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Checkout
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

if ( !function_exists( 'is_checkout' ) )
	return;

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div id="content-inn" class="js-checkout-steps woocommerce-checkout-main">
			<div class="site-content">
				<?php get_theme_part( 'content', 'page' ) ?>
			</div>
		</div>

	<?php endwhile; ?>
		
<?php get_footer(); ?>