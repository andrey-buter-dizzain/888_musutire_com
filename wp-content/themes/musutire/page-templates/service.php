<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Service
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

$tabs = get_post_meta( get_the_ID(), 'page_service_tabs', true );

$phone = get_theme_option( 'phone' );

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="page-header">
			<h1 class="page-title"><?php the_title() ?></h1>

			<?php if ( $phone ): ?>
				<div class="page-phone"><?php _e( 'Call Us:' ) ?> <?php echo $phone ?></div>
			<?php endif ?>
		</div>

		<div id="content-inn" class="with-sidebar js-section-tabs"><?php  
			?><div class="sidebar sidebar-left">
				<ul class="sidebar-navigation block-nav-tabs">
					<?php foreach ( $tabs as $key => $tab ): 
						$target = "tab-{$key}";
						$tabs[ $key ]['target'] = $target;
					?>
						<li class="js-tab-nav tab-nav" data-target="#<?php echo $target ?>">
							<span><?php echo $tab['title'] ?></span>
						</li>
					<?php endforeach ?>
				</ul>
			</div><?php 

			?><div class="site-content block-tabs">
				<?php foreach ( $tabs as $tab ): ?>
					<div id="<?php echo $tab['target'] ?>" class="js-tab-item tab-item">
						<h4><?php echo $tab['title'] ?></h4>
						<div class="entry-content">
							<?php echo wpautop( $tab['text'] ) ?>
						</div>

						<?php if ( $tab['price'] ): ?>
							<div class="entry-price">
								<div class="title"><?php _e( 'Price' ) ?></div>
								<?php echo $tab['price'] ?>
							</div>
						<?php endif ?>
					</div>
				<?php endforeach ?>
			</div><?php  
		?></div>

	<?php endwhile; ?>
		

<?php get_footer(); ?>