<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Cart
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

if ( !function_exists( 'is_checkout' ) )
	return;

$class = is_checkout() ? 'woocommerce-checkout js-checkout-steps' : '';

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="page-header">
			<h1 class="page-title"><?php the_title() ?></h1>

			<a class="link-back-to-shop" href="<?php echo wc_get_page_permalink( 'shop' ) ?>"><?php _e( 'Back to Catalog' ) ?></a>
		</div>

		<div id="content-inn" class="<?php echo $class ?>">
			<div class="site-content">
				<?php get_theme_part( 'content', 'page' ) ?>
			</div>
		</div>

	<?php endwhile; ?>
		

<?php get_footer(); ?>