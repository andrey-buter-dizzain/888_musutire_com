<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The front page template.
 *
 *
 * @package Wordpress
 * @subpackage Dizzain
 */

$img = get_theme_image_src( get_post_thumbnail_id(), 'full' );

get_header(); ?>

	<?php if ( have_posts() ): ?>
		<?php while( have_posts() ): the_post(); ?>

			<div id="content" style="background-image:url(<?php echo $img ?>)">
				<div class="content-inn">
					<div class="content-inn-inn">
						<?php get_theme_part( 'section/front', 'filters' ) ?>

						<?php get_theme_part( 'section/front', 'social' ) ?>
					</div>
				</div>
			</div>

		<?php endwhile ?>
	<?php endif ?>

	<?php get_theme_part( 'section/front', 'bottom-links' ) ?>

<?php get_footer(); ?>