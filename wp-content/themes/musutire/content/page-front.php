<?php 

$img = get_theme_image_src( get_post_thumbnail_id(), 'full' );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry-front' ); ?>>
	<div class="entry-header">
		<div class="wrapper">
			<h1 class="entry-title"><?php the_title() ?></h1>
		</div>
	</div>
	<div class="entry-thumbnail" style="background-image:url(<?php echo $img ?>)"></div>

</article>