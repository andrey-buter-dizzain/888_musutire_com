<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

?>
<div class="hentry hentry-404">
	<div class="entry-content">
		<p><br><?php _e( 'Sorry, but the requested page can’t be displayed. Unfortunately it does happen sometimes because of outdated links or mistypes.', 'nrec' ) ?></p>
	</div>
</div>