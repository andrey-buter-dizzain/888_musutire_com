<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The default template for displaying single post content
 *
 * @package WordPress
 * @subpackage Dizzain_Blog
 * @since Dizzain_Blog 1.0
 */

$user_email = get_the_author_meta( 'user_email' );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'hentry-post-single' ); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
			<?php the_title(); ?>
		</h1>

		<div class="entry-meta">
			<div class="entry-avatar">
				<?php echo get_avatar( $user_email, 66 ); ?>
			</div>

			<div>
				<time class="entry-date" datetime="<?php the_time( 'Y-m-d\TH:i:s' ) ?>"><?php the_time( 'F j, Y' ) ?></time>
				<br>by <span class="author vcard"><?php the_author() ?></span>
			</div>
			<div class="counters">
				<?php if ( function_exists( 'the_views' ) ): ?>
					<span class="views">
						<?php the_views() ?>
					</span>
				<?php endif ?>

				<?php /* ?>
				<span class="comments"><?php comments_number( '0', '1', '%' ); ?></span>
				<?php */ ?>
			</div>
		</div>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>

</article>