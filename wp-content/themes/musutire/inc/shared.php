<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * NUK theme shared utilies
 * Initialize theme options amd meta boxes
 *
 * @package WordPress
 * @subpackage NUK
 */

class Inc_Shared_Section extends Inc_Load_Section
{
	public static $section = 'shared';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'utils',
		'option-tree-loader',
		'options',
		'meta-boxes',
	);
}
Inc_Shared_Section::load();