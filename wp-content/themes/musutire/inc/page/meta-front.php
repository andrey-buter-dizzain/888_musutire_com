<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_front_page_meta' );
function populate_front_page_meta( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_front_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Page Meta', 'musutire-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'id'       => 'front-bottom-links',
				'label'    => __( 'Bottom links', 'musutire-admin' ),
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Select page', 'musutire-admin' ),
						'id'    => 'page',
						'type'  => 'page-select',
					),
					array(
						'label' => __( 'Icon', 'musutire-admin' ),
						'id'    => 'icon',
						'type'  => 'select',
						'choices'     => array( 
							array(
								'value' => '',
								'label' => __( '-- Choose One --', 'musutire-admin' ),
							),
							array(
								'value' => 'tires',
								'label' => __( 'Tires', 'musutire-admin' ),
							),
							array(
								'value' => 'mobile-service',
								'label' => __( 'Mobile Service', 'musutire-admin' ),
							),
							array(
								'value' => 'our-service',
								'label' => __( 'Our service', 'musutire-admin' ),
							)
						)
					),
				),
			)
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}