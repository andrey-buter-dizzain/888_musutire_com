<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_service_meta_boxes' );
function populate_service_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_service_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Page Meta', 'musutire-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Tabs', 'musutire-admin' ),
				'id'      => "{$prefix}tabs",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Text', 'musutire-admin' ),
						'id'    => 'text',
						'type'  => 'textarea',
						'rows'  => 6
					),
					array(
						'label' => __( 'Price', 'musutire-admin' ),
						'id'    => 'price',
						'type'  => 'text',
					),
				),
			),
		),
		'only_on' => array(
			'function' => 'is_template_service'
		)
	);

	return $meta_boxes;
}

function rw_is_template_service() {
	return rw_is_custom_page_template( 'service' );
}