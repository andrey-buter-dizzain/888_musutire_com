<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Include Socialflow Styles and Scripts
 *
 * @package WordPress
 * @subpackage socialflow
 */

/**
 * Plugin Name: Enqueue jQuery in Footer
 * Version:     0.0.1
 * Plugin URI:  http://wpgrafie.de/836/
 * Description: Prints jQuery in footer on front-end.
 * Author:      Dominik Schilling
 * Author URI:  http://wpgrafie.de/
 */
add_action( 'init', 'ds_enqueue_jquery_in_footer' );
function ds_enqueue_jquery_in_footer() {
	if ( is_admin() )
		return;

	wp_deregister_script( 'jquery' );
	wp_deregister_script( 'jquery-migrate' );



	// wp_enqueue_script( 'underscore' );

	// Load the copy of jQuery that comes with WordPress  
	// The last parameter set to TRUE states that it should be loaded  
	// in the footer.  
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js', false, '1.11.0', true );
	wp_enqueue_script( 'jquery-migrate', '/wp-includes/js/jquery/jquery-migrate.min.js', false, '1.2.1', true );
}

/**
 * Register and enqueue some theme scripts and styles
 *
 * @todo enqueue some scripts only if they are required
 */
add_action( 'wp_enqueue_scripts', 'theme_scripts_styles', 100 );
function theme_scripts_styles() {

	//wp_dequeue_script( 'wc-cart' );

	$min = is_localhost() ? '' : '.min';
	$min = '';

	$version = is_staging( 'dizzain.net' ) ? time() : 1;

	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-widget' );

	// custom theme js
	wp_enqueue_script( 'common-js', THEME_URI ."/js/common{$min}.js", array( 'jquery', 'jquery-ui-core', 'jquery-ui-widget' ), $version, true );

	// Load the Internet Explorer 9 specific stylesheet.
	wp_enqueue_script(  'html5', THEME_URI .'/js/html5.js', array( 'jquery' ), '1', false );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	if ( is_localhost() ) {
		// wp_enqueue_script( 'live-reload', '//192.168.1.100:35729/livereload.js', false, '1', false );
		wp_enqueue_script( 'live-reload', '//localhost:35729/livereload.js', false, '1', false );
	}

	if ( function_exists( 'is_cart' ) AND is_cart() ) {
	    $assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
	    wp_register_script( 'select2', $assets_path . 'js/select2/select2.js', array( 'jquery' ), '', true );
	    wp_enqueue_script( 'select2' );
	    wp_enqueue_style( 'select2', $assets_path . 'css/select2.css' );
	}	


	// Include theme compiled style
	wp_enqueue_style( 'google-fonts', "https://fonts.googleapis.com/css?family=Titillium+Web:300,300i,400,400i,600,600i,700,700i" );

	wp_enqueue_style( 'theme-styles', THEME_URI . "/css/styles{$min}.css", array(), $version );
};

add_action( 'admin_enqueue_scripts', 'theme_admin_enqueue_scripts' );
function theme_admin_enqueue_scripts() {
	$min = is_localhost() ? '' : '.min';

	$version = is_staging( 'dizzain.us' ) ? time() : 1;

	wp_enqueue_script( 'admin-common-js', THEME_URI ."/js/admin/common{$min}.js", array( 'jquery' ), $version, true );

	wp_localize_script( 'admin-common-js', 'themeAdminData', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' )
	));
}

/**
 * Output some theme scripts
 */
add_action( 'wp_head', 'theme_header_scripts' );
function theme_header_scripts() {
	$path = THEME_URI . '/images/favicon';
?>
	<link href="<?php echo $path ?>/favicon_32.ico" rel="shortcut icon">
	<link rel="apple-touch-icon" sizes="32x32" href="<?php echo $path ?>/favicon_32.png" />
	<link rel="apple-touch-icon" sizes="64x64" href="<?php echo $path ?>/favicon_64.png" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $path ?>/favicon_180.png" />

	<?php 
		// disable phone number linking in Mobile Safari
		// http://stackoverflow.com/questions/226131/how-to-disable-phone-number-linking-in-mobile-safari
	?>
	<meta name="format-detection" content="telephone=no">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P97RJKF');</script>
	<!-- End Google Tag Manager -->
<?php 
}

// add_action( 'wp_footer', 'theme_footer_scripts' );
function theme_footer_scripts() {}

// used header.php
add_action( 'theme_after_open_body', 'theme_body_scripts' );
function theme_body_scripts() {
	if ( is_localhost() )
		return;
?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P97RJKF"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
<?php 
}