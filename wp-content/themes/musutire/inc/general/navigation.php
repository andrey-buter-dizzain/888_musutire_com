<?php 

/**
 * 
 */
class Theme_Nav_Menu
{
	protected $dots = array();

	function __construct()
	{
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), 10, 2 );
		add_filter( 'wp_nav_menu_items', array( $this, 'wp_nav_menu_primary_items' ), 10, 2 );
	}

	function wp_nav_menu_objects( $sorted_menu_items, $args )
	{
		if ( 'primary' != $args->theme_location )
			return $sorted_menu_items;

		$sorted_menu_items = $this->add_css_class_to_objects_for_nav_height( $sorted_menu_items );
		$sorted_menu_items = $this->handle_nav_for_menu_item_dots( $sorted_menu_items );

		return $sorted_menu_items;
	}

	function wp_nav_menu_primary_items( $items, $args ) 
	{
		if ( 'primary' != $args->theme_location )
			return $items;

		$items = $this->add_logo( $items );
		// $items = $this->set_nav_sections( $items );

		return $items;
	}

	protected function add_logo( $items )
	{
		// $logo = '<img width="66" height="24" src="'. THEME_URI . '/images/nrec_logo.svg" />';
		$logo = '<span class="site-logo"></span>';

		$items = str_replace( '{{logo}}', $logo, $items );

		return $items;
	}

	protected function add_css_class_to_objects_for_nav_height( $sorted_menu_items ) {
		$i = -1;

		foreach ( $sorted_menu_items as $item ) {
			if ( $item->menu_item_parent )
				continue;

			$i++;
		}

		foreach ( $sorted_menu_items as $key => $item ) {
			if ( $item->menu_item_parent )
				continue;

			$sorted_menu_items[ $key ]->classes[] = "menu-item-count-{$i}";
		}

		return $sorted_menu_items;
	}

	protected function handle_nav_for_menu_item_dots( $sorted_menu_items ) {
		foreach ( $sorted_menu_items as $key => $item ) {
			if ( $item->menu_item_parent )
				continue;

			$values = trim( $item->post_content );
			$values = array_filter( explode( "\n", $values ) );
			$values = array_map( function( $val ) {
				return trim( str_replace( "\r", '', $val ) );
			}, $values );

			if ( !$values )
				$values = array( strip_tags( $item->title ) );

			$this->dots[ $item->ID ] = $values;
		}

		return $sorted_menu_items;
	}

	protected function set_nav_sections( $items ) 
	{
		if ( !preg_match_all( '/<li[^>]+>.+<\/li>/', $items, $mathes ) )
			return $items;

		$items = $mathes[0];

		if ( !$this->dots )
			return implode( '', $items );

		$items = array_map( function( $item ) {
			$is_active = false;

			preg_match( '/menu-item-([0-9]+)/', $item, $matches );

			$item_id = absint( $matches[1] );

			if ( false !== strpos( $item, 'current' ) )
				$is_active = true;

			$replace = $this->generate_nav_sections( $item_id, $is_active );

			return str_replace( '{{nav}}', $replace, $item );
		}, $items );

		return implode( '', $items );
	}

	protected function generate_nav_sections( $item_id, $is_active = false ) 
	{
		$dots = $this->dots[ $item_id ];

		$output = '';

		foreach ( $dots as $key => $title ) {
			if ( $is_active )
				$inner = "<span class=\"dot-title\" data-scroll-nav=\"$i\">$title</span>";
			else
				$inner = "<a class=\"dot-title\" href=\"#$key\">$title</a>";

			$output .= "<li data-scroll-nav=\"$key\">$inner</li>";
		}

		return "<ul class=\"sections-nav\">$output</ul>";
	}
}

new Theme_Nav_Menu;