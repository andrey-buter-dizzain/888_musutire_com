<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Populate product meta boxes
 *
 * @package WordPress
 * @subpackage musutire-admin
 */

add_filter( 'populate_theme_options', 'populate_theme_general_options' );
function populate_theme_general_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'General', 'musutire-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'phone',
		'label'   => __( 'Phone', 'musutire-admin' ),
		'type'    => 'text',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'social-links',
		'label'   => __( 'Social Links', 'musutire-admin' ),
		'type'    => 'social-links',
		'section' => $id,
	);

	

	return $settings;
}

add_filter( 'ot_type_social_links_defaults', 'theme_ot_empty_social_links' );
function theme_ot_empty_social_links() {
	return array();
}

add_filter( 'ot_social_links_settings', 'theme_ot_social_links_settings', 10, 2 );
function theme_ot_social_links_settings( $fields, $id ) {
	$fields[] = array(
		'id'        => 'icon',
		'label'     => 'Icon CSS Key',
		'desc'      => '',
		'type'      => 'text'
	);

	return $fields;
}

add_filter( 'populate_theme_options', 'populate_theme_footer_options' );
function populate_theme_footer_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Footer', 'musutire-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'footer-logo-text',
		'label'   => __( 'Logo Text', 'musutire-admin' ),
		'type'    => 'text',
		'wpmu'    => true,
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'copyright',
		'label'   => __( 'Copyright &copy;', 'musutire-admin' ),
		'type'    => 'text',
		'wpmu'    => true,
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'footer-contacts',
		'label'   => __( 'Contacts', 'musutire-admin' ),
		'type'    => 'textarea-simple',
		'rows'    => 3,
		'wpmu'    => true,
		'section' => $id,
	);

	return $settings;
}


add_filter( 'populate_theme_options', 'populate_theme_ids_options', 100 );
function populate_theme_ids_options( $settings = array() ) {
	$id = 'ids';

	$settings['sections'][] = array(
		'title' => __('ID\'s', 'musutire-admin'),
		'id'    => $id
	);

	$settings['settings'][] = array(
		'id'       => 'profile_cat_managment_id',
		'label'    => __( 'Profile Category for Managment Slider', 'musutire-admin' ),
		'type'     => 'taxonomy-select',
		'taxonomy' => 'profile_cat',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'profile_cat_letters_id',
		'label'    => __( 'Profile Category Letters Page', 'musutire-admin' ),
		'type'     => 'taxonomy-select',
		'taxonomy' => 'profile_cat',
		'wpmu'     => true,
		'section'  => $id,
	);

	return $settings;
}