<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Page extends Inc_Load_Section
{
	public static $section = 'page';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'meta-front',
		'meta-service',
		// 'meta-downloads',
		// 'meta-governance',
		// 'meta-portfolio',
		// 'meta-overview',
		// 'meta-responsibility',
	);
}
Inc_Page::load();