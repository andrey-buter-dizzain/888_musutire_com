<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Remove default styles
 */
add_action( 'wp_enqueue_scripts', 'module_wc_grid_wp_enqueue_scripts', 50 );
function module_wc_grid_wp_enqueue_scripts() {
	wp_dequeue_style( 'grid-list-layout' );
	wp_dequeue_style( 'grid-list-button' );
	wp_dequeue_style( 'dashicons' );
}