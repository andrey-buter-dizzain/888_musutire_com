<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); ?>

<div id="repairshopr_product_data" class="panel woocommerce_options_panel" ng-app="repairShoprApp">
	<style>
		.woocommerce_options_panel p.upc_code_field {
			/*padding-right: 65px !important;*/
			position: relative;
		}
		.woocommerce_options_panel p.upc_code_field input[type="text"] {
			width: 80px;
			margin-bottom: 10px;
		}
		.woocommerce_options_panel #upc_code {
			margin-right: 5px;
		}
		.woocommerce_options_panel p.upc_code_field .form-field-actions {
			white-space: nowrap;
		}
		.woocommerce_options_panel p.upc_code_field .button {
			position: relative;
			top: -1px;
		}
		.woocommerce_options_panel p.upc_code_field .spinner {
			float: none;
			vertical-align: top;
			margin-right: 0;
			margin-left: 5px;
		}
		.woocommerce_options_panel .custom-field {
			padding: 2px 5px;
			border: 1px solid #ddd;
			height: 22px;
			max-width: 300px;
			width: 80%;
			display: inline-block;
			vertical-align: middle;
			font-size: 14px;
		}
	</style>
	<price-handler></price-handler>
</div>