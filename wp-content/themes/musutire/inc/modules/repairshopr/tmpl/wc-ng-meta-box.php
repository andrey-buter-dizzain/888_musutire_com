<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$upc_code_key = RepairShopr_Product::$upc_code_key;

?>

<div class="options_group">
	<p class="form-field _regular_price_field">
		<label for="_regular_price"><?php echo __( 'Regular price', 'woocommerce' ) . ' ('. get_woocommerce_currency_symbol() .')' ?></label>
		<span class="custom-field">{{$ctrl.data.price}}</span>
	</p>
	<p class="form-field _sale_price_field" ng-show="!!$ctrl.data.salePrice">
		<label for="_sale_price"><?php echo __( 'Sale price', 'woocommerce' ) . ' ('. get_woocommerce_currency_symbol() .')' ?></label>
		<span class="custom-field">{{$ctrl.data.salePrice}}</span>
	</p>
	<p class="form-field _sale_price_field">
		<label for="_sale_price"><?php echo __( 'Stock quantity', 'woocommerce' ) ?></label>
		<span class="custom-field">{{$ctrl.data.stock}}</span>
	</p>
</div>
<div class="options_group upc-code-block">
	<p class="form-field upc_code_field">
		<label for="<?php echo $upc_code_key ?>"><?php _e( 'UPC Code', 'musutire-admin' ) ?></label>
		<input type="text" value="{{$ctrl.data.upcCode}}" id="<?php echo $upc_code_key ?>" name="<?php echo $upc_code_key ?>" class="short" ng-model="$ctrl.data.upcCode">
		<span class="form-field-actions">
			<button type="button" class="button add_attribute" ng-click="$ctrl.getPrice()" ng-disabled="!$ctrl.data.upcCode">
				<?php _e( 'Update data', 'repairshopr-admin' ); ?>
			</button>
			<span class="spinner" ng-class="$ctrl.showSpinner ? 'is-active' : ''"></span>
		</span>
	</p>
</div>