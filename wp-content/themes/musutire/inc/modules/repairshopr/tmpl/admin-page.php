<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

?>
<style>
	.repairshopr-wrapper .code {
		margin-top: 20px;
		font-family: Consolas,Monaco,monospace;
		unicode-bidi: embed;
		background: rgba(0, 0, 0, 0.07) none repeat scroll 0 0;
		font-size: 13px;
		padding: 3px 15px 2px;
	}
</style>
<div class="wrap repairshopr-wrapper">
	<h1><?php _e( 'RepairShopr Logs' ) ?></h1>

	<div class="code">
		<?php if ( $data ): ?>
			<?php echo wpautop( $data ) ?>
		<?php else: ?>
			<p><?php _( 'Logs not found.' ) ?></p>
		<?php endif ?>
	</div>
</div>