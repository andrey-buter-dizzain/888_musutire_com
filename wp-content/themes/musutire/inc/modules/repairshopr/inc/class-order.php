<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * При создании order в repairshopr создается invoice с данными самого сервиса.
 * Т.е. на сервис передаются только upc_code продукта, количество, и возможная скидка.
 * Цена на сервисе формируется соответственно данным с самого сервиса.
 * Поэтому, могут быть некотрые расхожения по ценам.
 * Но, вроде, это оговорено с клиентом
 */
class RepairShopr_Order
{
	protected $invoice_id;

	protected $customer_id;

	protected $order_id;

	protected $error = false;

	protected $delimiter = '->';

	protected $meta = array();

	protected $meta_key = 'repairshopr_invoice';

	function __construct()
	{
		// add_action( 'woocommerce_checkout_order_processed', array( $this, 'test' ) );

		add_filter( 'woocommerce_payment_successful_result', array( $this, 'woocommerce_payment_successful_result' ), 10, 2 );

		add_filter( 'woocommerce_checkout_no_payment_needed_redirect', array( $this, 'woocommerce_checkout_no_payment_needed_redirect' ), 10, 2 );
	}

	function test( $order_id )
	{
		$this->woocommerce_payment_successful_result( null, $order_id );
	}

	/**
	 * Filter if payment not needed
	 */
	function woocommerce_checkout_no_payment_needed_redirect( $return_url, $order )
	{
		$this->order = $order;

		$this->set_invoice();

		return $return_url;
	}

	/**
	 * Filter on payment success
	 */
	function woocommerce_payment_successful_result( $result, $order_id )
	{
		$this->order = wc_get_order( $order_id );

		$this->set_invoice();

		return $result;
	}

	protected function set_invoice()
	{
		// duplicate request to create invoice
		if ( get_post_meta( $this->order->id, $this->meta_key, true ) )
			return;

		$this->set_cutsomer();

		$this->post_invoice();

		if ( $this->is_error() )
			return;

		$discount = $this->get_discount();

		foreach ( $this->order->get_items() as $product ) {
			$upc_code = get_post_meta( $product['product_id'], RepairShopr_Product::$upc_code_key, true );
			$item_id  = $this->post_product( $upc_code, $product['qty'], $discount );

			// $this->update_product( $item_id );

			if ( $this->is_error() )
				break;

			$this->add_meta( $product['product_id'], $item_id );
			
			// add action see in class-product.php
			do_action( 'repairshopr_invoice_item_create', $product['product_id'], $upc_code );
		}

		$this->post_shipping();

		$this->save_meta();
	}

	protected function post_shipping()
	{
		// set by woocommerce 
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );

		$packages       = WC()->shipping->get_packages();

		if ( !$chosen_methods )
			return;

		foreach ( $packages as $i => $package ) {
			if ( empty( $chosen_methods[ $i ] ) )
				continue;

			$chosen_method = explode( ':', $chosen_methods[ $i ] );

			if ( 'need_to_install' != $chosen_method[0] )
				continue;

			if ( 0 == sizeof( $package['rates'] ) )
				continue;

			$meta = $package['rates'][ $chosen_methods[ $i ] ]->get_meta_data();

			foreach ( $meta['upc_codes'] as $key => $data ) {
				if ( !$data['code'] )
					continue;

				$qty = 1;

				if ( 'per_one' == $key )
					$qty = WC()->session->get( Module_WC_Shipping_Install_Wheels::SESSION_KEY, 1 );

				if ( !$qty )
					$qty = $data['qty'];

				$item_id = $this->post_product( $data['code'], $qty );

				if ( $this->is_error() )
					return;
			}
		}
	}

	/**
	 * Надо получить размер скидки купона
	 * - см Module_Roles_WC_User_Coupon 
	 * - надо получить весь купон при созданиии класса
	 * - и потом получить определенный атрибут
	 *
	 * + Надо провекрить вариант, когда
	 * админ заполняет заказ за клиента!!!!
	 */
	protected function get_discount() 
	{
		$user_id = RepairShopr_User::get_instance()->get_user_id();

		$default = 0;

		if ( !$user_id )
			return $default;

		if ( !class_exists( 'Module_Roles_WC_User_Coupon' ) )
			return $default;

		$coupon = new Module_Roles_WC_User_Coupon( $user_id );

		return $coupon->get_amount();
	}

	protected function post_invoice()
	{
		$data = array(
			'customer_id' => $this->customer_id,
		);

		$response = RapireShopr_Api::get_instance()
						->post( 'invoices', $data )
						->response();

		$this->log( 'invoice', $response );

		if ( is_wp_error( $response ) ) {
			$this->set_error();
			return;
		}

		$this->invoice_id = $response->invoice->id;
	}

	protected function set_cutsomer()
	{
		$this->customer_id = RepairShopr_User::get_instance()->get_customer_id();
	}

	protected function post_product( $upc_code, $quantity, $discount = 0 )
	{
		$data = array(
			'product_id' => $upc_code,
			'quantity'   => $quantity,
		);

		if ( $discount ) 
			$data['line_discount_percent'] = absint( $discount );

		$response = RapireShopr_Api::get_instance()
						->post( "invoices/{$this->invoice_id}/line_items", $data )
						->response();

		$this->log( 'line_item', $response );

		if ( is_wp_error( $response ) ) {
			$this->set_error();
			return;
		}

		return $response->line_item->id;
	}


	// protected function update_product( $item_id )
	// {
	// 	$data = array(
	// 		'quantity' => 10
	// 		// 'discount_percent' => 10
	// 	);

	// 	var_dump($item_id);
	// 	var_dump("invoices/{$this->invoice_id}/line_items/{$item_id}");

	// 	$response = RapireShopr_Api::get_instance()
	// 				->post( "invoices/{$this->invoice_id}/line_items/{$item_id}", $data )
	// 				->response();

	// 	var_dump($responce);
	// 	die;

	// 	if ( is_wp_error( $response ) ) {
	// 		$this->set_error();
	// 		return;
	// 	}
	// }

	protected function log( $type, $response )
	{
		$msg = "OrderID: {$this->order->id} {$this->delimiter} {$type} create: ";

		if ( is_wp_error( $response ) ) {
			$msg .= "ERROR {$this->delimiter} ". $response->get_error_message();
		} else {
			$id = $response->$type->id;

			$msg .= "SUCCESS {$this->delimiter} id: $id";
		}

		repirshop_log( $msg, $response );
	}

	protected function is_error()
	{
		return $this->error;
	}

	protected function set_error()
	{
		$this->error = true;
	}

	protected function add_meta( $product_id, $item_id )
	{
		$this->meta[] = compact( 'product_id', 'item_id' );
	}

	protected function save_meta()
	{
		if ( !$this->meta )
			return;

		update_post_meta( $this->order->id, $this->meta_key, array(
			'invoice_id' => $this->invoice_id,
			'line_items' => $this->meta
		) );
	}
}