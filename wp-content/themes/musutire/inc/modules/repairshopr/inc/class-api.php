<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
 * 
 */
class RapireShopr_Api
{

	// $api_key = '00512d48-aa22-4857-92b2-ffe91cfea4c3';
	// $subdomain = 'musutire';

	protected static $instance;

	protected $url = 'http://{subdomain}.repairshopr.com/api/v1/{query}?api_key={api_key}';

	protected $response;

	protected function __construct()
	{
		$this->set_url();
	}

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	public function get( $query )
	{
		$this->response = wp_remote_get( $this->get_url( $query ) );

		return $this;
	}

	public function post( $query, $data ) 
	{
		$this->response = wp_remote_post( $this->get_url( $query ), array( 'body' => $data ) );

		return $this;
	}

	public function put( $query, $data ) 
	{
		$this->response = wp_remote_request( $this->get_url( $query ), array( 
			'method' => 'PUT',
			'body'   => $data 
		) );

		return $this;
	}

	public function response()
	{
		if ( is_wp_error( $this->response ) ) {
			return $this->response;
		} else
		if ( 200 != wp_remote_retrieve_response_code( $this->response ) ) {
			return new WP_Error( 
				'repairshopr_server_error', 
				"Repairshopr -> code {$this->response['response']['code']}: {$this->response['response']['message']}" 
			);
		} 

		return json_decode( $this->response['body'] );
	}

	protected function get_url( $query )
	{
		return str_replace( '{query}', $query, $this->url );
	}

	protected function set_url()
	{
		$keys = array(
			'subdomain',
			'api_key'
		);

		foreach ( $keys as $key ) {
			$value = get_theme_option( "repairshopr_{$key}" );

			$this->url = str_replace( "{{$key}}", $value, $this->url );
		}
	}
}