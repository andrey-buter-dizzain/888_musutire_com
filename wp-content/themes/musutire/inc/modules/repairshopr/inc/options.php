<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

add_filter( 'populate_theme_options', 'populate_repairshopr_options' );
function populate_repairshopr_options( $settings = array() ) {

	$id = 'repairshopr';

	$settings['sections'][] = array(
		'title' => __( 'Repairshopr Integration', 'hah' ),
		'id'    => $id
	);

	$settings['settings'][] = array(
		'id'      => 'repairshopr_subdomain',
		'label'   => 'Subdomain',
		'type'    => 'text',
		'desc'    => 'https://{subdomain}.repairshopr.com/',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'repairshopr_api_key',
		'label'   => 'Api Key',
		'type'    => 'text',
		'desc'    => 'Reapirshopr account -> Your Name -> Profile/Password',
		'section' => $id,
	);

	return $settings;
}