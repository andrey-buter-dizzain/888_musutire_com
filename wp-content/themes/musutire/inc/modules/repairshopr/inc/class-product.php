<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
 * Связь single product с продуктом с сервиса по указанному upc_code
 *
 * В админке в продукте указывается upc_code продукта из repairshopr
 * и оттуда подтягивается цена и количество продукта на складе
 *
 * + На сервере работает cron jobs, который обновляет инфу всех продуктов каждый час
 */
class RepairShopr_Product
{
	const NONCE_KEY = 'repairshopr_product';

	const ACTION = 'repairshopr_get_product_data';

	const SERVER_ACTION = 'repairshopr_update_products_stock';

	public static $upc_code_key = 'upc_code';

	// cron url shold be 
	// http://musutire.com/wp-admin/admin-ajax.php?action=repairshopr_update_products_stock&auth_key=9V6iRshn84713BIHzzGx5D9PJ5xYztjl
	const AUTH_KEY = '9V6iRshn84713BIHzzGx5D9PJ5xYztjl';

	function __construct()
	{
		add_filter( 'woocommerce_product_data_tabs', array( $this, 'woocommerce_product_data_tabs' ) );

		add_filter( 'woocommerce_product_data_panels',         array( $this, 'woocommerce_product_data_panels' ) );
		add_filter( 'woocommerce_process_product_meta_simple', array( $this, 'woocommerce_process_product_meta_simple' ) );

		$action = self::ACTION;

		add_action( "wp_ajax_{$action}",        array( $this, 'ajax_get_data' ) );
		add_action( "wp_ajax_nopriv_{$action}", array( $this, 'ajax_get_data' ) );

		$action = self::SERVER_ACTION;

		add_action( "wp_ajax_{$action}",        array( $this, 'ajax_update_products_stock' ) );
		add_action( "wp_ajax_nopriv_{$action}", array( $this, 'ajax_update_products_stock' ) );

		// on create new order and invoice item
		add_action( 'repairshopr_invoice_item_create', array( $this, 'update_product_data' ), 10, 2 );

		// set out-of-stock by default on save product 
		add_action( 'save_post', array( $this, 'set_product_out_of_stock_by_default' ), 10, 3 );
	}

	function set_product_out_of_stock_by_default( $product_id, $post, $update )
	{
		if ( 'product' != $post->post_type )
			return;

		$upc_code = get_post_meta( $product_id, self::$upc_code_key, true );

		if ( $upc_code )
			return;

		wc_update_product_stock( $product_id, 0 );
	}

	function ajax_get_data()
	{
		check_ajax_referer( self::NONCE_KEY );

		$upc_code = $this->get_upc_code();

		if ( !$upc_code )
			wp_die();

		$data = $this->get_product_data( $upc_code );

		if ( !$data )
			wp_die();

		wp_send_json( $data );
	}

	protected function get_product_data( $upc_code )
	{
		$response = RapireShopr_Api::get_instance()
						->get( "products/{$upc_code}" )
						->response();

		if ( is_wp_error( $response ) )
			return;

		return array(
			'price' => $response->product->price_retail,
			'stock' => $response->product->quantity,
		);
	}

	/**
	 * Update price and quontity every hour
	 */
	function ajax_update_products_stock()
	{
		if ( !isset( $_GET['auth_key'] ) )
			wp_die();

		if ( self::AUTH_KEY != $_GET['auth_key'] )
			wp_die();

		$this->update_products_data();

		repirshop_log( 'Do cron job' );

		wp_die();
	}

	protected function update_products_data()
	{
		global $wpdb;

		$upc_code_key = self::$upc_code_key;

		$sql = "SELECT post_id, meta_value as upc_code FROM $wpdb->postmeta WHERE meta_key = '$upc_code_key' AND meta_value != ''";

		$products = $wpdb->get_results( $wpdb->prepare( $sql ) );

		if ( is_wp_error( $products ) )
			return;

		foreach ( $products as $product )
			$this->update_product_data( $product->post_id, $product->upc_code );
	}

	public function update_product_data( $product_id, $upc_code )
	{
		$data = $this->get_product_data( $upc_code );

		if ( !$data )
			return;

		if ( !function_exists( '_wc_save_product_price' ) )
			return;

		_wc_save_product_price(  $product_id, $data['price'] );
		wc_update_product_stock( $product_id, $data['stock'] );
	}

	function woocommerce_product_data_tabs( $tabs )
	{
		$cutsom['repairshopr'] = array(
			'label'  => __( 'Repairshopr integration', 'woocommerce' ),
			'target' => 'repairshopr_product_data',
			'class'  => array(),
		);

		return $cutsom + $tabs;
	}

	function woocommerce_product_data_panels()
	{
		global $thepostid, $post;

		$thepostid = empty( $thepostid ) ? $post->ID : $thepostid;

		wp_localize_script( 'admin-common-js', 'repairShoprData', array(
			'_wpnonce'  => wp_create_nonce( self::NONCE_KEY ),
			'data'      => array(
				'price'     => get_post_meta( $thepostid, '_regular_price', true ),
				'salePrice' => get_post_meta( $thepostid, '_sale_price', true ),
				'stock'     => absint( get_post_meta( $thepostid, '_stock', true ) ),
				'upcCode'   => get_post_meta( $thepostid, self::$upc_code_key, true ),
			),
			'action'    => self::ACTION,
			'tmpl'      => get_theme_part( REPAIRSHOPR_TMPL, 'wc-ng-meta-box', array( 
				'return' => true 
			) ),
		) );

		get_theme_part( REPAIRSHOPR_TMPL, 'wc-ng-meta-box-main' );
	}

	function woocommerce_process_product_meta_simple( $post_id )
	{
		$upc_code = $this->get_upc_code();

		if ( $upc_code )
			return update_post_meta( $post_id, self::$upc_code_key, $upc_code );

		delete_post_meta( $post_id, self::$upc_code_key );
	}

	protected function get_upc_code()
	{
		if ( !isset( $_REQUEST[ self::$upc_code_key ] ) )
			return;

		return absint( $_REQUEST[ self::$upc_code_key ] );
	}
}