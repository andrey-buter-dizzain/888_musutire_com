<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * Страница, на котрой отображаются логи интеграции с сервисом
 */
class RepairShopr_Admin_Page
{
	protected $slug = 'repairshopr-module';

	function __construct()
	{
		add_action( 'admin_menu', array( $this, 'admin_menu_init' ), 100 );
	}

	function admin_menu_init()
	{
		add_submenu_page( 
			'woocommerce', 
			__( 'RepairShopr Logs' ), 
			__( 'RepairShopr Logs' ), 
			'manage_options', 
			$this->slug, 
			array( $this, 'content' ) 
		);
	}

	function content() 
	{	
		get_theme_part( REPAIRSHOPR_TMPL, 'admin-page', array(
			'data' => Repairshopr_Log::get_instance()->get_readable_log_data()
		) );

	}
}