<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
 * 
 */
class Repairshopr_Log
{
	protected static $instance;

	protected $msgs = array();

	protected $folder = 'repairshopr';

	protected $notice;

	protected function __construct()
	{
		if ( is_admin() ) {
			add_action( 'init', array( $this, 'test_log_dir' ) );

			add_action( 'admin_notices', array( $this, 'admin_notices' ) );
		}

		add_action( 'shutdown', array( $this, 'write_all_logs' ) );
	}

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	function test_log_dir()
	{
		if ( !wp_mkdir_p( $this->get_path() ) )
			return $this->add_notice( "Log folder doesn't created" );

		if ( !wp_is_writable( $this->get_path() ) )
			return $this->add_notice( 'Log folder is not writable' );

		if ( !file_exists( $this->get_log_path() ) )
			if ( !$this->write_file( '' ) )
				return $this->add_notice( 'Log file does not created' );
	}

	function write_all_logs()
	{
		if ( empty( $this->msgs ) )
			return;

		$this->msgs[] = "\n";

		$this->write_file( implode( '', $this->msgs ) );
	}

	function admin_notices()
	{
		if ( !$this->notice )
			return;
	 ?>
		<div class="notice notice-warning is-dismissible">
			<p><b><?php _e( 'Repairshopr:' ) ?></b> <?php echo $this->notice ?> - <i><?php _e( 'File system permissions error.' ) ?></i></p>
			<p>Log file path: <?php echo str_replace( WP_CONTENT_DIR, '', $this->get_log_path() ) ?></p>
		</div>
	<?php
	}
	
	protected function write_file( $contents )
	{
		$fp = @fopen( $this->get_log_path(), 'a' );

		if ( !$fp )
			return false;

		mbstring_binary_safe_encoding();

		$data_length = strlen( $contents );

		$bytes_written = fwrite( $fp, $contents );

		reset_mbstring_encoding();

		fclose( $fp );

		if ( $data_length !== $bytes_written )
			return false;

		return true;
	}

	/**
	 * Not used
	 */
	protected function write_file_with_filesystem( $empty = false )
	{
		$filesystem = $this->get_wp_filesystem();

		$contents = $empty ? '' : implode( '', $this->msgs );

		if ( !$filesystem->put_contents( $this->get_log_path(), $contents ) )
			return $this->add_notice( 'Log file does not created' );
	}

	/**
	 * Include wp_filesystem classes
	 *
	 * BAD: Can only rewrite file
	 */
	protected function get_wp_filesystem()
	{
		$path = ABSPATH .'/wp-admin/includes';

		if ( !class_exists( 'WP_Filesystem_Base' ) )
			require_once( "$path/class-wp-filesystem-base.php" );

		if ( !class_exists( 'WP_Filesystem_Direct' ) )
			require_once( "$path/class-wp-filesystem-direct.php" );

		return new WP_Filesystem_Direct( '' );
	}


	public function add( $msg, $response = '' )
	{
		$date = date( "Y-m-d H:i:s" );

		$this->msgs[] = "$date: $msg \n";

		if ( defined( 'REPAIRSHOPR_DEBUG' ) AND REPAIRSHOPR_DEBUG AND $response ) {
			$msg = print_r( $response, true );

			$this->msgs[] = "$msg \n";
		}
	}

	protected function get_path()
	{
		$dir = wp_upload_dir();

		return "{$dir['basedir']}/theme-module-{$this->folder}";
	}

	protected function get_log_path()
	{
		return $this->get_path() ."/{$this->folder}.log";
	}

	protected function add_notice( $msg )
	{
		$this->notice = $msg;
	}

	public function get_file_log_data()
	{
		$fn = @fopen( $this->get_log_path(), "r" );

		if ( !$fn )
			return;

		$data = fread( $fn, filesize( $this->get_log_path() ) );

		fclose($fn);

		return $data;
	}

	public function get_readable_log_data()
	{
		$data = $this->get_file_log_data();
		$data = explode( "\n\n", $data );
		$data = array_reverse( $data );
		$data = implode( "\n\n", $data );

		return $data;
	}
}

function repirshop_log( $msg, $response = '' ) {
	Repairshopr_Log::get_instance()->add( $msg, $response );
}