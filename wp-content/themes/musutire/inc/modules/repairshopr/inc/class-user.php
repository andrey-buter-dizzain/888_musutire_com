<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * Синхронизация юзеров.
 * В repairshopr нельзя создать invoice без юзера, 
 * поэтому перед созданием incvoice там сначала создается новый юзер
 * или если такой юзер уже есть, берется его id.
 * Также для существующих юзеров идет синхронизация данных.
 * Если юзер изменил данные в ВП, то на repairshopr данные тоже обновятся
 */
class RepairShopr_User
{
	protected $meta_key = 'repairshopr_unregistered_users';

	protected $user_id;

	protected $subdomain = 'subdomain';

	protected $checkout_data;

	protected $delimiter = '->';

	protected static $instance;

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	function __construct()
	{
		$this->subdomain = get_theme_option( 'repairshopr_subdomain', $this->subdomain );

		// $this->replace_old_saved_data();

		// 1. Save checkout posted data
		add_action( 'woocommerce_after_checkout_validation', array( $this, 'save_checkout_posted_data' ) );

		// 2. save user data
		// priority = 50, because in theme module wc-user-roles переопределяется user_id,
		// чтоб seller мог произвести оплату под именем своего клиента
		add_filter( 'woocommerce_checkout_customer_id', array( $this, 'checkout_customer_id' ), 50 );
	}

	function save_checkout_posted_data( $posted )
	{
		$this->checkout_data = $posted;
	}

	function checkout_customer_id( $user_id )
	{	
		$this->user_id = $user_id;

		return $user_id;
	}

	/**
	 * Used in RepairShopr_Order::set_cutsomer()
	 */
	public function get_customer_id()
	{
		$saved_customer_id = $this->get_saved_customer_id();

		if ( $saved_customer_id ) {
			$this->update_customer();

			return $saved_customer_id;
		}

		$customer_id = $this->add_customer();

		return $customer_id;		
	}

	protected function get_customer_data()
	{
		$data = $this->checkout_data;

		$required = array(
			'firstname' => $data['billing_first_name'],
			'lastname'  => $data['billing_last_name'],
			'phone'     => $data['billing_phone'],
			'email'     => $data['billing_email'],
		);

		$optional = array(
			// 'mobile'        => '',
			// 'business_name' => '',
			'address'       => $data['billing_address_1'],
			// 'address_2'     => '',
			'city'          => $data['billing_city'],
			'state'         => $data['billing_state'],
			'zip'           => $data['billing_postcode'],
		);
		
		return wp_parse_args( $required, $optional );
	}

	public function get_customer_instructions()
	{
		$response = RapireShopr_Api::get_instance()
						->get( 'customers/new' )
						->response();

		$this->log( 'new-customer-instructions', $response );
	}

	public function get_user_id()
	{
		return $this->user_id;
	}

	protected function add_customer()
	{
		$data = $this->get_customer_data();

		$response = RapireShopr_Api::get_instance()
						->post( 'customers', $data )
						->response();

		$this->log( 'create', $response );

		if ( is_wp_error( $response ) ) {
			$this->set_error();
			return;
		}

		$customer_id = $response->customer->id;

		if ( !$customer_id )
			$customer_id = $this->mb_find_customer();

		if ( !$customer_id )
			return;

		$this->save_cusomer_id( $customer_id );

		return $customer_id;
	}

	protected function mb_find_customer()
	{
		$customers = $this->get_all_customers();

		if ( !$customers )
			return;

		$current = $this->get_customer_data();

		foreach ( $customers as $customer ) {
			if ( $customer->email == $current['email'] )
				return $customer->id;
		}
	}

	protected function get_all_customers()
	{
		$response = RapireShopr_Api::get_instance()
						->get( "customers" )
						->response();

		$this->log( 'get all customers', $response );

		if ( is_wp_error( $response ) ) {
			$this->set_error();
			return;
		}

		return $response->customers;
	}

	protected function get_saved_customer_id()
	{
		if ( !$this->is_unregistered_user() ) {
			$meta = get_user_meta( $this->user_id, $this->meta_key, true );

			return $meta[ $this->subdomain ];
		}

		$unregistered = get_option( $this->meta_key, array() );

		$email = $this->checkout_data['billing_email'];

		return $unregistered[ $this->subdomain ][ $email ];
	}

	protected function save_cusomer_id( $customer_id )
	{
		if ( !$this->is_unregistered_user() ) {
			$meta = get_user_meta( $this->user_id, $this->meta_key, true );

			repirshop_log( '$customer_id', $customer_id );

			if ( !$meta )
				$meta = array();

			$meta[ $this->subdomain ] = $customer_id;

			repirshop_log( '$meta', $meta );

			return update_user_meta( $this->user_id, $this->meta_key, $meta );
		}

		$unregistered = get_option( $this->meta_key, array() );

		$email = $this->checkout_data['billing_email'];

		$unregistered[ $this->subdomain ][ $email ] = $customer_id;

		update_option( $this->meta_key, $unregistered );
	}

	protected function update_customer() {
		$data        = $this->get_customer_data();
		$customer_id = $this->get_saved_customer_id();

		$response = RapireShopr_Api::get_instance()
						->put( "customers/{$customer_id}", $data )
						->response();

		$this->log( 'update customer', $response );

		if ( is_wp_error( $response ) ) {
			$this->set_error();
			return;
		}

		return $response->customer->id;
	}

	protected function log( $type, $response )
	{
		$msg = "Customer {$this->delimiter} ";

		if ( is_wp_error( $response ) ) {
			$msg .= "ERROR {$this->delimiter} ". $response->get_error_message();
		} else
		if ( 'new-customer-instructions' == $type ) {
			$msg .= "instructions: \n\n {$response->message}";
		} else {
			$msg .= "id: {$response->customer->id} {$this->delimiter} SUCCESS $type";
		}	

		repirshop_log( $msg, $response );
	}

	protected function is_unregistered_user()
	{
		return empty( $this->user_id );
	}

	/**
	 * Раньше customer_id записывался без указания submodule
	 * Возникла проблемы тестинга на тестовом аккаунте, 
	 * т.к. в базе сохранялись id для лайв версии,
	 * но для тестового они были не верны.
	 * И чтоб их переписать нужно было делать много телодвижений.
	 */
	protected function replace_old_saved_data()
	{
		$this->update_old_saved_users_data();
		$this->update_old_saved_unregistered_users_data();
	}

	protected function update_old_saved_unregistered_users_data()
	{
		$unregistered = get_option( $this->meta_key );

		if ( !$unregistered )
			return;

		if ( isset( $unregistered[ $this->subdomain ] ) )
			return;

		$unregistered[ $this->subdomain ] = $unregistered;

		update_option( $this->meta_key, $unregistered );
	}

	protected function update_old_saved_users_data()
	{
		$users = get_users( array(
			'meta_key' => $this->meta_key
		) );

		if ( !$users )
			return;

		foreach ( $users as $user ) {
			$new  = array();
			$meta = get_user_meta( $user->ID, $this->meta_key, true );

			if ( !$meta )
				continue;

			if ( isset( $meta[ $this->subdomain ] ) )
				continue;

			$new[ $this->subdomain ] = $meta;

			update_user_meta( $user->ID, $this->meta_key, $new );
		}
	}
}