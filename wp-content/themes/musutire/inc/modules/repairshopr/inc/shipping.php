<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * В shipping модулем темы woocomerce создан кастомный метода need_to_install
 * У него есть поля Service cost и Cost per 1 wheel
 * Здесь к этим полям добавляются поля с upc_code соттветственно.
 * Service и wheel - это продукты в repairshopr. (Т.к. другого варинта не было)
 * И их цена синхронизируется с сервисом здесь.
 */
add_filter( 'module_wc_shipping_need_to_install_fields', 'repairshopr_wc_shipping_need_to_install_fields' );
function repairshopr_wc_shipping_need_to_install_fields( $fields ) {
	$fields['service_upc_code'] = array(
		'title' 		=> __( 'Service UPC Code', 'woocommerce' ),
		'type' 			=> 'custom_field',
		'description'	=> __( 'UPC code of product "Service".', 'woocommerce' ),
		'desc_tip'		=> true,
		'custom_attributes' => array(
			'data-price' => 'woocommerce_need_to_install_service_cost'
		)
	);

	$fields['per_one_upc_code'] = array(
		'title' 		=> __( 'UPC Code product of per wheel', 'woocommerce' ),
		'type' 			=> 'custom_field',
		'description'	=> __( '', 'woocommerce' ),
		'desc_tip'		=> true,
		'custom_attributes' => array(
			'data-price' => 'woocommerce_need_to_install_cost'
		)
	);

	return $fields;
}


add_filter( 'module_wc_shipping_need_to_install_custom_field_html', 'repairshopr_wc_shipping_need_to_install_custom_field_html', 10, 4 );
function repairshopr_wc_shipping_need_to_install_custom_field_html( $html, $key, $data, $method ) {
	$field_key = $method->get_field_key( $key );
	$defaults  = array(
		'title'             => '',
		'disabled'          => false,
		'class'             => '',
		'css'               => '',
		'placeholder'       => '',
		'type'              => 'custom_field',
		'type_field'        => 'text',
		'desc_tip'          => false,
		'description'       => '',
		'custom_attributes' => array(),
	);

	$data = wp_parse_args( $data, $defaults );

	$id = 

	ob_start();
	?>
	<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			<?php echo $method->get_tooltip_html( $data ); ?>
		</th>
		<td class="forminp">
			<script>
				jQuery(document).ready(function($) {
					var id       = '<?php echo $field_key ?>';
					var $input   = $('#'+ id);
					var $btn     = $input.siblings('button');
					var $price   = $('#'+  $input.data( 'price' ) );
					var $spinner = $input.siblings( '.spinner' );

					$price.attr('readonly', true);

					$btn.on( 'click', function(e) {
						e.preventDefault();

						var val = $input.val();

						if ( !val )
							return;

						$spinner.addClass( 'is-active' );
						$btn.attr( 'disabled', true );

						$.ajax({
							type: 'GET',
							url: themeAdminData.ajaxUrl,
							data: {
								action: '<?php echo RepairShopr_Product::ACTION ?>',
								upc_code: val,
								_wpnonce: '<?php echo wp_create_nonce( RepairShopr_Product::NONCE_KEY ) ?>'
							},
							success: function(data) {
								if ( !data ) 
									return;

								if ( !data.price ) 
									return;

								$price.val( data.price );

								$spinner.removeClass( 'is-active' );
								$btn.attr( 'disabled', false );
							}
						});
					});
				});
			</script>
			<fieldset>
				<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
				<input 
					class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>" 
					type="<?php echo esc_attr( $data['type_field'] ); ?>" 
					name="<?php echo esc_attr( $field_key ); ?>" 
					id="<?php echo esc_attr( $field_key ); ?>" 
					style="<?php echo esc_attr( $data['css'] ); ?>" 
					value="<?php echo esc_attr( $method->get_option( $key ) ); ?>" 
					placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>" 
					<?php disabled( $data['disabled'], true ); ?> 
					<?php echo $method->get_custom_attribute_html( $data ); ?> 
				/>
				<button type="button" class="button add_attribute">Update data</button>
				<span class="spinner" style="float: none; margin-left: 5px; margin-right: 0; vertical-align: top;"></span>
				<?php echo $method->get_description_html( $data ); ?>
			</fieldset>
		</td>
	</tr>
	<?php

	return ob_get_clean();
}

// мета данные новых полей для ордера
add_filter( 'module_wc_shipping_need_to_install_rate', 'repairshopr_wc_shipping_need_to_install_rate', 10, 2 );
function repairshopr_wc_shipping_need_to_install_rate( $rate, $method ) {
	$rate['meta_data']['upc_codes'] = array(
		'service' => array(
			'code' => $method->get_option( 'service_upc_code' ),
			'qty'  => 1,
		),
		'per_one' => array(
			'code' => $method->get_option( 'per_one_upc_code' ),
			// 'qty'  => 'selected_by_user'
			'qty'  => 1,
		),
	);

	return $rate;
}