<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Модуль интеграции с сервисом Repairshopr.com
 */

$path = dirname( __FILE__ );

$name = basename( $path );

if ( !defined( 'REPAIRSHOPR_TMPL' ) )
	define( 'REPAIRSHOPR_TMPL', THEME_MODULE_PATH ."/{$name}/tmpl" );


if ( !defined( 'REPAIRSHOPR_DEBUG' ) )
	define( 'REPAIRSHOPR_DEBUG', is_dev_version() );


if ( is_admin() ) {
	require_once( "{$path}/inc/options.php" );
	require_once( "{$path}/inc/class-admin-page.php" );

	new RepairShopr_Admin_Page;
}

require_once( "{$path}/inc/class-api.php" );
require_once( "{$path}/inc/class-log.php" );

// синхронизация single product
require_once( "{$path}/inc/class-product.php" );

// создание order
require_once( "{$path}/inc/class-order.php" );

// синхронизация юзера-клиента
require_once( "{$path}/inc/class-user.php" );

// синхронизация shipping-продуктов
require_once( "{$path}/inc/shipping.php" );

new RepairShopr_Product;
new RepairShopr_Order;
RepairShopr_User::get_instance();
Repairshopr_Log::get_instance();