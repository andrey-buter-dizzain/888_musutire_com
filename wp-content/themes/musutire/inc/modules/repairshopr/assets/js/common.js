if ( 'undefined' != typeof repairShoprData ) {
	angular.module( 'repairShoprApp', [] )
		.component( 'priceHandler', {
			template: repairShoprData.tmpl,
			controller: function( $http, $log ) {
				var self     = this;
				var elements = {};

				var $main      = angular.element( '#woocommerce-product-data' );
				var $general   = $main.find( '#general_product_data' );
				var $inventory = $main.find( '#inventory_product_data' );

				this.data = repairShoprData.data;		

				this.getPrice = function() {
					self.showSpinner = true;

					$http({
						method: 'GET',
						url: themeAdminData.ajaxUrl,
						params: {
							action: repairShoprData.action,
							upc_code: self.data.upcCode,
							_wpnonce: repairShoprData._wpnonce
						}
					})
					.then( function( data ) {
						return data = data.data;
					})
					.then( function( data ) {
						self.data.price = data.price;
						self.data.stock = data.stock;

						self.showSpinner = false;

						self.replaceDefaultWCElements();
					});
				}

				this.replaceDefaultWCElements = function() {
					replaceField( $general,   '_regular_price', 'price' );
					replaceField( $general,   '_sale_price',    'salePrice' );
					replaceField( $inventory, '_stock',         'stock' );
					setCheckbox(  $inventory, '_manage_stock',   true );
				}

				function replaceField( $parent, id, key ) {
					if ( !( id in elements ) ) {
						var $span  = angular.element( '<span class="custom-field">' );
						var $input = $parent.find( '#'+ id )
										.attr( 'type', 'hidden' )
										.after( $span )

						elements[ id ] = {
							input: $input,
							span:  $span
						};
					};

					elements[ id ].input.val( self.data[ key ] );
					elements[ id ].span.text( self.data[ key ] );
				}

				function setCheckbox( $parent, id, checked ) {
					checked = checked || true;

					if ( !( id in elements ) ) {
						var $input = $parent.find( '#'+ id );

						elements[ id ] = {
							input: $input,
							span:  null
						};
					}

					elements[ id ].input.prop( 'checked', checked );
				}

				self.replaceDefaultWCElements();
			}
		});
};