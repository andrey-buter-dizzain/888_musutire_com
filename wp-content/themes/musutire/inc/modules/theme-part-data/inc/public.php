<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Get template data if it's set in get_theme_part()
 */
function get_theme_part_data() {
	return Theme_Part_Data::get_instance()->get_template_data();
}

/**
 * Theme-part set new data name
 */
function theme_part_set_new_name() {
	return Theme_Part_Names::set_new_name();
}

/**
 * Get theme-part current data name
 */
function theme_part_get_current_name() {
	return Theme_Part_Names::get_current_name();
}

/**
 * Delete theme part current (last) data name
 */
function theme_part_delete_current_name() {
	return Theme_Part_Names::delete_current_name();
}