<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );


/**
 * List Item option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_custom_list_item' ) ) {
	
	function ot_type_custom_list_item( $args = array() ) {
		
		/* turns arguments array into variables */
		extract( $args );
		
		/* verify a description */
		$has_desc = $field_desc ? true : false;

		/* format setting outer wrapper */
		echo '<div class="format-setting type-list-item ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . '">';
			
			/* description */
			echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';
			
			/* format setting inner wrapper */
			echo '<div class="format-setting-inner">';
				
				/* pass the settings array arround */
				echo '<input type="hidden" name="' . esc_attr( $field_id ) . '_settings_array" id="' . esc_attr( $field_id ) . '_settings_array" value="' . ot_encode( serialize( $field_settings ) ) . '" />';
				
				/** 
				 * settings pages have array wrappers like 'option_tree'.
				 * So we need that value to create a proper array to save to.
				 * This is only for NON metabox settings.
				 */
				if ( ! isset( $get_option ) )
					$get_option = '';
					
				/* build list items */
				echo '<ul class="option-tree-setting-wrap option-tree-sortable" data-name="' . esc_attr( $field_id ) . '" data-id="' . esc_attr( $post_id ) . '" data-get-option="' . esc_attr( $get_option ) . '" data-type="' . esc_attr( $type ) . '">';
				
				if ( is_array( $field_value ) && ! empty( $field_value ) ) {
				
					foreach( $field_value as $key => $list_item ) {
						
						echo '<li class="ui-state-default list-list-item">';
							ot_list_item_view( $field_id, $key, $list_item, $post_id, $get_option, $field_settings, $type );
						echo '</li>';
						
					}
					
				}
				
				echo '</ul>';
				
				/* button */
				// echo '<a href="javascript:void(0);" class="option-tree-list-item-add option-tree-ui-button button button-primary right hug-right" title="' . __( 'Add New', 'option-tree' ) . '">' . __( 'Add New', 'option-tree' ) . '</a>';
				
				/* description */
				echo '<div class="list-item-description">' . apply_filters( 'ot_list_item_description', __( 'You can re-order with drag & drop, the order will update after saving.', 'option-tree' ), $field_id ) . '</div>';
			
			echo '</div>';

		echo '</div>';
		
	}
	
}