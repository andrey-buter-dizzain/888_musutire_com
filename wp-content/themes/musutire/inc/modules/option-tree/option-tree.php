<?php 

class Inc_Module_Option_Tree_Addon extends Inc_Load_Section
{
	public static $section;

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'checkbox-simple',
		'hidden',
		'on-off-simple',
		'custom-list-item',
		'sortable-options',
		'custom-post-type-select-2'
	);

	/**
	 * Preload function
	 */
	public static function pre_load() 
	{
		if ( !is_admin() )
			return;

		$module = basename( __DIR__ );

		static::$section = 'modules/'. $module;

		static::$section = static::$section . '/fields';
	}

	/**
	 * Get partial function
	 */
	protected static function get_partial( $partial = '' )
	{
		return $partial .'/'. $partial;
	}
}
Inc_Module_Option_Tree_Addon::load();