<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( !class_exists( 'WooCommerce' ) )
	return;

if ( !class_exists( 'WC_Shipping_Method' ) )
	return;

require_once( 'inc/custom.php' );
require_once( 'inc/checkout.php' );
require_once( 'inc/account.php' );
require_once( 'inc/user.php' );
require_once( 'inc/shipping/shipping.php' );
require_once( 'inc/shipping/method-need-to-install.php' );
require_once( 'inc/payment/payment.php' );
require_once( 'inc/payment/partial-payment/class-wc-gateway-partial-payment.php' );
require_once( 'inc/payment/due-date/class-wc-gateway-due-date.php' );