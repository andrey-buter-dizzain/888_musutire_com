<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Shipping и billing устроен следующим образом:
 * 0) т.к. нельзя в woocommerce их заменить местами, изобретаем костыли
 * 1) по умолчанию на странице checkout находится форма billing, но подписана она как shipping,
 * поэтому в перую очередь заменяем все сообщения об ошибках с billing на shipping и соответственно обратно, 
 * см. module_woocommerce_checkout_required_field_notice()
 * 2) в форме billing (в коде это shipping) нет полей phone и email, т.к. они указываются только один раз
 * 3) перед обработкой checkout формы заменяем данные как в п. 1), данные billing на shipping, 
 * и данные shipping на billing. При этом поля phone и email оставляем в billing
 */
class Module_WC_Checkout
{	
	protected static $names = array(
		'first_name',
		'last_name',
		'email',
		'phone',
		'company',
		'city',
		'state',
		'country',
		'postcode',
		'address_1',
	);

	protected $labels = array();

	function __construct()
	{
		$this->set_labels();

		// --- CHECKOUT FORM --- //

		// Изменение labels of fields
		add_filter( 'woocommerce_default_address_fields', array( $this, 'woocommerce_default_address_fields' ) );

		// Изменение email label
		add_filter( 'woocommerce_billing_fields', array( $this, 'woocommerce_billing_fields' ) );

		// Сортировка полей в форме и удаление не нужных
		add_filter( 'woocommerce_checkout_fields', array( $this, 'filter_checkout_fields' ) );

		add_action( 'woocommerce_checkout_after_customer_details', array( $this, 'add_hidden_country_field' ) );

		// create js errors variable
		add_action( 'wp_footer', array( $this, 'wp_footer' ), 1 );

		// Замена error messages
		add_filter( 'woocommerce_checkout_required_field_notice', array( $this, 'woocommerce_checkout_required_field_notice' ), 10, 2 );

		// --- START CHECKOUT PROCESS / BEFORE CREATE NEW ORDER --- //

		// Подмена местами billing and shipping
		// Самый первый action in WC_Checkout::process_checkout()
		add_action( 'woocommerce_checkout_process', array( $this, 'woocommerce_checkout_process' ), 20 );
	}

	function add_hidden_country_field()
	{
		$keys = array(
			'billing',
			'shipping'
		);

		$location = $this->get_general_location();

		foreach ( $keys as $key ) {
			echo "<input type=\"hidden\" name=\"{$key}_country\" value=\"{$location['country']}\">";

			if ( $location['state'] )
				echo "<input type=\"hidden\" name=\"{$key}_state\" value=\"{$location['state']}\">";
		}
	}

	/**
	 * create js errors variable
	 */
	function wp_footer()
	{
		if ( !is_checkout() )
			return;

		$errors = $this->get_error_messages();

		wp_localize_script( 'common-js', 'themeCheckoutErrors', $errors );
	}

	protected function get_error_messages()
	{
		$errors = array();

		foreach ( $this->labels as $key => $name ) {
			$errors[ $key ] = sprintf( _x( '%s is a required field.', 'FIELDNAME is a required field.', 'woocommerce' ), "<strong>PART {$name}</strong>" );
		}

		$errors['email_not_valid'] = "<strong>PART {$this->labels['email']}</strong> ". __( 'is not a valid email address.', 'woocommerce' );

		$errors['phone_not_valid'] = "<strong>PART {$this->labels['phone']}</strong> ". __( 'is not a valid phone number.', 'woocommerce' );

		$errors['shipping'] = __( 'Shipping' );
		$errors['billing']  = __( 'Billing' );

		return $errors;
	}

	protected function set_labels()
	{
		$this->labels = array(
			'email'    => __( 'Email' ),
			'phone'    => __( 'Phone', 'woocommerce' ),
			'company'  => __( 'Company' ),
			'city'     => __( 'City/Town' ),
			'state'    => __( 'State' ),
			'postcode' => __( 'Zip' ),
		);
	}

	/**
	 * Изменение labels of fields
	 */
	function woocommerce_default_address_fields( $fields )
	{
		$keys = array(
			'company',
			'city',
			'state',
			'postcode',
		);

		$labels = array_intersect_key( $this->labels, array_flip( $keys ) );
		
		foreach ( $labels as $key => $label ) {
			$fields[ $key ]['label'] = $label;
		}

		$fields['address_1']['placeholder'] = '';

		$this->save_labels( $fields );

		return $fields;
	}

	/**
	 * Save labels for use it in js error messages
	 */
	protected function save_labels( $fields )
	{
		foreach ( $fields as $key => $field ) {
			if ( isset( $this->labels[ $key ] ) )
				continue;

			// for ex. field 'placeholder'
			if ( !isset( $field['label'] ) )
				continue;

			$this->labels[ $key ] = $field['label'];
		}
	}

	/**
	 * Изменение email label
	 */
	function woocommerce_billing_fields( $fields )
	{
		$keys = array(
			'email'
		);

		$labels = array_intersect_key( $this->labels, array_flip( $keys ) );
		
		foreach ( $labels as $key => $label ) {
			$fields["billing_{$key}" ]['label'] = $label;
		}

		return $fields;
	}

	/**
	 * Замена error messages
	 */
	function woocommerce_checkout_required_field_notice( $notice, $field ) 
	{
		$labels = array(
			'shipping' => sprintf( _x( 'Shipping %s', 'Shipping FIELDNAME', 'woocommerce' ), '' ),
			'billing'  => sprintf( _x( 'Billing %s',  'Billing FIELDNAME',  'woocommerce' ), '' ),
		);

		$only_billing_fields = array( 'email', 'phone' );

		foreach ( $only_billing_fields as $key ) {
			if ( false === strpos( $field, $this->labels[ $key ] ) )
				continue;

			return str_replace( $labels['billing'], $labels['shipping'], $notice );
		}

		/**
		 * Когда определен ship_to_different_address, 
		 * тогда названия ошибок ставятся правильные.
		 * Когда нет, их нужно заменять на зеркальные.
		 */
		if ( isset( $_POST['ship_to_different_address'] ) )
			return $notice;

		$relates = array(
			'shipping' => 'billing',
			'billing'  => 'shipping',
		);

		foreach ( $labels as $key => $label ) {
			if ( false === strpos( $field, $label ) )
				continue;

			return str_replace( $label, $labels[ $relates[ $key ] ], $notice );
		}

		return $notice;
	}

	/**
	 * Сортировка полей в форме и удаление не нужных
	 */
	function filter_checkout_fields( $fileds ) 
	{
		$keys = array(
			'billing',
			'shipping'
		);

		foreach ( $keys as $key ) {
			$output = array();

			foreach ( self::get_names() as $name ) {
				$name = "{$key}_{$name}";

				if ( in_array( $name, array( 'shipping_email', 'shipping_phone' ) ) )
					continue;

				if ( 'billing_postcode' == $name )
					$fileds[ $key ][ $name ]['id'] = 'shipping_postcode';
				elseif ( 'shipping_postcode' == $name )
					$fileds[ $key ][ $name ]['id'] = 'billing_postcode';

				$output[ $name ] = $fileds[ $key ][ $name ];
			}

			$fileds[ $key ] = $output;
		}

		return $fileds;
	}

	/**
	 * Подмена местами billing and shipping
	 */
	function woocommerce_checkout_process() 
	{
		if ( !defined( 'WOOCOMMERCE_CHECKOUT' ) )
			return;

		if ( true !== WOOCOMMERCE_CHECKOUT )
			return;

		$data = $this->parse_billing_shipping();
		$data = $this->replace_billing_shipping( $data );

		$this->replace_POST( $data );
		$this->set_customer_default_country();
	}

	protected function set_customer_default_country()
	{
		$shipping_country = WC()->customer->get_shipping_country();

		if ( $shipping_country )
			return;

		$default = wc_get_customer_default_location();
		$country = $default['country'];

		if ( isset( $_POST['shipping_country'] ) )
			$country = $_POST['shipping_country'];
		else 
		if ( isset( $_POST['billing_country'] ) )
			$country = $_POST['billing_country'];

		WC()->customer->set_shipping_country( $country );
	}

	protected function get_general_location()
	{
		return wc_get_base_location();

		// $shipping_country = WC()->customer->get_shipping_country();

		var_dump(wc_get_base_location());
		var_dump(wc_get_customer_default_location());
		var_dump( WC()->customer);
		// var_dump($shipping_country);

		if ( $shipping_country )
			return $shipping_country;

		$default = wc_get_customer_default_location();

		return $default['country'];
	}

	protected function parse_billing_shipping() 
	{
		$data = array();

		$keys = array(
			'billing',
			'shipping'
		);

		foreach ( $keys as $key ) {
			foreach ( $_POST as $name => $value ) {
				if ( 'shipping_method' == $name )
					continue;

				if ( 0 !== strpos( $name, $key ) )
					continue;

				$data[ $key ][ $name ] = $value;
			}

			foreach ( $data[ $key ] as $name => $value ) {
				unset( $_POST[ $name ] );
				unset( $data[ $key ][ $name ] );

				$name = str_replace( "{$key}_", '', $name );

				$data[ $key ][ $name ] = $value;
			}
		}

		return array_filter( $data );
	}

	protected function replace_billing_shipping( $data ) 
	{
		if ( !isset( $_POST['ship_to_different_address'] ) ) {
			unset( $data['shipping'] );
			return $data;
		}

		if ( 1 == count( $data ) )
			return $data;

		extract( $data );

		$data['shipping'] = $billing;
		$data['billing']  = $shipping;

		$data['billing']['email'] = $billing['email'];
		$data['billing']['phone'] = $billing['phone'];

		unset( $data['shipping']['email'] );
		unset( $data['shipping']['phone'] );

		return $data;
	}

	protected function replace_POST( $data ) 
	{
		foreach ( $data as $key => $fields ) {
			foreach ( $fields as $name => $value ) {
				$name = "{$key}_{$name}";

				$_POST[ $name ] = $value;
			}
		}
	}

	public static function get_names()
	{
		return self::$names;
	}
}
new Module_WC_Checkout;


add_action( 'woocommerce_checkout_after_customer_details', 'module_wc_custom_shipping_action', 50 );
function module_wc_custom_shipping_action() {
?>
	<button class="checkout-button js-btn-step-shipping" type="button" data-step-action="external" data-step-disabled="1">
		<span>
			<?php _e( 'Proceed Purchase' ) ?>
		</span>
	</button>
<?php 
}