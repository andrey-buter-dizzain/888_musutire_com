<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

remove_filter( 'the_title', 'wc_page_endpoint_title' );

/**
 * Redirect dashboard page to orders list
 */
add_action( 'template_redirect', 'module_wc_redirect_dashboard' );
function module_wc_redirect_dashboard() {
	if ( !is_user_logged_in() )
		return;

	if ( !is_account_page() )
		return;

	$endpoint = WC()->query->get_current_endpoint();

	if ( !empty( $endpoint ) )
		return;

	wp_redirect( wc_get_account_endpoint_url( 'orders' ) );
	die;
}

add_filter( 'woocommerce_account_menu_items', 'module_woocommerce_account_menu_items' );
function module_woocommerce_account_menu_items( $items ) {
	$default = array(
		'orders',
		'clients',
		'edit-account',
		'edit-address',
		'customer-logout'
	);

	if ( current_user_can( 'seller' ) OR current_user_can( 'administrator' ) )
		$items['clients'] = __( 'Clients' );

	$output = array();

	foreach ( $default as $key ) {
		if ( !isset( $items[ $key ] ) )
			continue;

		$output[ $key ] = $items[ $key ];
	}

	return $output;
}

add_filter( 'woocommerce_account_menu_item_classes', 'module_woocommerce_account_menu_item_classes', 10, 2 );
function module_woocommerce_account_menu_item_classes( $classes, $endpoint ) {
	if ( 'orders' != $endpoint )
		return $classes;

	if ( 'view-order' != WC()->query->get_current_endpoint() )
		return $classes;

	$classes[] = 'is-active';

	return $classes;
}

/**
 * Edit address page
 */
// add_filter( 'woocommerce_address_to_edit', 'module_woocommerce_address_to_edit' );
function module_woocommerce_address_to_edit( $address ) {
	$names = Module_WC_Checkout::get_names();

	reset( $address );
	$first_key = key( $address );

	$type = false === strpos( $first_key, 'shipping' ) ? 'billing' : 'shipping';

	$output = array();

	foreach ( $names as $key => $name ) {
		$key = "{$type}_{$name}";

		if ( !isset( $address[ $key ] ) )
			continue;

		$output[ $key ] = $address[ $key ];
	}

	return $output;
}