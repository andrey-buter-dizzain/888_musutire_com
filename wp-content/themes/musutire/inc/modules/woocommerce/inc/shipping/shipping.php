<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_WC_Shipping_Install_Wheels
{
	protected $per_one = 0;

	const SESSION_KEY = 'need_to_install';

	protected $method = 'need_to_install';

	protected static $instance;

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	protected function __construct()
	{
		// see event name in class-wc-ajax.php
		$ajax_event = 'update_shipping_method';
		
		/**
		 * Chackout page frontend, update shipping dropdown by ajax
		 */
		add_action( "wp_ajax_nopriv_woocommerce_{$ajax_event}", array( $this, 'update_shipping_method' ), 1 );
		add_action( "wc_ajax_{$ajax_event}", array( $this, 'update_shipping_method' ), 1 );

		// Устанавливаем в shipping package cost суммарное значение цены в cart, checkout 
		add_filter( 'woocommerce_shipping_packages', array( $this, 'wc_shipping_packages' ) );

		// Заменяем label в shipping дропдауне in the cart page
		add_filter( 'woocommerce_cart_shipping_method_full_label', array( $this, 'wc_cart_shipping_method_full_label' ), 10, 2 );

		// Add shipping meta data in order
		add_action( 'woocommerce_add_shipping_order_item', array( $this, 'wc_add_shipping_order_item' ), 10, 3 );

		// При создании order очищаем сохранненое количество need_to_install
		add_filter( 'woocommerce_payment_successful_result', array( $this, 'clear_session' ), 100, 2 );
		add_filter( 'woocommerce_checkout_no_payment_needed_redirect', array( $this, 'clear_session' ), 100, 2 );

		add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'wc_add_to_cart_fragments' ) );
	}

	function wc_add_to_cart_fragments( $fragments )
	{
		$fragments['cart_contents_count'] = WC()->cart->get_cart_contents_count();
		$fragments['product_cart_qty']    = get_product_count_in_cart( $_POST['product_id'] );

		return $fragments;
	}

	/**
	 * При создании order очищаем сохранненое количество need_to_install,
	 * т.к. после создания order сессия остается активной и в cart попадает те же значения
	 */
	function clear_session( $data )
	{
		WC()->session->set( self::SESSION_KEY, 1 );

		return $data;
	}

	/**
	 * Add shipping meta data in order
	 */
	function wc_add_shipping_order_item( $order_id, $item_id, $package_key )
	{
		$qty      = WC()->session->get( self::SESSION_KEY );
		$shipping = wc_get_chosen_shipping_method_ids();
		$shipping = $shipping[ $package_key ];

		if ( $shipping != $this->method )
			return;

		wc_add_order_item_meta( $item_id, 'quantity', $qty, true );
		wc_delete_order_item_meta( $item_id, 'Items' );
	}

	/**
	 * перехватываем ajax request 
	 * т.к. нет возможности передавать отдельно параметры, 
	 * то в значении shipping_method находится 2 значения:
	 * - shipping_method
	 * - need_to_install
	 *
	 * Извлекаем need_to_install, записываем в WC сессию и перезаписываем $_POST на оригинальный
	 */
	function update_shipping_method() 
	{
		check_ajax_referer( 'update-shipping-method', 'security' );

		if ( !isset( $_POST['shipping_method'] ) )
			return;

		$value = stripslashes( array_shift( $_POST['shipping_method'] ) );

		if ( !$this->is_json( $value ) )
			return;

		$data = json_decode( $value );


		if ( ! defined('WOOCOMMERCE_CART') )
			define( 'WOOCOMMERCE_CART', true );

		$_POST['shipping_method']      = array( $data->shipping );
		$_POST['json_shipping_method'] = $data; // for using by other modules

		$install = $data->install;

		WC()->session->set( self::SESSION_KEY, absint( $install ) );
	}

	/**
	 * Получение параметров need_to_instal счетчика
	 */
	public function get_need_install_data( $chosen_method, $index ) {
		$per_one = 0;

		$qty = absint( WC()->session->get( self::SESSION_KEY, 1 ) );

		$count       = WC()->cart->get_cart_contents_count();
		$packages    = WC()->shipping->get_packages();

		if ( !$packages[ $index ] )
			return array();

		$method_meta = $packages[ $index ]['rates'][ $chosen_method ]->get_meta_data();

		if ( $chosen_method ) 
			$chosen_method = $this->parse_value( $chosen_method, 0 );

		if ( $this->method == $chosen_method ) {
			$per_one = absint( $method_meta['per_one'] );

			if ( 0 == $qty )
				$qty = 1;
		}
		else 
			$qty = 0;

		if ( $qty > $count )
			$qty = $count;

		WC()->session->set( self::SESSION_KEY, $qty );

		return array(
			'input_name'  => $this->method, // required name 'shipping_method'
			'input_value' => $qty,
			'max_value'   => $count,
			'min_value'   => 1,
			'subtotal'    => $qty * $per_one,
			'service'     => $method_meta['service']
		);
	}

	/**
	 * Устанавливаем в shipping packages rates сумму за количество устанавливаемых колес 
	 * плюс цена за сервис
	 */
	function wc_shipping_packages( $packages ) {
		// set by woocommerce plugin
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
		
		$install        = WC()->session->get( self::SESSION_KEY );

		$is_install_method = false;

		if ( !$install )
			return $packages;

		if ( !$chosen_methods )
			return $packages;

		foreach ( $chosen_methods as $chosen_method ) {
			$chosen_method = $this->parse_value( $chosen_method, 0 );

			if ( $this->method == $chosen_method ) {
				$is_install_method = true;
				break;
			}
		}

		if ( !$is_install_method )
			return $packages;

		foreach ( $packages as $i => $package ) {
			if ( ! empty( $chosen_methods[ $i ] ) )
				$chosen_method = $chosen_methods[ $i ];

			if ( 0 == sizeof( $package['rates'] ) )
				continue;

			$meta = $package['rates'][ $chosen_method ]->get_meta_data();

			$packages[ $i ]['rates'][ $chosen_method ]->cost = $meta['per_one'] * $install + $meta['service'];
		}

		return $packages;
	}

	/**
	 * Меняем label в shipping дропдауне,
	 * добавляем к тексту цену за 1 колесо
	 */
	function wc_cart_shipping_method_full_label( $label, $method ) {
		if ( !is_cart() )
			return $label;
		
		if ( $method->cost <= 0 )
			return $label;

		if ( $this->method != $method->method_id )
			return $label;


		$label = $method->get_label();

		$meta = $method->get_meta_data();
		$per_one = wp_kses_post( wc_price( $meta['per_one'] ) );

		return "$label ($per_one per 1 wheel)";
	}

	protected function parse_value( $value, $key = 0 )
	{
		$value = explode( ':', $value );

		return $value[ $key ];
	}

	protected function is_json( $string ) {
		json_decode( $string );
		return ( json_last_error() == JSON_ERROR_NONE );
	}


}

Module_WC_Shipping_Install_Wheels::get_instance();

function module_wc_need_install_data( $chosen_method, $index ) {
	return Module_WC_Shipping_Install_Wheels::get_instance()->get_need_install_data( $chosen_method, $index );
}


/**
 * Add new shipping method
 */
add_filter( 'woocommerce_shipping_methods', 'module_woocommerce_shipping_methods' );
function module_woocommerce_shipping_methods( $shipping_methods ) {
	$shipping_methods['need_to_install'] = 'WC_Shipping_Need_To_Install';
	return $shipping_methods;
}
