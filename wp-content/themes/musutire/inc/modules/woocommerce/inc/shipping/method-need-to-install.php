<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Free Shipping Method.
 *
 * A simple shipping method for free shipping.
 *
 * @class   WC_Shipping_Free_Shipping
 * @version 2.6.0
 * @package WooCommerce/Classes/Shipping
 * @author  WooThemes
 */
class WC_Shipping_Need_To_Install extends WC_Shipping_Method 
{
	protected $per_one;

	protected $requires;

	/**
	 * Constructor.
	 *
	 * @param int $instance_id Shipping method instance.
	 */
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'need_to_install';
		$this->instance_id        = absint( $instance_id );
		$this->method_title       = __( 'Need to install', 'woocommerce' );
		$this->method_description = __( 'Free Shipping is a special method which can be triggered with coupons and minimum spends.', 'woocommerce' );
		$this->supports           = array(
			'shipping-zones',
			'instance-settings',
			'instance-settings-modal',
		);

		$this->init();
	}

	/**
	 * Initialize free shipping.
	 */
	public function init() {
		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables.
		$this->title        = $this->get_option( 'title' );
		$this->cost	        = $this->get_option( 'cost', 0 );
		$this->per_one      = $this->get_option( 'cost', 0 );
		$this->service_cost = $this->get_option( 'service_cost', 0 );

		// Actions.
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	/**
	 * Init form fields.
	 */
	public function init_form_fields() {
		$fields = array(
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => $this->method_title,
				'desc_tip'    => true,
			),
			'service_cost' => array(
				'title' 		=> __( 'Service cost', 'woocommerce' ),
				'type' 			=> 'text',
				'placeholder'	=> '0',
				'description'	=> __( 'Optional cost for local pickup.', 'woocommerce' ),
				'default'		=> 0,
				'desc_tip'		=> true
			),
			'cost' => array(
				'title' 		=> __( 'Cost per 1 wheel', 'woocommerce' ),
				'type' 			=> 'text',
				'placeholder'	=> '0',
				'description'	=> __( '', 'woocommerce' ),
				'default'		=> 0,
				'desc_tip'		=> true
			),
			'per_one' => array(
				'type'          => 'hidden',
			),
		);

		$this->instance_form_fields = apply_filters( 'module_wc_shipping_need_to_install_fields', $fields );
	}

	/**
	 * Generate Text Input HTML.
	 *
	 * @param  mixed $key
	 * @param  mixed $data
	 * @since  1.0.0
	 * @return string
	 */
	public function generate_custom_field_html( $key, $data ) {
		$html = apply_filters( 'module_wc_shipping_need_to_install_custom_field_html', '', $key, $data, $this );

		if ( $html )
			return $html;

		return $this->generate_text_html( $key, $data );

		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
				<?php echo $this->get_tooltip_html( $data ); ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					<input class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>" type="<?php echo esc_attr( $data['type'] ); ?>" name="<?php echo esc_attr( $field_key ); ?>" id="<?php echo esc_attr( $field_key ); ?>" style="<?php echo esc_attr( $data['css'] ); ?>" value="<?php echo esc_attr( $this->get_option( $key ) ); ?>" placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>" <?php disabled( $data['disabled'], true ); ?> <?php echo $this->get_custom_attribute_html( $data ); ?> />
					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php

		return ob_get_clean();
	}

	/**
	 * See if free shipping is available based on the package and cart.
	 *
	 * @param array $package Shipping package.
	 * @return bool
	 */
	// public function is_available( $package ) {
	// 	$has_coupon         = false;
	// 	$has_met_min_amount = false;

	// 	if ( in_array( $this->requires, array( 'coupon', 'either', 'both' ) ) ) {
	// 		if ( $coupons = WC()->cart->get_coupons() ) {
	// 			foreach ( $coupons as $code => $coupon ) {
	// 				if ( $coupon->is_valid() && $coupon->enable_free_shipping() ) {
	// 					$has_coupon = true;
	// 					break;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	if ( in_array( $this->requires, array( 'min_amount', 'either', 'both' ) ) && isset( WC()->cart->cart_contents_total ) ) {
	// 		$total = WC()->cart->get_displayed_subtotal();

	// 		if ( 'incl' === WC()->cart->tax_display_cart ) {
	// 			$total = $total - ( WC()->cart->get_cart_discount_total() + WC()->cart->get_cart_discount_tax_total() );
	// 		} else {
	// 			$total = $total - WC()->cart->get_cart_discount_total();
	// 		}

	// 		if ( $total >= $this->min_amount ) {
	// 			$has_met_min_amount = true;
	// 		}
	// 	}

	// 	switch ( $this->requires ) {
	// 		case 'min_amount' :
	// 			$is_available = $has_met_min_amount;
	// 			break;
	// 		case 'coupon' :
	// 			$is_available = $has_coupon;
	// 			break;
	// 		case 'both' :
	// 			$is_available = $has_met_min_amount && $has_coupon;
	// 			break;
	// 		case 'either' :
	// 			$is_available = $has_met_min_amount || $has_coupon;
	// 			break;
	// 		default :
	// 			$is_available = true;
	// 			break;
	// 	}

	// 	return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', $is_available, $package );
	// }

	/**
	 * Called to calculate shipping rates for this method. Rates can be added using the add_rate() method.
	 *
	 * @uses WC_Shipping_Method::add_rate()
	 *
	 * @param array $package Shipping package.
	 */
	public function calculate_shipping( $package = array() ) {
		$rate = array(
			'label'     => $this->title,
			'cost'      => $this->cost,
			'meta_data' => array(
				'per_one' => absint( $this->per_one ),
				'service' => absint( $this->service_cost )
			),
			'taxes'     => false,
			'package'   => $package,
		);

		$rate = apply_filters( 'module_wc_shipping_need_to_install_rate', $rate, $this );

		$this->add_rate( $rate );
	}
}
