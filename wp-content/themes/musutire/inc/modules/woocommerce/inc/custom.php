<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function get_product_count_in_cart( $product_id ) {
	$qty = 0;

	foreach ( WC()->cart->get_cart() as $cart_item ) {
		$_product = $cart_item['data'];

		if ( $product_id != $_product->id )
			continue;

		$qty = $cart_item['quantity'];
		break;
	}

	return $qty;
}

function module_wc_is_test() {
	return isset( $_GET['test'] );
}

/**
 * Remove default stylesheets
 *
 * @see https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
// if ( !module_wc_is_test() )
	add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


/**
 * ReRemove all default product list filters excerpt only simple type
 *
 * Used in single admin product
 *
 * @see wc_get_product_types()
 */
add_filter( 'product_type_selector', 'module_product_type_selector' );
function module_product_type_selector( $types = array() ) {
	return array_intersect_key( $types, array_flip( array( 'simple' ) ) );
}


/**
 * Remove all product type options
 */
add_filter( 'product_type_options', 'wc_module_product_type_options' );
function wc_module_product_type_options( $product_type_options ) {
	return array();
}



/**
 * Remove all default product list filters excerpt only price
 */
add_filter( 'woocommerce_catalog_orderby', 'module_woocommerce_catalog_orderby' );
function module_woocommerce_catalog_orderby( $orderby ) {
	return array_intersect_key( $orderby, array_flip( array( 'price', 'price-desc' ) ) );
}

remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

/* remove messages from toggle layout */
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 ); 
remove_action( 'woocommerce_shortcode_before_product_cat_loop', 'wc_print_notices', 10 );
remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 ); 

add_action( 'woocommerce_outside_notices', 'wc_print_notices', 10 );


add_filter( 'woocommerce_product_tabs', 'woo_remove_all_tab', 98);
function woo_remove_all_tab( $tabs ) {
	return array();
}