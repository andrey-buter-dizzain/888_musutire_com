<?php 

add_filter( 'woocommerce_payment_gateways', 'module_woocommerce_payment_gateways' );
function module_woocommerce_payment_gateways( $load_gateways ) {
	$load_gateways[] = 'WC_Gateway_Partial';
	$load_gateways[] = 'WC_Gateway_Due_Date';
	return $load_gateways;
}

// add_action( 'woocommerce_checkout_order_processed', 'module_wc_handle_payment', 10, 2 );
function module_wc_handle_payment( $order_id, $posted ) {
	// var_dump($posted);
	// var_dump($_POST);

}

add_filter( 'woocommerce_available_payment_gateways', 'module_woocommerce_available_payment_gateways' );
function module_woocommerce_available_payment_gateways( $available_gateways ) {
	if ( is_user_logged_in() )
		return $available_gateways;

	if ( !isset( $available_gateways['square'] ) )
		return array();

	return array( 'square' => $available_gateways['square'] );
}