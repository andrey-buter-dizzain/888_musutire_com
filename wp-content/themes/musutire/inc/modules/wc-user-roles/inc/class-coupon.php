<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Связь юзера и выбранного купона у юзера в мета полях
 */
class Module_Roles_WC_User_Coupon
{
	protected $user_id;

	protected $coupon_id;

	protected $coupon_code = '';

	protected $wc_coupon;

	function __construct( $user_id )
	{
		$this->user_id   = $user_id;
		$this->coupon_id = get_user_meta( $user_id, Module_Roles_User_Edit::$coupon_meta_key, true );

		if ( !$this->coupon_id )
			return;

		$this->set_coupon_code();
		$this->set_wc_coupon();
	}

	protected function set_wc_coupon()
	{
		$this->wc_coupon = new WC_Coupon( $this->coupon_code );
	}

	protected function set_coupon_code()
	{
		$post = get_post( $this->coupon_id );

		if ( !$post )
			return;

		$this->coupon_code = $post->post_title;
	}

	public function get_code()
	{
		return $this->coupon_code;
	}

	public function get_amount()
	{
		if ( !$this->coupon_id )
			return;

		return $this->wc_coupon->coupon_amount;
	}

	public function get_discount_amount( $price )
	{
		if ( !$this->coupon_id )
			return;

		return $this->wc_coupon->get_discount_amount( $price );
	}

	public static function get_coupons_array( $output = array() )
	{
		$args = array(
			'posts_per_page' => -1,
			'orderby'        => 'title',
			'order'          => 'asc',
			'post_type'      => 'shop_coupon',
			'post_status'    => 'publish',
		);

		$coupons = get_posts( $args );

		if ( !$coupons )
			return $output;

		foreach ( $coupons as $post ) {
			$amount = get_post_meta( $post->ID, 'coupon_amount', true );

			$output[ $post->ID ] = "{$post->post_title} ($amount%)";
		}

		return $output;
	}
}