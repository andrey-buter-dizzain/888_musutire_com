<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Замена цены для юзера со скидкой.
 * Для выбранного client продавцом на cart page скидка работает по другому:
 * 
 * - Если к залогиненому юзеру прикреплен купон, то цены продуктов отображаются как скидка на продукт. 
 *   И WC записывает в order в price скидочную цену.
 *   
 * - Если seller в cart page выбирает клиента со скидкой, то у него скидка запишется в виде купона в order
 */
class Module_Roles_WC_Price extends Module_WC_User_Roles
{
	protected $current_role = 'not_client';

	protected $user_id = 0;

	protected $coupons_cache = array();

	function __construct()
	{
		add_filter( 'woocommerce_get_sale_price', array( $this, 'get_price' ), 10, 2 );
		add_filter( 'woocommerce_get_price', array( $this, 'get_price' ), 10, 2 );

		$this->init_current_role();
	}

	protected function get_roles_for_price()
	{
		$roles = self::$client_roles;

		if ( defined( 'WC_ROLES_DEBUG' ) AND WC_ROLES_DEBUG )
			$roles['administrator'] = 'Administrator';

		return $roles;
	}

	protected function init_current_role()
	{
		foreach ( $this->get_roles_for_price() as $role => $name ) {
			if ( !current_user_can( $role ) )
				continue;

			$this->current_role = $role;
			$this->user_id      = get_current_user_id();
			break;
		}
	}

	protected function is_checkout_user()
	{
		return ! Module_WC_Checkout_User::inst()->is_default_user();
	}

	protected function is_client()
	{
		$roles = array_keys( $this->get_roles_for_price() );

		return in_array( $this->current_role, $roles );
	}

	function get_price( $price, $product ) 
	{
		if ( !$this->is_client() )
			return $price;

		if ( $this->is_checkout_user() )
			return $price;

		$regular_price = get_post_meta( $product->id, '_regular_price', true );

		if ( !$regular_price )
			return $price;

		$sale_price = $this->get_sale_price( $regular_price, $product->id );

		if ( !$sale_price )
			return $price;

		return $sale_price;
	}

	protected function get_sale_price( $price, $product_id )
	{
		$coupon = $this->get_coupon( $product_id );

		if ( !$coupon->get_code() )
			return;

		$price = $price - $coupon->get_discount_amount( $price );

		return absint( round( $price ) );
	}

	protected function get_coupon( $product_id )
	{
		if ( isset( $this->coupons_cache[ $product_id ] ) AND $this->coupons_cache[ $product_id ] )
			return $this->coupons_cache[ $product_id ];

		$coupon = new Module_Roles_WC_User_Coupon( $this->user_id );

		$this->coupons_cache[ $product_id ] = $coupon;

		return $coupon;
	}
}


