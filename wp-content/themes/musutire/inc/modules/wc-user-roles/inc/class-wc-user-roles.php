<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
 * Добавляются роли клиента и продавца (seller)
 * и редиректы в account page
 */
class Module_WC_User_Roles
{
	protected static $client_roles = array(
		'client_fleet' => 'Fleet Client',
	);

	private $removed_roles = array(
		'client_simple' => 'Simple Client',
		'client_bronze' => 'Bronze Client',
		'client_silver' => 'Silver Client',
		'client_gold'   => 'Gold Client'
	);

	protected static $meta_key = 'client_parent_id';

	protected $seller = 'seller';

	function __construct()
	{
		$this->add_roles();
		$this->remove_roles();

		add_action( 'init' ,      array( $this, 'redirect_wp_login' ) );
		add_action( 'login_init', array( $this, 'redirect_wp_admin' ) );
	}

	function redirect_wp_login()
	{
		global $pagenow;

		if ( is_user_logged_in() )
			return;

		if ( 'wp-login.php' != $pagenow ) 
			return;

		$url = wc_get_page_permalink( 'myaccount' );

		// if ( isset( $_GET['redirect_to'] ) )
		// 	$url = add_query_arg( 'redirect_to', $_GET['redirect_to'], $url );

		wp_redirect( $url );
		exit();
	}

	function redirect_wp_admin()
	{
		if ( is_user_logged_in() )
			return;

		wp_redirect( wc_get_page_permalink( 'myaccount' ) );
		exit();
	}

	public static function get_client_roles()
	{
		return self::$client_roles;
	}

	private function remove_roles()
	{
		foreach ( $this->removed_roles as $role => $name ) {
			if ( !get_role( $role ) )
				continue;

			remove_role( $role );
		}
	}

	/**
	 * Add cusom roles for woccomerce
	 */
	private function add_roles()
	{
		foreach ( self::$client_roles as $role => $name ) {
			add_role( $role, $name, array( 'read' => true, ) );
		}

		add_role(
			$this->seller,
			__( 'Sales representative' ),
			array(
				'read' => true,  // true allows this capability,
				'delete_users'  => true,
				'create_users'  => true,
				'edit_users'    => true,
				'list_users'    => true,
				'promote_users' => false,
				'remove_users'  => true,
				'view_admin_dashboard' => true
			)
		);
	}
}