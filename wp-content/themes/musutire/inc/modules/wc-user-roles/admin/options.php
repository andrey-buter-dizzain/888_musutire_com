<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Populate product meta boxes
 *
 * @package WordPress
 * @subpackage musutire-admin
 */

add_filter( 'populate_theme_options', 'populate_module_wc_roles_options' );
function populate_module_wc_roles_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Woocommerce Roles', 'musutire-admin' ),
		'id'      => $id
	);

	$roles = Module_WC_User_Roles::get_client_roles();

	foreach ( $roles as $role => $name ) {
		// $settings['settings'][] = array(
		// 	'id'      => "{$role}_discount",
		// 	'label'   => "{$name} discount",
		// 	'type'    => 'text',
		// 	'desc'    => '%',
		// 	'section' => $id,
		// );
		$settings['settings'][] = array(
			'id'        => "{$role}_coupon",
			'label'     => "{$name} discount",
			'type'      => 'custom-post-type-select',
			'post_type' => 'shop_coupon',
			'section'   => $id,
		);
	}

	return $settings;
}