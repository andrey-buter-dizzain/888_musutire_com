<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * WC User Roles
 */

if ( !class_exists( 'WooCommerce' ) )
	return;

$path = dirname( __FILE__ );

$name = basename( $path );

if ( !defined( 'WC_ROLES_TMPL' ) )
	define( 'WC_ROLES_TMPL', THEME_MODULE_PATH ."/{$name}/tmpl" );

if ( !defined( 'WC_ROLES_DEBUG' ) )
	define( 'WC_ROLES_DEBUG', is_dev_version() );

// добавляет кастомные роли client_fleet и seller
require_once( "{$path}/inc/class-wc-user-roles.php" );

// для залогиненых seller изменяется поведение списка users в админке - показываются только клиенты of current seller
require_once( "{$path}/admin/class-wp-users-list-table.php" );

// Связь юзера и выбранного купона у юзера в мета полях
require_once( "{$path}/inc/class-coupon.php" );

// Seller может создавать только клиентов из админ панели
require_once( "{$path}/admin/class-user-edit.php" );

// возможность для seller оформить заказ за своего клиента
require_once( "{$path}/inc/class-checkout-user.php" );

new Module_WC_User_Roles;

if ( is_admin() ) {
	require_once( "{$path}/admin/options.php" );

	new Customize_WP_Users_List_Table;
	new Module_Roles_User_Edit;
} else {
	// отображение цен со скидкой для юзера с выбранным купоном
	require_once( "{$path}/inc/class-wc-price.php" );

	new Module_Roles_WC_Price;
}

Module_WC_Checkout_User::inst();