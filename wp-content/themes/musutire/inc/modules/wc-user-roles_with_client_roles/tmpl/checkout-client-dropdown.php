<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

$data = get_theme_part_data();

?>
<div id="js-theme-cart-clients" class="cart-clients-dropdown row">
	<h3 class="col-md-4"><?php _e( 'Choose Client' ) ?></h3>

	<div data-title="<?php _e( 'Choose Client' ) ?>" class="shipping-select-box col-md-8">
		<select name="" id="cart-clients-list">
			<option value=""><?php _e( 'No client' ) ?></option>
			<?php foreach ( $data['users'] as $user ): ?>
				<option value="<?php echo $user->ID ?>" <?php selected( $user->ID, $data['client_id'] ); ?>><?php echo $user->data->display_name ?></option>
			<?php endforeach ?>
		</select>
	</div>
</div>