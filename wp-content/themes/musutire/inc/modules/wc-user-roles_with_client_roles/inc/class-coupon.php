<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_Roles_WC_Coupon
{
	protected $role;

	protected $coupon_id;

	protected $coupon_code = '';

	function __construct( $role )
	{
		$this->role      = $role;
		$this->coupon_id = get_theme_option( "{$this->role}_coupon", 0 );

		if ( !$this->coupon_id )
			return;

		$this->set_coupon_code();
	}

	protected function set_coupon_code()
	{
		$post = get_post( $this->coupon_id );

		if ( !$post )
			return;

		$this->coupon_code = $post->post_title;
	}

	public function get_code()
	{
		return $this->coupon_code;
	}

	public function get_discount_amount( $price )
	{
		$coupon = new WC_Coupon( $this->coupon_code );

		return $coupon->get_discount_amount( $price );
	}
}