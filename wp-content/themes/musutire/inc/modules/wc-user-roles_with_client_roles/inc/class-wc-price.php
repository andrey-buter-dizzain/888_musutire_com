<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_Roles_WC_Price extends Module_WC_User_Roles
{
	protected $current_role = 'not_client';

	function __construct()
	{
		add_filter( 'woocommerce_get_sale_price', array( $this, 'get_price' ), 10, 2 );
		add_filter( 'woocommerce_get_price', array( $this, 'get_price' ), 10, 2 );

		$this->init_current_role();
	}

	protected function init_current_role()
	{
		foreach ( self::$client_roles as $role => $name ) {
			if ( !current_user_can( $role ) )
				continue;

			$this->current_role = $role;
			break;
		}
	}

	protected function is_client()
	{
		return in_array( $this->current_role, array_keys( self::$client_roles ) );
	}

	function get_price( $price, $product ) 
	{
		if ( !$this->is_client() )
			return $price;

		$regular_price = get_post_meta( $product->id, '_regular_price', true );

		if ( !$regular_price )
			return $price;

		$sale_price = $this->get_sale_price( $regular_price );

		if ( !$sale_price )
			return $price;

		return $sale_price;
	}

	protected function get_sale_price( $price )
	{
		$coupon = new Module_Roles_WC_Coupon( $this->current_role );

		if ( !$coupon->get_code() )
			return;

		return $price - $coupon->get_discount_amount( $price );


		// $discount = get_theme_option( "{$this->curren_role}_discount", 0 );
		// $discount = str_replace( '%', '',  $discount );
		// $discount = str_replace( ',', '.', $discount );

		// if ( !$discount )
		// 	return;

		// return round( $discount * $price / 100, 0 );
	}
}
new Module_Roles_WC_Price;

