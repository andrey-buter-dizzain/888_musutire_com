<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * Seller can select own client on checkout page,
 * and all user data in checkout form and checkout process will be custom user data
 *
 * Продавец может выбрать на cart page своего клиента и оформить от его имени заказ
 * Вся инфа в checkout form заполняется custom user data
 * а в checkout process заменятеся user id на custome user id
 */
class Module_WC_Checkout_User
{
	protected $user_type;

	protected $user_id;

	protected $session_key = 'checkout_custom_user';

	protected $discount_html;

	function __construct()
	{
		add_action( 'init', array( $this, 'init' ) );

		/**
		 * Checkout form
		 */

		// set checkout form fields value with custom user data
		add_filter( 'woocommerce_checkout_get_value', array( $this, 'checkout_form_field_get_value' ), 10, 2 );

		/**
		 * Checkout process
		 */

		// set custom user id in checkout process
		add_filter( 'woocommerce_checkout_customer_id', array( $this, 'checkout_customer_id' ), 50 );

		// clear session data at the end of checkout process
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'clear_session_data' ) );

		/**
		 * Cart page
		 */

		add_action( 'woocommerce_cart_totals_before_order_total', array( $this, 'show_clients_dropdown' ) );

		// totals html block
		add_filter( 'woocommerce_coupon_discount_amount_html', array( $this, 'save_coupon_discount_amount_html' ) );
		add_filter( 'woocommerce_cart_totals_coupon_html', array( $this, 'set_cart_totals_coupon_html' ) );
		add_filter( 'woocommerce_cart_totals_coupon_label', array( $this, 'woocommerce_cart_totals_coupon_label' ) );

		// clear coupon messages
		add_filter( 'woocommerce_coupon_message', array( $this, 'clear_woocommerce_coupon_message' ) );
		add_filter( 'woocommerce_coupon_error', array( $this, 'clear_woocommerce_coupon_message' ) );


		// see class-wc-ajax.php
		$ajax_event = 'update_shipping_method';
		
		// Cart page frontend, update shipping dropdown by ajax
		add_action( "wp_ajax_nopriv_woocommerce_{$ajax_event}", array( $this, 'save_client_data' ), 2 );
		add_action( "wc_ajax_{$ajax_event}", array( $this, 'save_client_data' ), 2 );
	}

	/**
	 * Clear coupon message
	 *
	 * Когда добавляется купон через WC()->cart->add_discount()
	 * создается сообщение о статусе добавления,
	 * который потом может отображаться на странице.
	 * Чтоб этого не было, чистим все такие сообщения.
	 */
	function clear_woocommerce_coupon_message()
	{
		return '';
	}

	/**
	 * В cart page и checkout order view выводим нужный лейбл
	 */
	function woocommerce_cart_totals_coupon_label( $label )
	{
		return __( 'Client discount:' );
	}

	/**
	 * По умолчанию в html купона добавляется ссылка на его удалени.
	 * В данном случае она не нужна,
	 * поэтому правим html
	 */
	function set_cart_totals_coupon_html( $html )
	{
		if ( $this->discount_html )
			return $this->discount_html;

		return $html;
	}

	/**
	 * см. self::set_cart_totals_coupon_html()
	 * Здесь первоначальный html, до добавлния ссылки, запоминается
	 */
	function save_coupon_discount_amount_html( $discount_html )
	{
		$this->discount_html = $discount_html;

		return $discount_html;
	}

	/**
	 * WC plugin is set data on init action
	 */
	function init() 
	{
		$this->set_user_type();

		if ( !$this->user_id )
			return;

		if ( $this->is_set_coupon() )
			return;

		$this->set_coupon();
	}

	/**
	 * Cart page frontend, update shipping dropdown by ajax
	 */
	function save_client_data()
	{
		if ( !isset( $_POST['json_shipping_method'] ) )
			return;

		$data = $_POST['json_shipping_method'];

		if ( !isset( $data->client_id ) )
			return;

		$this->set_client_id( $data->client_id );
		$this->set_coupon();
	}

	/**
	 * Set coupon
	 */
	protected function set_coupon()
	{
		if ( $this->is_set_coupon() ) 
			$this->remove_old_coupon();

		$this->set_new_coupon();
	}

	protected function is_set_coupon()
	{
		return !!$this->get_session_data( 'coupon' );
	}

	protected function remove_old_coupon()
	{
		$code = $this->get_session_data( 'coupon' );

		if ( !$code )
			return;

		WC()->cart->remove_coupon( $code );

		$this->update_session_data( 'coupon', '' );
	}

	protected function set_new_coupon()
	{
		// $code = sanitize_text_field( 'yyy' );
		$code = $this->get_coupone_code_by_user_role();

		if ( !$code )
			return;

		WC()->cart->add_discount( $code );

		$this->update_session_data( 'coupon', $code );
	}


	protected function get_coupone_code_by_user_role()
	{
		$user_id = $this->get_client_id();

		if ( !$user_id )
			return;

		$user = get_userdata( $user_id );

		$role = array_shift( $user->roles );

		$coupon = new Module_Roles_WC_Coupon( $role );

		return $coupon->get_code();
	}

	/**
	 * set checkout form fields value with custom user data
	 */
	function checkout_form_field_get_value( $value, $input ) 
	{
		if ( $this->is_default_user() )
			return $value;

		return $this->get_value( $input );
	}

	/**
	 * set custom user id in checkout process
	 */
	function checkout_customer_id( $user_id )
	{
		if ( !$this->user_id )
			return $user_id;

		return $this->user_id;
	}


	/**
	 * Show clients dropdown on the cart page
	 */
	function show_clients_dropdown()
	{
		if ( !$this->show_clients_dopdown() )
			return;

		$seller_id = current_user_can( 'seller' ) ? get_current_user_id() : null;

		$client_ids = Customize_WP_Users_List_Table::get_client_ids( $seller_id );

		if ( !$client_ids )
			return;

		$users = get_users( array(
			'include' => $client_ids
		));

		get_theme_part( WC_ROLES_TMPL, 'checkout-client-dropdown', array( 
			'data' => array(
				'users'     => $users,
				'client_id' => $this->get_client_id()
			)
		) );
	}

	function show_clients_dopdown()
	{
		if ( !is_user_logged_in() )
			return false;

		if ( current_user_can( 'seller' ) )
			return true;

		if ( current_user_can( 'administrator' ) )
			return true;

		return false;
	}

	/**
	 * Set current user type: custom or default
	 * Custom user is client set by seller in checkout page
	 */
	protected function set_user_type()
	{
		$user_id = $this->get_client_id();

		$this->user_type = $user_id ? 'custom' : 'default';

		if ( $this->is_default_user() )
			return;

		$this->user_id = $user_id;
	}

	protected function is_default_user()
	{
		return ( 'default' == $this->user_type );
	}

	/**
	 * See WC_Checkout::get_value()
	 */
	protected function get_value( $input )
	{
		if ( !empty( $_POST[ $input ] ) ) 
			return wc_clean( $_POST[ $input ] );

		$value = get_user_meta( $this->user_id, $input, true );

		if ( $value )
			return $value;

		if ( $input == 'billing_email' ) {
			$user = get_user_by( 'ID', $this->user_id );

			return $user->user_email;
		}
	}

	public function get_client_id()
	{
		return $this->get_session_data( 'client_id' );
	}

	protected function set_client_id( $client_id )
	{
		return $this->update_session_data( 'client_id', absint( $client_id ) );
	}

	protected function update_session_data( $key, $value )
	{
		$data = WC()->session->get( $this->session_key );

		if ( !is_array( $data ) )
			$data = array();

		$data[ $key ] = $value;

		WC()->session->set( $this->session_key, $data );
	}

	protected function get_session_data( $key, $default = null )
	{
		$data = WC()->session->get( $this->session_key );

		return is_array( $data ) ? $data[ $key ] : $default;
	}

	function clear_session_data()
	{
		$key = $this->session_key;

		unset( WC()->session->$key );
	}
}
new Module_WC_Checkout_User;