<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
* 
*/
class Module_Roles_User_Edit extends Module_WC_User_Roles
{
	function __construct()
	{
		add_filter( 'editable_roles', array( $this, 'editable_roles' ), 20 );
		add_filter( 'module_user_meta_fileds', array( $this, 'client_meta_fileds' ) );
		add_action( 'user_register', array( $this, 'user_register' ) );
		add_action( 'admin_init', array( $this, 'admin_init_by_seller' ) );
	}

	/**
	 * On register client!!! auto save client parent id
	 */
	function user_register( $user_id )
	{
		if ( ! current_user_can( 'edit_user' ) )
			return;

		if ( ! isset( $_POST['role'] ) )
			return;

		if ( ! in_array( $_POST['role'], array_keys( self::$client_roles ) ) )
			return;

		update_user_meta( $user_id, self::$meta_key, get_current_user_id() );
	}

	/**
	 * Seller can add new user with allowed roles
	 */
	function editable_roles( $roles )
	{
		if ( !current_user_can( $this->seller ) )
			return $roles;

		$output = array();

		foreach ( self::$client_roles as $role => $name ) {
			if ( !isset( $roles[ $role ] ) )
				continue;

			$output[ $role ] = $roles[ $role ];
		}

		if ( empty( $output ) )
			$output['subscriber'] = $roles['subscriber'];

		return $output;
	}

	function client_meta_fileds( $user_meta )
	{
		$field = $this->get_client_parent_field();

		if ( !$field )
			return $user_meta;

		$user_meta[] = array(
			'title'  => __( 'Seller-Client Relation', 'module-user-meta' ),
			'fields' => array( $field ),
		);

		return $user_meta;
	}

	/**
	 * Used in user profile in wp-admin
	 *
	 * User meta field for seller and other users who can edit user
	 */
	protected function get_client_parent_field()
	{
		if ( ! $this->is_active_client_parent_field() )
			return;

		$user_id = absint( $_REQUEST['user_id'] );

		$is_client = false;

		foreach ( self::$client_roles as $role => $name ) {
			if ( !user_can( $user_id, $role ) )
				continue;

			$is_client = true;
			break;
		}

		if ( !$is_client )
			return;
			
		$field = array(
			'label'   => __( 'Seller is' ),
			'id'      => self::$meta_key,
			'desc'    => '',
			'default' => get_current_user_id()
		);

		if ( current_user_can( 'seller' ) ) {
			// $parent_id = get_user_meta( $user_id, self::$meta_key, true );
			// $current = false;

			// if ( $parent_id == get_current_user_id() ) {
			// 	$parent = wp_get_current_user();
			// 	$cur
			// } else {
			// 	$parent = 
			// }

			$field = array_merge( $field, array(
				'label' => $field['label'] .' '. wp_get_current_user()->data->display_name .' (you)',
				'type'  => 'hidden'
			) );
		} else {
			$field = array_merge( $field, array(
				'type'    => 'select',
				'choices' => $this->get_user_can_be_seller()
			) );
		}

		return $field;
	}

	function admin_init_by_seller()
	{
		if ( 'user-edit.php' != $GLOBALS['pagenow'] )
			return;

		if ( !current_user_can( $this->seller ) )
			return;

		$user_id = $_REQUEST['user_id'];


		if ( $user_id == get_current_user_id() )
			return;

		$parent_id = get_user_meta( $user_id, self::$meta_key, true );

		if ( $parent_id == get_current_user_id() )
			return;

		wp_die( __( 'Sorry, you are not allowed to edit this user.' ) );
	}

	protected function is_active_client_parent_field()
	{
		if ( ! current_user_can( 'edit_user' ) )
			return false;

		$user_id = absint( $_REQUEST['user_id'] );

		if ( ! $user_id )
			return false;

		if ( $user_id == get_current_user_id() )
			return false;

		return true;
	}

	/**
	 * Used on user profile page in wp-admin
	 *
	 * Filter users who can edit users
	 */
	protected function get_user_can_be_seller()
	{
		$users = get_users( array(
			'role__not_in' => array_keys( self::$client_roles ),
		) );

		$output = array();

		foreach ( $users as $key => $user ) {
			if ( !user_can( $user, 'edit_user' ) )
				continue;

			$output[ $user->ID ] = $user->data->display_name;
		}

		return $output;
	}

}

new Module_Roles_User_Edit;