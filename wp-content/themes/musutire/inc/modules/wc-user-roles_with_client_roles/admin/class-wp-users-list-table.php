<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

/**
 * Class to replace wp users list table in wp-admin for seller
 */
class Customize_WP_Users_List_Table extends Module_WC_User_Roles
{
	function __construct()
	{
		add_filter( 'views_users', array( $this, 'wp_list_table_views_users' ) );
		add_filter( 'users_list_table_query_args', array( $this, 'users_list_table_query_args' ) );
	}

	/**
	 * Only used for seller
	 *
	 * Show only seller clients in user list table
	 */
	function users_list_table_query_args( $args )
	{
		if ( ! current_user_can( $this->seller ) )
			return $args;

		$args['role__in']   = array_keys( self::$client_roles );
		$args['meta_key']   = self::$meta_key;
		$args['meta_value'] = get_current_user_id();

		return $args;
	}

	/**
	 * Filter user views bar in users list in wp-admin ( http://joxi.ru/LmGDM6YsR7JnW2 )
	 */
	function wp_list_table_views_users( $views )
	{
		if ( !current_user_can( $this->seller ) )
			return $views;

		$count_users = $this->count_users();

		$regexp = '/\(([0-9]+)\)<\/span>/';

		foreach ( $views as $role => $html ) {
			if ( !isset( $count_users[ $role ] ) ) {
				unset( $views[ $role ] );
				continue;
			}

			$count = $count_users[ $role ];

			$views[ $role ] = preg_replace( $regexp, "($count)</span>", $views[ $role ] );
		}

		return $views;
	}

	/**
	 * See wp function count_users()
	 */
	protected function count_users()
	{
		global $wpdb;

		// Initialize
		$id = get_current_blog_id();
		$blog_prefix = $wpdb->get_blog_prefix($id);
		$result = array(
			'all' => 0
		);

		// Build a CPU-intensive query that will return concise information.
		$select_count = array();

		foreach ( self::$client_roles as $role => $name ) {
			$esc_like = $wpdb->esc_like( '"' . $role . '"' );
			$select_count[] = $wpdb->prepare( "COUNT(NULLIF(`meta_value` LIKE %s, false))", "%{$esc_like}%" );
		}

		$select_count = implode( ', ', $select_count );

		$client_ids = $this->get_related_clients_id();

		if ( !$client_ids )
			return $result;

		$client_ids = implode( ', ', $client_ids );

		// Add the meta_value index to the selection list, then run the query.
		$sql = "SELECT $select_count, COUNT(*) FROM $wpdb->usermeta WHERE meta_key = '{$blog_prefix}capabilities' AND user_id IN( $client_ids )";

		$row = $wpdb->get_row( $sql, ARRAY_N );

		// Run the previous loop again to associate results with role names.
		$col = 0;
		foreach ( self::$client_roles as $role => $name ) {
			$count = (int) $row[ $col++ ];

			if ( $count > 0 )
				$result[ $role ] = $count;
		}

		// Get the meta_value index from the end of the result set.
		$result['all'] = (int) $row[$col];

		return $result;
	}

	protected function get_related_clients_id()
	{
		$current_user_id = get_current_user_id();

		return self::get_client_ids( $current_user_id );
	}

	public static function get_client_ids( $seller_id = null )
	{
		global $wpdb;

		$meta_value = $seller_id ? "AND meta_value = '{$seller_id}'" : '';

		$meta_key = self::$meta_key;

		$sql = "SELECT user_id FROM $wpdb->usermeta WHERE meta_key = '{$meta_key}' $meta_value";

		return $wpdb->get_col( $sql );
	}

}