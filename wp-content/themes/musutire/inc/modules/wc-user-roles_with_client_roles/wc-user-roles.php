<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

if ( !class_exists( 'WooCommerce' ) )
	return;

$path = dirname( __FILE__ );

$name = basename( $path );

if ( !defined( 'WC_ROLES_TMPL' ) )
	define( 'WC_ROLES_TMPL', THEME_MODULE_PATH ."/{$name}/tmpl" );

require_once( "{$path}/inc/class-wc-user-roles.php" );
require_once( "{$path}/admin/class-wp-users-list-table.php" );

if ( is_admin() ) {
	new Customize_WP_Users_List_Table;
	
	require_once( "{$path}/admin/class-user-edit.php" );
	require_once( "{$path}/admin/options.php" );
} else {
	require_once( "{$path}/inc/class-wc-price.php" );
	require_once( "{$path}/inc/class-checkout-user.php" );
	require_once( "{$path}/inc/class-coupon.php" );
}


// add_filter( 'woocommerce_account_menu_items', 'module_roles_woocommerce_account_menu_items' );
// function module_roles_woocommerce_account_menu_items( $items ) {

// 	return $items;
// }

