<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'woocommerce_square_payment_input_styles', 'change_payment_input_styles' );
function change_payment_input_styles() {
	$styles = array(
		array(
			'fontSize'        => '16px',
			'padding'         => '0 20px',
			'fontWeight'      => 400,
			'backgroundColor' => 'transparent',
			'lineHeight'      => '38px',
			'color' 		  => 'rgb(0,0,0)'
		),
		array(
			'mediaMaxWidth' => '1200px',
			'fontSize'      => '16px'
		)
	);

	return wp_json_encode( $styles );
}