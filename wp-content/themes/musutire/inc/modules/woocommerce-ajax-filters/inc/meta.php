<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_module_wc_ajax_filters_meta' );
function populate_module_wc_ajax_filters_meta( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_filters_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Dropdown filters', 'musutire-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'id'       => 'front-dropdowns-title',
				'label'    => __( 'Title', 'musutire-admin' ),
				'type'     => 'text',
			),
			array(
				'id'       => 'front-dropdowns',
				'label'    => __( 'Set dropdowns', 'musutire-admin' ),
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Select attribute', 'musutire-admin' ),
						'id'    => 'taxonomy',
						'type'  => 'wc_attributes_select',
					),
				),
			)
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}