<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

// add_filter( 'populate_theme_options', 'populate_module_wc_ajax_filters' );
function populate_module_wc_ajax_filters( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Front dropdowns', 'musutire-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'       => 'front-dropdowns',
		'label'    => __( 'Set dropdowns', 'musutire-admin' ),
		'type'     => 'list-item',
		'settings' => array(
			array(
				'label' => __( 'Select attribute', 'nrec-admin' ),
				'id'    => 'attr',
				'type'  => 'wc_attributes_select',
			),
		),
		'wpmu'    => false,
		'section' => $id,
	);

	return $settings;
}