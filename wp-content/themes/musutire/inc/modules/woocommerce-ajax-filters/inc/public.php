<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_action( 'wp_enqueue_scripts', 'module_wc_ajax_filters_wp_enqueue_scripts', 20 );
function module_wc_ajax_filters_wp_enqueue_scripts() {
	wp_dequeue_style( 'berocket_aapf_widget-style' );
}


if ( !is_admin() )
	add_action( 'wp_footer', 'module_wc_ajax_filters_wp_footer', 1 );
function module_wc_ajax_filters_wp_footer() {
	wp_add_inline_script( 
		'berocket_aapf_widget-script', 
		"
			the_ajax_script.user_func.after_update = \"jQuery('body').trigger('module_wc_ajax_filter.after_update');\";
			the_ajax_script.user_func.before_update = \"jQuery('body').trigger('module_wc_ajax_filter.before_update');\";
			the_ajax_script.user_func.on_update = \"jQuery('body').trigger('module_wc_ajax_filter.on_update');\";
		" 
	);

	if ( !is_shop() )
		return;

	wp_localize_script( 'common-js', 'wcAjaxFilters', array(
		'cookieKey' => Module_WC_Ajax_Filters::COOKIE_KEY,
	) );
}

function module_wc_filters_get_url() {
	return Module_WC_Ajax_Filters::get_instance()->get_url();
}

function module_wc_filters_get_terms() {
	return Module_WC_Ajax_Filters::get_instance()->get_terms();
}