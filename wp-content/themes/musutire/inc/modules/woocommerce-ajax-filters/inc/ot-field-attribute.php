<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

	
function ot_type_wc_attributes_select( $args = array() ) {

	/* turns arguments array into variables */
	extract( $args );

	/* verify a description */
	$has_desc = $field_desc ? true : false;

?>
	
	<?php /* format setting outer wrapper */ ?>
	<div class="format-setting type-category-select <?php echo ( $has_desc ? 'has-desc' : 'no-desc' ) ?>">
		
		<?php /* description */ ?>
		<?php if ( $has_desc ): ?>
			<div class="description"><?php echo htmlspecialchars_decode( $field_desc ) ?></div>
		<?php endif ?>
		
		<?php /* format setting inner wrapper */ ?>
		<div class="format-setting-inner">
		
			<?php /* build category */ ?>
			<select name="<?php echo esc_attr( $field_name ) ?>" id="<?php echo esc_attr( $field_id ) ?>" class="option-tree-ui-select <?php echo $field_class ?>">
			
				<?php /* get category array */ ?>
				<?php $taxes = wc_get_attribute_taxonomies(); ?>
				
				<?php /* has cats */ ?>
				<?php if ( $taxes ): ?>
					<option value="">-- <?php echo __( 'Choose One', 'option-tree' ) ?> --</option>

					<?php foreach ( $taxes as $tax ): 
						$tax_key = wc_attribute_taxonomy_name( $tax->attribute_name );
					?>
						<option value="<?php echo esc_attr( $tax_key ) ?>" <?php selected( $field_value, $tax_key, true ) ?>><?php echo $tax->attribute_label ?></option>
					<?php endforeach ?>
				<?php else: ?>
					<option value=""><?php echo __( 'No Attributes Found', 'option-tree' ) ?></option>
				<?php endif ?>
		
			</select>
		
		</div>
	
	</div>
<?php 
	
}
	
