<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_WC_Ajax_Filters
{
	protected static $instance;

	protected $url = '';

	protected $url_query = array();

	const COOKIE_KEY = 'shop-last-url';

	protected function __construct()
	{
		add_action( 'template_redirect', array( $this, 'set_data' ) );
	}

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	function set_data()
	{
		$this->set_data_to_cookie();
		$this->set_data_from_cookies();
	}

	protected function set_data_to_cookie()
	{
		if ( !is_shop() )
			return;

		$_COOKIE[ self::COOKIE_KEY ] = current_url();
	}

	protected function set_data_from_cookies()
	{
		if ( !is_product() )
			return;

		$this->set_url();
		$this->parse_url_query();
	}

	protected function set_url()
	{
		if ( isset( $_COOKIE[ self::COOKIE_KEY ] ) AND $_COOKIE[ self::COOKIE_KEY ] )
			$this->url = $_COOKIE[ self::COOKIE_KEY ];
		else
			$this->url = get_permalink( woocommerce_get_page_id( 'shop' ) );
	}

	protected function parse_url_query()
	{
		$query = parse_url( $this->get_url(), PHP_URL_QUERY );

		if ( !$query )
			return;

		parse_str( $query, $query );

		if ( !isset( $query['filters'] ) )
			return;

		$filters = explode( "|", $query['filters'] );

		$data = array();

		unset( $filters['order'] );
		unset( $filters['price'] );

		foreach ( $filters as $filter ) {
			if ( preg_match( "/([^\[]+)\[([^\]]+)\]/", $filter, $matches ) ) {
				list( $temp, $attr, $value ) = $matches;

				if ( preg_match_all( "/[0-9]+/", $value, $matches ) )
					$value = $matches[0];
			} else {
				list( $attr, $value ) = explode( "-", $filter, 2 );
			}

			$data["pa_{$attr}"] = $value;
		}

		$this->url_query = $data;
	}

	public function get_terms()
	{
		$output = array();

		if ( !$this->url_query )
			return $output;

		foreach ( $this->url_query as $taxonomy => $term_ids ) {
			$terms = get_terms( $taxonomy, array(
				'include' => $term_ids,
			) );

			if ( is_wp_error( $terms ) )
				continue;

			$output[ $taxonomy ] = $terms;
		}

		return $output;
	}

	public function get_url()
	{
		return $this->url;
	}
}

Module_WC_Ajax_Filters::get_instance();