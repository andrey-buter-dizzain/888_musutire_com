<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( !class_exists( 'WooCommerce' ) )
	return;

if ( !class_exists( 'BeRocket_AAPF' ) )
	return;

$path = dirname( __FILE__ );

require_once( "{$path}/inc/public.php" );
require_once( "{$path}/inc/shop-query.class.php" );
// require_once( "{$path}/inc/options.php" );
require_once( "{$path}/inc/meta.php" );
require_once( "{$path}/inc/ot-field-attribute.php" );