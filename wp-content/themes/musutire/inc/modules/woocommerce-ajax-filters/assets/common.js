(function(){
	if ( 'undefined' == typeof wcAjaxFilters ) 
		return;

	setOrderHandler();
	setLocation();

	$('body').on( 'module_wc_ajax_filter.after_update', function() {
		if ( 'function' != typeof $.cookie ) 
			return;

		setLocation();
		setOrderHandler();

		$(window).scrollTop( 0 );
	});

	function setOrderHandler() {
		$('#js-wc-order-by-price').click(function(e){
			var $el = $(this);

			$el.removeClass( $el.data('prevOrder') ).addClass( $el.data('order') );

			$el.next().val( $el.data('order') ).trigger('change');
		});
	}

	/**
	 * Save current url after update posts
	 * Used in single product back link
	 */
	function setLocation() {
		updateCookie( wcAjaxFilters.cookieKey, window.location.href, 365 );
	}
})();