/*!
 * Responsive Bootstrap Toolkit
 * Author:    Maciej Gurban
 * License:   MIT
 * Version:   2.5.1 (2015-11-02)
 * Origin:    https://github.com/maciej-gurban/responsive-bootstrap-toolkit
 */
;var ResponsiveBootstrapToolkit=function(i){var e={detectionDivs:{bootstrap:{xs:i('<div class="device-xs visible-xs visible-xs-block"></div>'),sm:i('<div class="device-sm visible-sm visible-sm-block"></div>'),md:i('<div class="device-md visible-md visible-md-block"></div>'),lg:i('<div class="device-lg visible-lg visible-lg-block"></div>')},foundation:{small:i('<div class="device-xs show-for-small-only"></div>'),medium:i('<div class="device-sm show-for-medium-only"></div>'),large:i('<div class="device-md show-for-large-only"></div>'),xlarge:i('<div class="device-lg show-for-xlarge-only"></div>')}},applyDetectionDivs:function(){i(document).ready(function(){i.each(o.breakpoints,function(i){o.breakpoints[i].appendTo(".responsive-bootstrap-toolkit")})})},isAnExpression:function(i){return"<"==i.charAt(0)||">"==i.charAt(0)},splitExpression:function(i){var e=i.charAt(0),o="="==i.charAt(1)?!0:!1,s=1+(o?1:0),n=i.slice(s);return{operator:e,orEqual:o,breakpointName:n}},isAnyActive:function(e){var s=!1;return i.each(e,function(i,e){return o.breakpoints[e].is(":visible")?(s=!0,!1):void 0}),s},isMatchingExpression:function(i){var s=e.splitExpression(i),n=Object.keys(o.breakpoints),r=n.indexOf(s.breakpointName);if(-1!==r){var t=0,a=0;"<"==s.operator&&(t=0,a=s.orEqual?++r:r),">"==s.operator&&(t=s.orEqual?r:++r,a=void 0);var c=n.slice(t,a);return e.isAnyActive(c)}}},o={interval:300,framework:null,breakpoints:null,is:function(i){return e.isAnExpression(i)?e.isMatchingExpression(i):o.breakpoints[i]&&o.breakpoints[i].is(":visible")},use:function(i,s){o.framework=i.toLowerCase(),o.breakpoints="bootstrap"===o.framework||"foundation"===o.framework?e.detectionDivs[o.framework]:s,e.applyDetectionDivs()},current:function(){var e="unrecognized";return i.each(o.breakpoints,function(i){o.is(i)&&(e=i)}),e},changed:function(i,e){var s;return function(){clearTimeout(s),s=setTimeout(function(){i()},e||o.interval)}}};return i(document).ready(function(){i('<div class="responsive-bootstrap-toolkit"></div>').appendTo("body")}),null===o.framework&&o.use("bootstrap"),o}(jQuery);

/*!
 * @fileOverview TouchSwipe - jQuery Plugin
 * @version 1.6.18
 *
 * @author Matt Bryson http://www.github.com/mattbryson
 * @see https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
 * @see http://labs.rampinteractive.co.uk/touchSwipe/
 * @see http://plugins.jquery.com/project/touchSwipe
 * @license
 * Copyright (c) 2010-2015 Matt Bryson
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 */
!function(factory){"function"==typeof define&&define.amd&&define.amd.jQuery?define(["jquery"],factory):factory("undefined"!=typeof module&&module.exports?require("jquery"):jQuery)}(function($){"use strict";function init(options){return!options||void 0!==options.allowPageScroll||void 0===options.swipe&&void 0===options.swipeStatus||(options.allowPageScroll=NONE),void 0!==options.click&&void 0===options.tap&&(options.tap=options.click),options||(options={}),options=$.extend({},$.fn.swipe.defaults,options),this.each(function(){var $this=$(this),plugin=$this.data(PLUGIN_NS);plugin||(plugin=new TouchSwipe(this,options),$this.data(PLUGIN_NS,plugin))})}function TouchSwipe(element,options){function touchStart(jqEvent){if(!(getTouchInProgress()||$(jqEvent.target).closest(options.excludedElements,$element).length>0)){var event=jqEvent.originalEvent?jqEvent.originalEvent:jqEvent;if(!event.pointerType||"mouse"!=event.pointerType||0!=options.fallbackToMouseEvents){var ret,touches=event.touches,evt=touches?touches[0]:event;return phase=PHASE_START,touches?fingerCount=touches.length:options.preventDefaultEvents!==!1&&jqEvent.preventDefault(),distance=0,direction=null,currentDirection=null,pinchDirection=null,duration=0,startTouchesDistance=0,endTouchesDistance=0,pinchZoom=1,pinchDistance=0,maximumsMap=createMaximumsData(),cancelMultiFingerRelease(),createFingerData(0,evt),!touches||fingerCount===options.fingers||options.fingers===ALL_FINGERS||hasPinches()?(startTime=getTimeStamp(),2==fingerCount&&(createFingerData(1,touches[1]),startTouchesDistance=endTouchesDistance=calculateTouchesDistance(fingerData[0].start,fingerData[1].start)),(options.swipeStatus||options.pinchStatus)&&(ret=triggerHandler(event,phase))):ret=!1,ret===!1?(phase=PHASE_CANCEL,triggerHandler(event,phase),ret):(options.hold&&(holdTimeout=setTimeout($.proxy(function(){$element.trigger("hold",[event.target]),options.hold&&(ret=options.hold.call($element,event,event.target))},this),options.longTapThreshold)),setTouchInProgress(!0),null)}}}function touchMove(jqEvent){var event=jqEvent.originalEvent?jqEvent.originalEvent:jqEvent;if(phase!==PHASE_END&&phase!==PHASE_CANCEL&&!inMultiFingerRelease()){var ret,touches=event.touches,evt=touches?touches[0]:event,currentFinger=updateFingerData(evt);if(endTime=getTimeStamp(),touches&&(fingerCount=touches.length),options.hold&&clearTimeout(holdTimeout),phase=PHASE_MOVE,2==fingerCount&&(0==startTouchesDistance?(createFingerData(1,touches[1]),startTouchesDistance=endTouchesDistance=calculateTouchesDistance(fingerData[0].start,fingerData[1].start)):(updateFingerData(touches[1]),endTouchesDistance=calculateTouchesDistance(fingerData[0].end,fingerData[1].end),pinchDirection=calculatePinchDirection(fingerData[0].end,fingerData[1].end)),pinchZoom=calculatePinchZoom(startTouchesDistance,endTouchesDistance),pinchDistance=Math.abs(startTouchesDistance-endTouchesDistance)),fingerCount===options.fingers||options.fingers===ALL_FINGERS||!touches||hasPinches()){if(direction=calculateDirection(currentFinger.start,currentFinger.end),currentDirection=calculateDirection(currentFinger.last,currentFinger.end),validateDefaultEvent(jqEvent,currentDirection),distance=calculateDistance(currentFinger.start,currentFinger.end),duration=calculateDuration(),setMaxDistance(direction,distance),ret=triggerHandler(event,phase),!options.triggerOnTouchEnd||options.triggerOnTouchLeave){var inBounds=!0;if(options.triggerOnTouchLeave){var bounds=getbounds(this);inBounds=isInBounds(currentFinger.end,bounds)}!options.triggerOnTouchEnd&&inBounds?phase=getNextPhase(PHASE_MOVE):options.triggerOnTouchLeave&&!inBounds&&(phase=getNextPhase(PHASE_END)),phase!=PHASE_CANCEL&&phase!=PHASE_END||triggerHandler(event,phase)}}else phase=PHASE_CANCEL,triggerHandler(event,phase);ret===!1&&(phase=PHASE_CANCEL,triggerHandler(event,phase))}}function touchEnd(jqEvent){var event=jqEvent.originalEvent?jqEvent.originalEvent:jqEvent,touches=event.touches;if(touches){if(touches.length&&!inMultiFingerRelease())return startMultiFingerRelease(event),!0;if(touches.length&&inMultiFingerRelease())return!0}return inMultiFingerRelease()&&(fingerCount=fingerCountAtRelease),endTime=getTimeStamp(),duration=calculateDuration(),didSwipeBackToCancel()||!validateSwipeDistance()?(phase=PHASE_CANCEL,triggerHandler(event,phase)):options.triggerOnTouchEnd||options.triggerOnTouchEnd===!1&&phase===PHASE_MOVE?(options.preventDefaultEvents!==!1&&jqEvent.preventDefault(),phase=PHASE_END,triggerHandler(event,phase)):!options.triggerOnTouchEnd&&hasTap()?(phase=PHASE_END,triggerHandlerForGesture(event,phase,TAP)):phase===PHASE_MOVE&&(phase=PHASE_CANCEL,triggerHandler(event,phase)),setTouchInProgress(!1),null}function touchCancel(){fingerCount=0,endTime=0,startTime=0,startTouchesDistance=0,endTouchesDistance=0,pinchZoom=1,cancelMultiFingerRelease(),setTouchInProgress(!1)}function touchLeave(jqEvent){var event=jqEvent.originalEvent?jqEvent.originalEvent:jqEvent;options.triggerOnTouchLeave&&(phase=getNextPhase(PHASE_END),triggerHandler(event,phase))}function removeListeners(){$element.unbind(START_EV,touchStart),$element.unbind(CANCEL_EV,touchCancel),$element.unbind(MOVE_EV,touchMove),$element.unbind(END_EV,touchEnd),LEAVE_EV&&$element.unbind(LEAVE_EV,touchLeave),setTouchInProgress(!1)}function getNextPhase(currentPhase){var nextPhase=currentPhase,validTime=validateSwipeTime(),validDistance=validateSwipeDistance(),didCancel=didSwipeBackToCancel();return!validTime||didCancel?nextPhase=PHASE_CANCEL:!validDistance||currentPhase!=PHASE_MOVE||options.triggerOnTouchEnd&&!options.triggerOnTouchLeave?!validDistance&&currentPhase==PHASE_END&&options.triggerOnTouchLeave&&(nextPhase=PHASE_CANCEL):nextPhase=PHASE_END,nextPhase}function triggerHandler(event,phase){var ret,touches=event.touches;return(didSwipe()||hasSwipes())&&(ret=triggerHandlerForGesture(event,phase,SWIPE)),(didPinch()||hasPinches())&&ret!==!1&&(ret=triggerHandlerForGesture(event,phase,PINCH)),didDoubleTap()&&ret!==!1?ret=triggerHandlerForGesture(event,phase,DOUBLE_TAP):didLongTap()&&ret!==!1?ret=triggerHandlerForGesture(event,phase,LONG_TAP):didTap()&&ret!==!1&&(ret=triggerHandlerForGesture(event,phase,TAP)),phase===PHASE_CANCEL&&touchCancel(event),phase===PHASE_END&&(touches?touches.length||touchCancel(event):touchCancel(event)),ret}function triggerHandlerForGesture(event,phase,gesture){var ret;if(gesture==SWIPE){if($element.trigger("swipeStatus",[phase,direction||null,distance||0,duration||0,fingerCount,fingerData,currentDirection]),options.swipeStatus&&(ret=options.swipeStatus.call($element,event,phase,direction||null,distance||0,duration||0,fingerCount,fingerData,currentDirection),ret===!1))return!1;if(phase==PHASE_END&&validateSwipe()){if(clearTimeout(singleTapTimeout),clearTimeout(holdTimeout),$element.trigger("swipe",[direction,distance,duration,fingerCount,fingerData,currentDirection]),options.swipe&&(ret=options.swipe.call($element,event,direction,distance,duration,fingerCount,fingerData,currentDirection),ret===!1))return!1;switch(direction){case LEFT:$element.trigger("swipeLeft",[direction,distance,duration,fingerCount,fingerData,currentDirection]),options.swipeLeft&&(ret=options.swipeLeft.call($element,event,direction,distance,duration,fingerCount,fingerData,currentDirection));break;case RIGHT:$element.trigger("swipeRight",[direction,distance,duration,fingerCount,fingerData,currentDirection]),options.swipeRight&&(ret=options.swipeRight.call($element,event,direction,distance,duration,fingerCount,fingerData,currentDirection));break;case UP:$element.trigger("swipeUp",[direction,distance,duration,fingerCount,fingerData,currentDirection]),options.swipeUp&&(ret=options.swipeUp.call($element,event,direction,distance,duration,fingerCount,fingerData,currentDirection));break;case DOWN:$element.trigger("swipeDown",[direction,distance,duration,fingerCount,fingerData,currentDirection]),options.swipeDown&&(ret=options.swipeDown.call($element,event,direction,distance,duration,fingerCount,fingerData,currentDirection))}}}if(gesture==PINCH){if($element.trigger("pinchStatus",[phase,pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData]),options.pinchStatus&&(ret=options.pinchStatus.call($element,event,phase,pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData),ret===!1))return!1;if(phase==PHASE_END&&validatePinch())switch(pinchDirection){case IN:$element.trigger("pinchIn",[pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData]),options.pinchIn&&(ret=options.pinchIn.call($element,event,pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData));break;case OUT:$element.trigger("pinchOut",[pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData]),options.pinchOut&&(ret=options.pinchOut.call($element,event,pinchDirection||null,pinchDistance||0,duration||0,fingerCount,pinchZoom,fingerData))}}return gesture==TAP?phase!==PHASE_CANCEL&&phase!==PHASE_END||(clearTimeout(singleTapTimeout),clearTimeout(holdTimeout),hasDoubleTap()&&!inDoubleTap()?(doubleTapStartTime=getTimeStamp(),singleTapTimeout=setTimeout($.proxy(function(){doubleTapStartTime=null,$element.trigger("tap",[event.target]),options.tap&&(ret=options.tap.call($element,event,event.target))},this),options.doubleTapThreshold)):(doubleTapStartTime=null,$element.trigger("tap",[event.target]),options.tap&&(ret=options.tap.call($element,event,event.target)))):gesture==DOUBLE_TAP?phase!==PHASE_CANCEL&&phase!==PHASE_END||(clearTimeout(singleTapTimeout),clearTimeout(holdTimeout),doubleTapStartTime=null,$element.trigger("doubletap",[event.target]),options.doubleTap&&(ret=options.doubleTap.call($element,event,event.target))):gesture==LONG_TAP&&(phase!==PHASE_CANCEL&&phase!==PHASE_END||(clearTimeout(singleTapTimeout),doubleTapStartTime=null,$element.trigger("longtap",[event.target]),options.longTap&&(ret=options.longTap.call($element,event,event.target)))),ret}function validateSwipeDistance(){var valid=!0;return null!==options.threshold&&(valid=distance>=options.threshold),valid}function didSwipeBackToCancel(){var cancelled=!1;return null!==options.cancelThreshold&&null!==direction&&(cancelled=getMaxDistance(direction)-distance>=options.cancelThreshold),cancelled}function validatePinchDistance(){return null!==options.pinchThreshold?pinchDistance>=options.pinchThreshold:!0}function validateSwipeTime(){var result;return result=options.maxTimeThreshold?!(duration>=options.maxTimeThreshold):!0}function validateDefaultEvent(jqEvent,direction){if(options.preventDefaultEvents!==!1)if(options.allowPageScroll===NONE)jqEvent.preventDefault();else{var auto=options.allowPageScroll===AUTO;switch(direction){case LEFT:(options.swipeLeft&&auto||!auto&&options.allowPageScroll!=HORIZONTAL)&&jqEvent.preventDefault();break;case RIGHT:(options.swipeRight&&auto||!auto&&options.allowPageScroll!=HORIZONTAL)&&jqEvent.preventDefault();break;case UP:(options.swipeUp&&auto||!auto&&options.allowPageScroll!=VERTICAL)&&jqEvent.preventDefault();break;case DOWN:(options.swipeDown&&auto||!auto&&options.allowPageScroll!=VERTICAL)&&jqEvent.preventDefault();break;case NONE:}}}function validatePinch(){var hasCorrectFingerCount=validateFingers(),hasEndPoint=validateEndPoint(),hasCorrectDistance=validatePinchDistance();return hasCorrectFingerCount&&hasEndPoint&&hasCorrectDistance}function hasPinches(){return!!(options.pinchStatus||options.pinchIn||options.pinchOut)}function didPinch(){return!(!validatePinch()||!hasPinches())}function validateSwipe(){var hasValidTime=validateSwipeTime(),hasValidDistance=validateSwipeDistance(),hasCorrectFingerCount=validateFingers(),hasEndPoint=validateEndPoint(),didCancel=didSwipeBackToCancel(),valid=!didCancel&&hasEndPoint&&hasCorrectFingerCount&&hasValidDistance&&hasValidTime;return valid}function hasSwipes(){return!!(options.swipe||options.swipeStatus||options.swipeLeft||options.swipeRight||options.swipeUp||options.swipeDown)}function didSwipe(){return!(!validateSwipe()||!hasSwipes())}function validateFingers(){return fingerCount===options.fingers||options.fingers===ALL_FINGERS||!SUPPORTS_TOUCH}function validateEndPoint(){return 0!==fingerData[0].end.x}function hasTap(){return!!options.tap}function hasDoubleTap(){return!!options.doubleTap}function hasLongTap(){return!!options.longTap}function validateDoubleTap(){if(null==doubleTapStartTime)return!1;var now=getTimeStamp();return hasDoubleTap()&&now-doubleTapStartTime<=options.doubleTapThreshold}function inDoubleTap(){return validateDoubleTap()}function validateTap(){return(1===fingerCount||!SUPPORTS_TOUCH)&&(isNaN(distance)||distance<options.threshold)}function validateLongTap(){return duration>options.longTapThreshold&&DOUBLE_TAP_THRESHOLD>distance}function didTap(){return!(!validateTap()||!hasTap())}function didDoubleTap(){return!(!validateDoubleTap()||!hasDoubleTap())}function didLongTap(){return!(!validateLongTap()||!hasLongTap())}function startMultiFingerRelease(event){previousTouchEndTime=getTimeStamp(),fingerCountAtRelease=event.touches.length+1}function cancelMultiFingerRelease(){previousTouchEndTime=0,fingerCountAtRelease=0}function inMultiFingerRelease(){var withinThreshold=!1;if(previousTouchEndTime){var diff=getTimeStamp()-previousTouchEndTime;diff<=options.fingerReleaseThreshold&&(withinThreshold=!0)}return withinThreshold}function getTouchInProgress(){return!($element.data(PLUGIN_NS+"_intouch")!==!0)}function setTouchInProgress(val){$element&&(val===!0?($element.bind(MOVE_EV,touchMove),$element.bind(END_EV,touchEnd),LEAVE_EV&&$element.bind(LEAVE_EV,touchLeave)):($element.unbind(MOVE_EV,touchMove,!1),$element.unbind(END_EV,touchEnd,!1),LEAVE_EV&&$element.unbind(LEAVE_EV,touchLeave,!1)),$element.data(PLUGIN_NS+"_intouch",val===!0))}function createFingerData(id,evt){var f={start:{x:0,y:0},last:{x:0,y:0},end:{x:0,y:0}};return f.start.x=f.last.x=f.end.x=evt.pageX||evt.clientX,f.start.y=f.last.y=f.end.y=evt.pageY||evt.clientY,fingerData[id]=f,f}function updateFingerData(evt){var id=void 0!==evt.identifier?evt.identifier:0,f=getFingerData(id);return null===f&&(f=createFingerData(id,evt)),f.last.x=f.end.x,f.last.y=f.end.y,f.end.x=evt.pageX||evt.clientX,f.end.y=evt.pageY||evt.clientY,f}function getFingerData(id){return fingerData[id]||null}function setMaxDistance(direction,distance){direction!=NONE&&(distance=Math.max(distance,getMaxDistance(direction)),maximumsMap[direction].distance=distance)}function getMaxDistance(direction){return maximumsMap[direction]?maximumsMap[direction].distance:void 0}function createMaximumsData(){var maxData={};return maxData[LEFT]=createMaximumVO(LEFT),maxData[RIGHT]=createMaximumVO(RIGHT),maxData[UP]=createMaximumVO(UP),maxData[DOWN]=createMaximumVO(DOWN),maxData}function createMaximumVO(dir){return{direction:dir,distance:0}}function calculateDuration(){return endTime-startTime}function calculateTouchesDistance(startPoint,endPoint){var diffX=Math.abs(startPoint.x-endPoint.x),diffY=Math.abs(startPoint.y-endPoint.y);return Math.round(Math.sqrt(diffX*diffX+diffY*diffY))}function calculatePinchZoom(startDistance,endDistance){var percent=endDistance/startDistance*1;return percent.toFixed(2)}function calculatePinchDirection(){return 1>pinchZoom?OUT:IN}function calculateDistance(startPoint,endPoint){return Math.round(Math.sqrt(Math.pow(endPoint.x-startPoint.x,2)+Math.pow(endPoint.y-startPoint.y,2)))}function calculateAngle(startPoint,endPoint){var x=startPoint.x-endPoint.x,y=endPoint.y-startPoint.y,r=Math.atan2(y,x),angle=Math.round(180*r/Math.PI);return 0>angle&&(angle=360-Math.abs(angle)),angle}function calculateDirection(startPoint,endPoint){if(comparePoints(startPoint,endPoint))return NONE;var angle=calculateAngle(startPoint,endPoint);return 45>=angle&&angle>=0?LEFT:360>=angle&&angle>=315?LEFT:angle>=135&&225>=angle?RIGHT:angle>45&&135>angle?DOWN:UP}function getTimeStamp(){var now=new Date;return now.getTime()}function getbounds(el){el=$(el);var offset=el.offset(),bounds={left:offset.left,right:offset.left+el.outerWidth(),top:offset.top,bottom:offset.top+el.outerHeight()};return bounds}function isInBounds(point,bounds){return point.x>bounds.left&&point.x<bounds.right&&point.y>bounds.top&&point.y<bounds.bottom}function comparePoints(pointA,pointB){return pointA.x==pointB.x&&pointA.y==pointB.y}var options=$.extend({},options),useTouchEvents=SUPPORTS_TOUCH||SUPPORTS_POINTER||!options.fallbackToMouseEvents,START_EV=useTouchEvents?SUPPORTS_POINTER?SUPPORTS_POINTER_IE10?"MSPointerDown":"pointerdown":"touchstart":"mousedown",MOVE_EV=useTouchEvents?SUPPORTS_POINTER?SUPPORTS_POINTER_IE10?"MSPointerMove":"pointermove":"touchmove":"mousemove",END_EV=useTouchEvents?SUPPORTS_POINTER?SUPPORTS_POINTER_IE10?"MSPointerUp":"pointerup":"touchend":"mouseup",LEAVE_EV=useTouchEvents?SUPPORTS_POINTER?"mouseleave":null:"mouseleave",CANCEL_EV=SUPPORTS_POINTER?SUPPORTS_POINTER_IE10?"MSPointerCancel":"pointercancel":"touchcancel",distance=0,direction=null,currentDirection=null,duration=0,startTouchesDistance=0,endTouchesDistance=0,pinchZoom=1,pinchDistance=0,pinchDirection=0,maximumsMap=null,$element=$(element),phase="start",fingerCount=0,fingerData={},startTime=0,endTime=0,previousTouchEndTime=0,fingerCountAtRelease=0,doubleTapStartTime=0,singleTapTimeout=null,holdTimeout=null;try{$element.bind(START_EV,touchStart),$element.bind(CANCEL_EV,touchCancel)}catch(e){$.error("events not supported "+START_EV+","+CANCEL_EV+" on jQuery.swipe")}this.enable=function(){return this.disable(),$element.bind(START_EV,touchStart),$element.bind(CANCEL_EV,touchCancel),$element},this.disable=function(){return removeListeners(),$element},this.destroy=function(){removeListeners(),$element.data(PLUGIN_NS,null),$element=null},this.option=function(property,value){if("object"==typeof property)options=$.extend(options,property);else if(void 0!==options[property]){if(void 0===value)return options[property];options[property]=value}else{if(!property)return options;$.error("Option "+property+" does not exist on jQuery.swipe.options")}return null}}var VERSION="1.6.18",LEFT="left",RIGHT="right",UP="up",DOWN="down",IN="in",OUT="out",NONE="none",AUTO="auto",SWIPE="swipe",PINCH="pinch",TAP="tap",DOUBLE_TAP="doubletap",LONG_TAP="longtap",HORIZONTAL="horizontal",VERTICAL="vertical",ALL_FINGERS="all",DOUBLE_TAP_THRESHOLD=10,PHASE_START="start",PHASE_MOVE="move",PHASE_END="end",PHASE_CANCEL="cancel",SUPPORTS_TOUCH="ontouchstart"in window,SUPPORTS_POINTER_IE10=window.navigator.msPointerEnabled&&!window.navigator.pointerEnabled&&!SUPPORTS_TOUCH,SUPPORTS_POINTER=(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&!SUPPORTS_TOUCH,PLUGIN_NS="TouchSwipe",defaults={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,hold:null,triggerOnTouchEnd:!0,triggerOnTouchLeave:!1,allowPageScroll:"auto",fallbackToMouseEvents:!0,excludedElements:".noSwipe",preventDefaultEvents:!0};$.fn.swipe=function(method){var $this=$(this),plugin=$this.data(PLUGIN_NS);if(plugin&&"string"==typeof method){if(plugin[method])return plugin[method].apply(plugin,Array.prototype.slice.call(arguments,1));$.error("Method "+method+" does not exist on jQuery.swipe")}else if(plugin&&"object"==typeof method)plugin.option.apply(plugin,arguments);else if(!(plugin||"object"!=typeof method&&method))return init.apply(this,arguments);return $this},$.fn.swipe.version=VERSION,$.fn.swipe.defaults=defaults,$.fn.swipe.phases={PHASE_START:PHASE_START,PHASE_MOVE:PHASE_MOVE,PHASE_END:PHASE_END,PHASE_CANCEL:PHASE_CANCEL},$.fn.swipe.directions={LEFT:LEFT,RIGHT:RIGHT,UP:UP,DOWN:DOWN,IN:IN,OUT:OUT},$.fn.swipe.pageScroll={NONE:NONE,HORIZONTAL:HORIZONTAL,VERTICAL:VERTICAL,AUTO:AUTO},$.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,FOUR:4,FIVE:5,ALL:ALL_FINGERS}});
/**
 * Main mathys theme js file
 */

"use strict";

(function($, viewport){
var pixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;

var isRtl = false;

jQuery( document ).ready( function() {
	isRtl = ( 'undefined' != typeof icl_vars && icl_vars && 'ar' == icl_vars.current_language );
});

// var ThemeChart = function( el, data ) {
// 	this.el   = el;
// 	this.data = data;
// }

// ThemeChart.prototype.init = function() {
// 	var chart = new google.visualization.ColumnChart( this.el );
// 	chart.draw(this.getArgs(), this.getOptions());
// }

// ThemeChart.prototype.getArgs = function() {
// 	var data = google.visualization.arrayToDataTable([
// 		["Element", "Density", { role: "style" } ],
// 		["Copper", 8.94, "#b87333"],
// 		["Silver", 10.49, "silver"],
// 		["Gold", 19.30, "gold"],
// 		["Platinum", 21.45, "color: #e5e4e2"]
// 	]);

// 	var view = new google.visualization.DataView(data);
// 	view.setColumns([0, 1,
// 		{ calc: "stringify",
// 			sourceColumn: 1,
// 			type: "string",
// 			role: "annotation" },
// 	2]);

// 	return view;
// }

// ThemeChart.prototype.getOptions = function() {
// 	return {
// 		// title: "Density of Precious Metals, in g/cm^3",
// 		// width: 600,
// 		// height: 400,
// 		// bar: {groupWidth: "95%"},
// 		// legend: { position: "none" },
// 	};
// }

// google.charts.load("current", {packages:['corechart']});
// google.charts.setOnLoadCallback(drawChart);
// function drawChart() {
	

// }

// google.charts.load("current", '1', {packages:['corechart']});
// google.charts.setOnLoadCallback(drawChart2);
// function drawChart2() {
// 	var data = google.visualization.arrayToDataTable([
// 		["Element", "Density", { role: "style" } ],
// 		["Copper", 8.94, "#b87333"],
// 		["Silver", 10.49, "silver"],
// 		["Gold", 19.30, "gold"],
// 		["Platinum", 21.45, "color: #e5e4e2"]
// 	]);

// 	var view = new google.visualization.DataView(data);
// 	view.setColumns([0, 1,
// 		{ calc: "stringify",
// 			sourceColumn: 1,
// 			type: "string",
// 			role: "annotation" },
// 	2]);

// 	var options = {
// 		title: "Density of Precious Metals, in g/cm^3",
// 		width: 600,
// 		height: 400,
// 		bar: {groupWidth: "95%"},
// 		legend: { position: "none" },
// 	};
// 	var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
// 	chart.draw(view, options);
// }
(function(){
	/*
	 * Waits specified number of miliseconds before executing a callback
	 */
	function changed( fn, ms ) {
		var timer;
		return function(){
			clearTimeout(timer);
			timer = setTimeout(function(){
				fn();
			}, ms || 300);
		};
	}

	var $window = $(window);

	$window.on( 'resize', changed( 
		function() {
			$window.trigger( 'customResize' );
		}
	));
})();





function setScrollBarMargin( set ) {
	var margin = set ? scrollbarWidth() : 0;

	if ( $(document).height() <= $(window).height() ) 
		margin = '';

	$('html').css( 'margin-right', margin );
	$('.stick-header-completed #site-header').css( 'padding-right', margin );
}

/**
 * Detect browser scrollbar width
 */
function scrollbarWidth() {
	$(window).on( 'resize', function() {
		window.dizzainScrollBarWidth = null;
	});

	if ( window.dizzainScrollBarWidth ) 
		return window.dizzainScrollBarWidth;

	var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
		$outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
		inner = $inner[0],
		outer = $outer[0];

	var width1,
		width2;
	 
	jQuery('body').append(outer);

	width1 = inner.offsetWidth;

	$outer.css('overflow', 'scroll');

	width2 = outer.clientWidth;

	$outer.remove();

	window.dizzainScrollBarWidth = (width1 - width2);

	return window.dizzainScrollBarWidth;
}

function parseUrl( url ) {
    var a = document.createElement('a');
    a.href = url;

	// a.protocol; // => "http:"
	// a.hostname; // => "example.com"
	// a.port; // => "3000"
	// a.pathname; // => "/pathname/"
	// a.search; // => "?search=test"
	// a.hash; // => "#hash"
	// a.host; // => "example.com:3000" 

    return a;
}

/**
 * Parse query string
 *
 * Tested:     test=1&class=2
 * Not tested: test=1&class
 *
 * @param  {string} query - NOT url, only query string
 * @return {object}
 */
function parseQueryString( query ) {
	var obj = {};

	if ( 'string' != typeof query ) 
		return obj;

	var vars = query.split( '&' );

	for ( var i = 0; i < vars.length; i++ ) {
		var pair = vars[i].split( '=' );

		var key = decodeURIComponent( pair[0] );
		var val = decodeURIComponent( pair[1] );

		obj[key] = val;
	};

	return obj;
}

function getQueryVariable( variable ) {
	var query = window.location.search.substring(1);

	var vars = query.split('&');

	for ( var i = 0; i < vars.length; i++ ) {
		var pair = vars[i].split( '=' );
		if ( decodeURIComponent( pair[0] ) == variable ) {
			return decodeURIComponent( pair[1] );
		}
	}

	console.log( 'Query variable %s not found', variable );
} 

function getParameterByName( name, url ) {
	if ( !url ) 
		url = window.location.href;

	name = name.replace(/[\[\]]/g, "\\$&");

	var regex   = new RegExp( "[?&]" + name + "(=([^&#]*)|&|#|$)" ),
		results = regex.exec(url);

	if ( !results )
		return null;

	if ( !results[2] )
		return '';

	return decodeURIComponent( results[2].replace( /\+/g, " " ) );
}

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

/**	
 * Detect iOS
 */
function is_iOSmobile() {
	return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
}

function isAndroid() {
	var ua = navigator.userAgent.toLowerCase();
	return ( ua.indexOf( 'android' ) > -1 /* && ua.indexOf( 'mobile' ) */ );
}

// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
// see http://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser
function isOpera() {
	return !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
}

// Firefox 1.0+
function isFirefox() {
	return typeof InstallTrigger !== 'undefined';
}

// At least Safari 3+: "[object HTMLElementConstructor]"
function isSafari() {
	return Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
}

// Chrome 1+
function isChrome() {
	return !!window.chrome && !isOpera();
}

// At least IE6
function isIE() {
	return /*@cc_on!@*/false || !!document.documentMode;
}

function isMobileAndTabletDevice() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}


function getIOSWindowHeight() {
	// Get zoom level of mobile Safari
	// Note, that such zoom detection might not work correctly in other browsers
	// We use width, instead of height, because there are no vertical toolbars :)
	var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
	 
	// window.innerHeight returns height of the visible area.
	// We multiply it by zoom and get out real height.
	return window.innerHeight * zoomLevel;
};
 
// You can also get height of the toolbars that are currently displayed
function getHeightOfIOSToolbars() {
	var tH = (window.orientation === 0 ? screen.height : screen.width) - getIOSWindowHeight();
	return tH > 1 ? tH : 0;
}; 


/**
 * Generic functions to deal with cookies
 * чтобы удалить cookie, нужно создать новый с теми же параметрами, только c value=''
 */
function createCookie( name,value,days ) {
	var expires = "";

	if ( days ) {
		var date = new Date();
		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
		var expires = "; expires="+date.toGMTString();
	}

	document.cookie = name + "=" + value + expires + "; path=/";
}


function readCookie( name ) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');

	for ( var i = 0; i < ca.length; i++ ) {
		var c = ca[i];

		// if first char of C is space
		while (c.charAt(0)==' ') 
			c = c.substring(1,c.length);

		// search for necessary elements
		if (c.indexOf(nameEQ) == 0) {
			c = decodeURIComponent(c.substring(nameEQ.length,c.length));
			c = c.replace('"','');
			c = c.replace('"','');
			return c;
		}				
	}
	return undefined;
}

function updateCookie(name,value,days) {
	var cookie = readCookie(name);

	if ( undefined !== cookie ) {
		createCookie(name,'',-days);
	};
	createCookie(name,value,days);
}

function log( name, value ) {
	var out = undefined !== value ? [ name, value ] : name;

	console.log( out )
}
/*!
 * jQuery BBQ: Back Button & Query Library - v1.2.1 - 2/17/2010
 * http://benalman.com/projects/jquery-bbq-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery BBQ: Back Button & Query Library
//
// *Version: 1.2.1, Last updated: 2/17/2010*
// 
// Project Home - http://benalman.com/projects/jquery-bbq-plugin/
// GitHub       - http://github.com/cowboy/jquery-bbq/
// Source       - http://github.com/cowboy/jquery-bbq/raw/master/jquery.ba-bbq.js
// (Minified)   - http://github.com/cowboy/jquery-bbq/raw/master/jquery.ba-bbq.min.js (4.0kb)
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// These working examples, complete with fully commented code, illustrate a few
// ways in which this plugin can be used.
// 
// Basic AJAX     - http://benalman.com/code/projects/jquery-bbq/examples/fragment-basic/
// Advanced AJAX  - http://benalman.com/code/projects/jquery-bbq/examples/fragment-advanced/
// jQuery UI Tabs - http://benalman.com/code/projects/jquery-bbq/examples/fragment-jquery-ui-tabs/
// Deparam        - http://benalman.com/code/projects/jquery-bbq/examples/deparam/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with, what browsers it has been tested in, and where the unit tests
// reside (so you can test it yourself).
// 
// jQuery Versions - 1.3.2, 1.4.1, 1.4.2
// Browsers Tested - Internet Explorer 6-8, Firefox 2-3.7, Safari 3-4,
//                   Chrome 4-5, Opera 9.6-10.1.
// Unit Tests      - http://benalman.com/code/projects/jquery-bbq/unit/
// 
// About: Release History
// 
// 1.2.1 - (2/17/2010) Actually fixed the stale window.location Safari bug from
//         <jQuery hashchange event> in BBQ, which was the main reason for the
//         previous release!
// 1.2   - (2/16/2010) Integrated <jQuery hashchange event> v1.2, which fixes a
//         Safari bug, the event can now be bound before DOM ready, and IE6/7
//         page should no longer scroll when the event is first bound. Also
//         added the <jQuery.param.fragment.noEscape> method, and reworked the
//         <hashchange event (BBQ)> internal "add" method to be compatible with
//         changes made to the jQuery 1.4.2 special events API.
// 1.1.1 - (1/22/2010) Integrated <jQuery hashchange event> v1.1, which fixes an
//         obscure IE8 EmulateIE7 meta tag compatibility mode bug.
// 1.1   - (1/9/2010) Broke out the jQuery BBQ event.special <hashchange event>
//         functionality into a separate plugin for users who want just the
//         basic event & back button support, without all the extra awesomeness
//         that BBQ provides. This plugin will be included as part of jQuery BBQ,
//         but also be available separately. See <jQuery hashchange event>
//         plugin for more information. Also added the <jQuery.bbq.removeState>
//         method and added additional <jQuery.deparam> examples.
// 1.0.3 - (12/2/2009) Fixed an issue in IE 6 where location.search and
//         location.hash would report incorrectly if the hash contained the ?
//         character. Also <jQuery.param.querystring> and <jQuery.param.fragment>
//         will no longer parse params out of a URL that doesn't contain ? or #,
//         respectively.
// 1.0.2 - (10/10/2009) Fixed an issue in IE 6/7 where the hidden IFRAME caused
//         a "This page contains both secure and nonsecure items." warning when
//         used on an https:// page.
// 1.0.1 - (10/7/2009) Fixed an issue in IE 8. Since both "IE7" and "IE8
//         Compatibility View" modes erroneously report that the browser
//         supports the native window.onhashchange event, a slightly more
//         robust test needed to be added.
// 1.0   - (10/2/2009) Initial release

(function($,window){
  '$:nomunge'; // Used by YUI compressor.
  
  // Some convenient shortcuts.
  var undefined,
    aps = Array.prototype.slice,
    decode = decodeURIComponent,
    
    // Method / object references.
    jq_param = $.param,
    jq_param_fragment,
    jq_deparam,
    jq_deparam_fragment,
    jq_bbq = $.bbq = $.bbq || {},
    jq_bbq_pushState,
    jq_bbq_getState,
    jq_elemUrlAttr,
    jq_event_special = $.event.special,
    
    // Reused strings.
    str_hashchange = 'hashchange',
    str_querystring = 'querystring',
    str_fragment = 'fragment',
    str_elemUrlAttr = 'elemUrlAttr',
    str_location = 'location',
    str_href = 'href',
    str_src = 'src',
    
    // Reused RegExp.
    re_trim_querystring = /^.*\?|#.*$/g,
    re_trim_fragment = /^.*\#/,
    re_no_escape,
    
    // Used by jQuery.elemUrlAttr.
    elemUrlAttr_cache = {};
  
  // A few commonly used bits, broken out to help reduce minified file size.
  
  function is_string( arg ) {
    return typeof arg === 'string';
  };
  
  // Why write the same function twice? Let's curry! Mmmm, curry..
  
  function curry( func ) {
    var args = aps.call( arguments, 1 );
    
    return function() {
      return func.apply( this, args.concat( aps.call( arguments ) ) );
    };
  };
  
  // Get location.hash (or what you'd expect location.hash to be) sans any
  // leading #. Thanks for making this necessary, Firefox!
  function get_fragment( url ) {
    return url.replace( /^[^#]*#?(.*)$/, '$1' );
  };
  
  // Get location.search (or what you'd expect location.search to be) sans any
  // leading #. Thanks for making this necessary, IE6!
  function get_querystring( url ) {
    return url.replace( /(?:^[^?#]*\?([^#]*).*$)?.*/, '$1' );
  };
  
  // Section: Param (to string)
  // 
  // Method: jQuery.param.querystring
  // 
  // Retrieve the query string from a URL or if no arguments are passed, the
  // current window.location.
  // 
  // Usage:
  // 
  // > jQuery.param.querystring( [ url ] );
  // 
  // Arguments:
  // 
  //  url - (String) A URL containing query string params to be parsed. If url
  //    is not passed, the current window.location is used.
  // 
  // Returns:
  // 
  //  (String) The parsed query string, with any leading "?" removed.
  //
  
  // Method: jQuery.param.querystring (build url)
  // 
  // Merge a URL, with or without pre-existing query string params, plus any
  // object, params string or URL containing query string params into a new URL.
  // 
  // Usage:
  // 
  // > jQuery.param.querystring( url, params [, merge_mode ] );
  // 
  // Arguments:
  // 
  //  url - (String) A valid URL for params to be merged into. This URL may
  //    contain a query string and/or fragment (hash).
  //  params - (String) A params string or URL containing query string params to
  //    be merged into url.
  //  params - (Object) A params object to be merged into url.
  //  merge_mode - (Number) Merge behavior defaults to 0 if merge_mode is not
  //    specified, and is as-follows:
  // 
  //    * 0: params in the params argument will override any query string
  //         params in url.
  //    * 1: any query string params in url will override params in the params
  //         argument.
  //    * 2: params argument will completely replace any query string in url.
  // 
  // Returns:
  // 
  //  (String) Either a params string with urlencoded data or a URL with a
  //    urlencoded query string in the format 'a=b&c=d&e=f'.
  
  // Method: jQuery.param.fragment
  // 
  // Retrieve the fragment (hash) from a URL or if no arguments are passed, the
  // current window.location.
  // 
  // Usage:
  // 
  // > jQuery.param.fragment( [ url ] );
  // 
  // Arguments:
  // 
  //  url - (String) A URL containing fragment (hash) params to be parsed. If
  //    url is not passed, the current window.location is used.
  // 
  // Returns:
  // 
  //  (String) The parsed fragment (hash) string, with any leading "#" removed.
  
  // Method: jQuery.param.fragment (build url)
  // 
  // Merge a URL, with or without pre-existing fragment (hash) params, plus any
  // object, params string or URL containing fragment (hash) params into a new
  // URL.
  // 
  // Usage:
  // 
  // > jQuery.param.fragment( url, params [, merge_mode ] );
  // 
  // Arguments:
  // 
  //  url - (String) A valid URL for params to be merged into. This URL may
  //    contain a query string and/or fragment (hash).
  //  params - (String) A params string or URL containing fragment (hash) params
  //    to be merged into url.
  //  params - (Object) A params object to be merged into url.
  //  merge_mode - (Number) Merge behavior defaults to 0 if merge_mode is not
  //    specified, and is as-follows:
  // 
  //    * 0: params in the params argument will override any fragment (hash)
  //         params in url.
  //    * 1: any fragment (hash) params in url will override params in the
  //         params argument.
  //    * 2: params argument will completely replace any query string in url.
  // 
  // Returns:
  // 
  //  (String) Either a params string with urlencoded data or a URL with a
  //    urlencoded fragment (hash) in the format 'a=b&c=d&e=f'.
  
  function jq_param_sub( is_fragment, get_func, url, params, merge_mode ) {
    var result,
      qs,
      matches,
      url_params,
      hash;
    
    if ( params !== undefined ) {
      // Build URL by merging params into url string.
      
      // matches[1] = url part that precedes params, not including trailing ?/#
      // matches[2] = params, not including leading ?/#
      // matches[3] = if in 'querystring' mode, hash including leading #, otherwise ''
      matches = url.match( is_fragment ? /^([^#]*)\#?(.*)$/ : /^([^#?]*)\??([^#]*)(#?.*)/ );
      
      // Get the hash if in 'querystring' mode, and it exists.
      hash = matches[3] || '';
      
      if ( merge_mode === 2 && is_string( params ) ) {
        // If merge_mode is 2 and params is a string, merge the fragment / query
        // string into the URL wholesale, without converting it into an object.
        qs = params.replace( is_fragment ? re_trim_fragment : re_trim_querystring, '' );
        
      } else {
        // Convert relevant params in url to object.
        url_params = jq_deparam( matches[2] );
        
        params = is_string( params )
          
          // Convert passed params string into object.
          ? jq_deparam[ is_fragment ? str_fragment : str_querystring ]( params )
          
          // Passed params object.
          : params;
        
        qs = merge_mode === 2 ? params                              // passed params replace url params
          : merge_mode === 1  ? $.extend( {}, params, url_params )  // url params override passed params
          : $.extend( {}, url_params, params );                     // passed params override url params
        
        // Convert params object to a string.
        qs = jq_param( qs );
        
        // Unescape characters specified via $.param.noEscape. Since only hash-
        // history users have requested this feature, it's only enabled for
        // fragment-related params strings.
        if ( is_fragment ) {
          qs = qs.replace( re_no_escape, decode );
        }
      }
      
      // Build URL from the base url, querystring and hash. In 'querystring'
      // mode, ? is only added if a query string exists. In 'fragment' mode, #
      // is always added.
      result = matches[1] + ( is_fragment ? '#' : qs || !matches[1] ? '?' : '' ) + qs + hash;
      
    } else {
      // If URL was passed in, parse params from URL string, otherwise parse
      // params from window.location.
      result = get_func( url !== undefined ? url : window[ str_location ][ str_href ] );
    }
    
    return result;
  };
  
  jq_param[ str_querystring ]                  = curry( jq_param_sub, 0, get_querystring );
  jq_param[ str_fragment ] = jq_param_fragment = curry( jq_param_sub, 1, get_fragment );
  
  // Method: jQuery.param.fragment.noEscape
  // 
  // Specify characters that will be left unescaped when fragments are created
  // or merged using <jQuery.param.fragment>, or when the fragment is modified
  // using <jQuery.bbq.pushState>. This option only applies to serialized data
  // object fragments, and not set-as-string fragments. Does not affect the
  // query string. Defaults to ",/" (comma, forward slash).
  // 
  // Note that this is considered a purely aesthetic option, and will help to
  // create URLs that "look pretty" in the address bar or bookmarks, without
  // affecting functionality in any way. That being said, be careful to not
  // unescape characters that are used as delimiters or serve a special
  // purpose, such as the "#?&=+" (octothorpe, question mark, ampersand,
  // equals, plus) characters.
  // 
  // Usage:
  // 
  // > jQuery.param.fragment.noEscape( [ chars ] );
  // 
  // Arguments:
  // 
  //  chars - (String) The characters to not escape in the fragment. If
  //    unspecified, defaults to empty string (escape all characters).
  // 
  // Returns:
  // 
  //  Nothing.
  
  jq_param_fragment.noEscape = function( chars ) {
    chars = chars || '';
    var arr = $.map( chars.split(''), encodeURIComponent );
    re_no_escape = new RegExp( arr.join('|'), 'g' );
  };
  
  // A sensible default. These are the characters people seem to complain about
  // "uglifying up the URL" the most.
  jq_param_fragment.noEscape( ',/' );
  
  // Section: Deparam (from string)
  // 
  // Method: jQuery.deparam
  // 
  // Deserialize a params string into an object, optionally coercing numbers,
  // booleans, null and undefined values; this method is the counterpart to the
  // internal jQuery.param method.
  // 
  // Usage:
  // 
  // > jQuery.deparam( params [, coerce ] );
  // 
  // Arguments:
  // 
  //  params - (String) A params string to be parsed.
  //  coerce - (Boolean) If true, coerces any numbers or true, false, null, and
  //    undefined to their actual value. Defaults to false if omitted.
  // 
  // Returns:
  // 
  //  (Object) An object representing the deserialized params string.
  
  $.deparam = jq_deparam = function( params, coerce ) {
    var obj = {},
      coerce_types = { 'true': !0, 'false': !1, 'null': null };
    
    // Iterate over all name=value pairs.
    $.each( params.replace( /\+/g, ' ' ).split( '&' ), function(j,v){
      var param = v.split( '=' ),
        key = decode( param[0] ),
        val,
        cur = obj,
        i = 0,
        
        // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
        // into its component parts.
        keys = key.split( '][' ),
        keys_last = keys.length - 1;
      
      // If the first keys part contains [ and the last ends with ], then []
      // are correctly balanced.
      if ( /\[/.test( keys[0] ) && /\]$/.test( keys[ keys_last ] ) ) {
        // Remove the trailing ] from the last keys part.
        keys[ keys_last ] = keys[ keys_last ].replace( /\]$/, '' );
        
        // Split first keys part into two parts on the [ and add them back onto
        // the beginning of the keys array.
        keys = keys.shift().split('[').concat( keys );
        
        keys_last = keys.length - 1;
      } else {
        // Basic 'foo' style key.
        keys_last = 0;
      }
      
      // Are we dealing with a name=value pair, or just a name?
      if ( param.length === 2 ) {
        val = decode( param[1] );
        
        // Coerce values.
        if ( coerce ) {
          val = val && !isNaN(val)            ? +val              // number
            : val === 'undefined'             ? undefined         // undefined
            : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
            : val;                                                // string
        }
        
        if ( keys_last ) {
          // Complex key, build deep object structure based on a few rules:
          // * The 'cur' pointer starts at the object top-level.
          // * [] = array push (n is set to array length), [n] = array if n is 
          //   numeric, otherwise object.
          // * If at the last keys part, set the value.
          // * For each keys part, if the current level is undefined create an
          //   object or array based on the type of the next keys part.
          // * Move the 'cur' pointer to the next level.
          // * Rinse & repeat.
          for ( ; i <= keys_last; i++ ) {
            key = keys[i] === '' ? cur.length : keys[i];
            cur = cur[key] = i < keys_last
              ? cur[key] || ( keys[i+1] && isNaN( keys[i+1] ) ? {} : [] )
              : val;
          }
          
        } else {
          // Simple key, even simpler rules, since only scalars and shallow
          // arrays are allowed.
          
          if ( $.isArray( obj[key] ) ) {
            // val is already an array, so push on the next value.
            obj[key].push( val );
            
          } else if ( obj[key] !== undefined ) {
            // val isn't an array, but since a second value has been specified,
            // convert val into an array.
            obj[key] = [ obj[key], val ];
            
          } else {
            // val is a scalar.
            obj[key] = val;
          }
        }
        
      } else if ( key ) {
        // No value was defined, so set something meaningful.
        obj[key] = coerce
          ? undefined
          : '';
      }
    });
    
    return obj;
  };
  
  // Method: jQuery.deparam.querystring
  // 
  // Parse the query string from a URL or the current window.location,
  // deserializing it into an object, optionally coercing numbers, booleans,
  // null and undefined values.
  // 
  // Usage:
  // 
  // > jQuery.deparam.querystring( [ url ] [, coerce ] );
  // 
  // Arguments:
  // 
  //  url - (String) An optional params string or URL containing query string
  //    params to be parsed. If url is omitted, the current window.location
  //    is used.
  //  coerce - (Boolean) If true, coerces any numbers or true, false, null, and
  //    undefined to their actual value. Defaults to false if omitted.
  // 
  // Returns:
  // 
  //  (Object) An object representing the deserialized params string.
  
  // Method: jQuery.deparam.fragment
  // 
  // Parse the fragment (hash) from a URL or the current window.location,
  // deserializing it into an object, optionally coercing numbers, booleans,
  // null and undefined values.
  // 
  // Usage:
  // 
  // > jQuery.deparam.fragment( [ url ] [, coerce ] );
  // 
  // Arguments:
  // 
  //  url - (String) An optional params string or URL containing fragment (hash)
  //    params to be parsed. If url is omitted, the current window.location
  //    is used.
  //  coerce - (Boolean) If true, coerces any numbers or true, false, null, and
  //    undefined to their actual value. Defaults to false if omitted.
  // 
  // Returns:
  // 
  //  (Object) An object representing the deserialized params string.
  
  function jq_deparam_sub( is_fragment, url_or_params, coerce ) {
    if ( url_or_params === undefined || typeof url_or_params === 'boolean' ) {
      // url_or_params not specified.
      coerce = url_or_params;
      url_or_params = jq_param[ is_fragment ? str_fragment : str_querystring ]();
    } else {
      url_or_params = is_string( url_or_params )
        ? url_or_params.replace( is_fragment ? re_trim_fragment : re_trim_querystring, '' )
        : url_or_params;
    }
    
    return jq_deparam( url_or_params, coerce );
  };
  
  jq_deparam[ str_querystring ]                    = curry( jq_deparam_sub, 0 );
  jq_deparam[ str_fragment ] = jq_deparam_fragment = curry( jq_deparam_sub, 1 );
  
  // Section: Element manipulation
  // 
  // Method: jQuery.elemUrlAttr
  // 
  // Get the internal "Default URL attribute per tag" list, or augment the list
  // with additional tag-attribute pairs, in case the defaults are insufficient.
  // 
  // In the <jQuery.fn.querystring> and <jQuery.fn.fragment> methods, this list
  // is used to determine which attribute contains the URL to be modified, if
  // an "attr" param is not specified.
  // 
  // Default Tag-Attribute List:
  // 
  //  a      - href
  //  base   - href
  //  iframe - src
  //  img    - src
  //  input  - src
  //  form   - action
  //  link   - href
  //  script - src
  // 
  // Usage:
  // 
  // > jQuery.elemUrlAttr( [ tag_attr ] );
  // 
  // Arguments:
  // 
  //  tag_attr - (Object) An object containing a list of tag names and their
  //    associated default attribute names in the format { tag: 'attr', ... } to
  //    be merged into the internal tag-attribute list.
  // 
  // Returns:
  // 
  //  (Object) An object containing all stored tag-attribute values.
  
  // Only define function and set defaults if function doesn't already exist, as
  // the urlInternal plugin will provide this method as well.
  $[ str_elemUrlAttr ] || ($[ str_elemUrlAttr ] = function( obj ) {
    return $.extend( elemUrlAttr_cache, obj );
  })({
    a: str_href,
    base: str_href,
    iframe: str_src,
    img: str_src,
    input: str_src,
    form: 'action',
    link: str_href,
    script: str_src
  });
  
  jq_elemUrlAttr = $[ str_elemUrlAttr ];
  
  // Method: jQuery.fn.querystring
  // 
  // Update URL attribute in one or more elements, merging the current URL (with
  // or without pre-existing query string params) plus any params object or
  // string into a new URL, which is then set into that attribute. Like
  // <jQuery.param.querystring (build url)>, but for all elements in a jQuery
  // collection.
  // 
  // Usage:
  // 
  // > jQuery('selector').querystring( [ attr, ] params [, merge_mode ] );
  // 
  // Arguments:
  // 
  //  attr - (String) Optional name of an attribute that will contain a URL to
  //    merge params or url into. See <jQuery.elemUrlAttr> for a list of default
  //    attributes.
  //  params - (Object) A params object to be merged into the URL attribute.
  //  params - (String) A URL containing query string params, or params string
  //    to be merged into the URL attribute.
  //  merge_mode - (Number) Merge behavior defaults to 0 if merge_mode is not
  //    specified, and is as-follows:
  //    
  //    * 0: params in the params argument will override any params in attr URL.
  //    * 1: any params in attr URL will override params in the params argument.
  //    * 2: params argument will completely replace any query string in attr
  //         URL.
  // 
  // Returns:
  // 
  //  (jQuery) The initial jQuery collection of elements, but with modified URL
  //  attribute values.
  
  // Method: jQuery.fn.fragment
  // 
  // Update URL attribute in one or more elements, merging the current URL (with
  // or without pre-existing fragment/hash params) plus any params object or
  // string into a new URL, which is then set into that attribute. Like
  // <jQuery.param.fragment (build url)>, but for all elements in a jQuery
  // collection.
  // 
  // Usage:
  // 
  // > jQuery('selector').fragment( [ attr, ] params [, merge_mode ] );
  // 
  // Arguments:
  // 
  //  attr - (String) Optional name of an attribute that will contain a URL to
  //    merge params into. See <jQuery.elemUrlAttr> for a list of default
  //    attributes.
  //  params - (Object) A params object to be merged into the URL attribute.
  //  params - (String) A URL containing fragment (hash) params, or params
  //    string to be merged into the URL attribute.
  //  merge_mode - (Number) Merge behavior defaults to 0 if merge_mode is not
  //    specified, and is as-follows:
  //    
  //    * 0: params in the params argument will override any params in attr URL.
  //    * 1: any params in attr URL will override params in the params argument.
  //    * 2: params argument will completely replace any fragment (hash) in attr
  //         URL.
  // 
  // Returns:
  // 
  //  (jQuery) The initial jQuery collection of elements, but with modified URL
  //  attribute values.
  
  function jq_fn_sub( mode, force_attr, params, merge_mode ) {
    if ( !is_string( params ) && typeof params !== 'object' ) {
      // force_attr not specified.
      merge_mode = params;
      params = force_attr;
      force_attr = undefined;
    }
    
    return this.each(function(){
      var that = $(this),
        
        // Get attribute specified, or default specified via $.elemUrlAttr.
        attr = force_attr || jq_elemUrlAttr()[ ( this.nodeName || '' ).toLowerCase() ] || '',
        
        // Get URL value.
        url = attr && that.attr( attr ) || '';
      
      // Update attribute with new URL.
      that.attr( attr, jq_param[ mode ]( url, params, merge_mode ) );
    });
    
  };
  
  $.fn[ str_querystring ] = curry( jq_fn_sub, str_querystring );
  $.fn[ str_fragment ]    = curry( jq_fn_sub, str_fragment );
  
  // Section: History, hashchange event
  // 
  // Method: jQuery.bbq.pushState
  // 
  // Adds a 'state' into the browser history at the current position, setting
  // location.hash and triggering any bound <hashchange event> callbacks
  // (provided the new state is different than the previous state).
  // 
  // If no arguments are passed, an empty state is created, which is just a
  // shortcut for jQuery.bbq.pushState( {}, 2 ).
  // 
  // Usage:
  // 
  // > jQuery.bbq.pushState( [ params [, merge_mode ] ] );
  // 
  // Arguments:
  // 
  //  params - (String) A serialized params string or a hash string beginning
  //    with # to merge into location.hash.
  //  params - (Object) A params object to merge into location.hash.
  //  merge_mode - (Number) Merge behavior defaults to 0 if merge_mode is not
  //    specified (unless a hash string beginning with # is specified, in which
  //    case merge behavior defaults to 2), and is as-follows:
  // 
  //    * 0: params in the params argument will override any params in the
  //         current state.
  //    * 1: any params in the current state will override params in the params
  //         argument.
  //    * 2: params argument will completely replace current state.
  // 
  // Returns:
  // 
  //  Nothing.
  // 
  // Additional Notes:
  // 
  //  * Setting an empty state may cause the browser to scroll.
  //  * Unlike the fragment and querystring methods, if a hash string beginning
  //    with # is specified as the params agrument, merge_mode defaults to 2.
  
  jq_bbq.pushState = jq_bbq_pushState = function( params, merge_mode ) {
    if ( is_string( params ) && /^#/.test( params ) && merge_mode === undefined ) {
      // Params string begins with # and merge_mode not specified, so completely
      // overwrite window.location.hash.
      merge_mode = 2;
    }
    
    var has_args = params !== undefined,
      // Merge params into window.location using $.param.fragment.
      url = jq_param_fragment( window[ str_location ][ str_href ],
        has_args ? params : {}, has_args ? merge_mode : 2 );
    
    // Set new window.location.href. If hash is empty, use just # to prevent
    // browser from reloading the page. Note that Safari 3 & Chrome barf on
    // location.hash = '#'.
    window[ str_location ][ str_href ] = url + ( /#/.test( url ) ? '' : '#' );
  };
  
  // Method: jQuery.bbq.getState
  // 
  // Retrieves the current 'state' from the browser history, parsing
  // location.hash for a specific key or returning an object containing the
  // entire state, optionally coercing numbers, booleans, null and undefined
  // values.
  // 
  // Usage:
  // 
  // > jQuery.bbq.getState( [ key ] [, coerce ] );
  // 
  // Arguments:
  // 
  //  key - (String) An optional state key for which to return a value.
  //  coerce - (Boolean) If true, coerces any numbers or true, false, null, and
  //    undefined to their actual value. Defaults to false.
  // 
  // Returns:
  // 
  //  (Anything) If key is passed, returns the value corresponding with that key
  //    in the location.hash 'state', or undefined. If not, an object
  //    representing the entire 'state' is returned.
  
  jq_bbq.getState = jq_bbq_getState = function( key, coerce ) {
    return key === undefined || typeof key === 'boolean'
      ? jq_deparam_fragment( key ) // 'key' really means 'coerce' here
      : jq_deparam_fragment( coerce )[ key ];
  };
  
  // Method: jQuery.bbq.removeState
  // 
  // Remove one or more keys from the current browser history 'state', creating
  // a new state, setting location.hash and triggering any bound
  // <hashchange event> callbacks (provided the new state is different than
  // the previous state).
  // 
  // If no arguments are passed, an empty state is created, which is just a
  // shortcut for jQuery.bbq.pushState( {}, 2 ).
  // 
  // Usage:
  // 
  // > jQuery.bbq.removeState( [ key [, key ... ] ] );
  // 
  // Arguments:
  // 
  //  key - (String) One or more key values to remove from the current state,
  //    passed as individual arguments.
  //  key - (Array) A single array argument that contains a list of key values
  //    to remove from the current state.
  // 
  // Returns:
  // 
  //  Nothing.
  // 
  // Additional Notes:
  // 
  //  * Setting an empty state may cause the browser to scroll.
  
  jq_bbq.removeState = function( arr ) {
    var state = {};
    
    // If one or more arguments is passed..
    if ( arr !== undefined ) {
      
      // Get the current state.
      state = jq_bbq_getState();
      
      // For each passed key, delete the corresponding property from the current
      // state.
      $.each( $.isArray( arr ) ? arr : arguments, function(i,v){
        delete state[ v ];
      });
    }
    
    // Set the state, completely overriding any existing state.
    jq_bbq_pushState( state, 2 );
  };
  
  // Event: hashchange event (BBQ)
  // 
  // Usage in jQuery 1.4 and newer:
  // 
  // In jQuery 1.4 and newer, the event object passed into any hashchange event
  // callback is augmented with a copy of the location.hash fragment at the time
  // the event was triggered as its event.fragment property. In addition, the
  // event.getState method operates on this property (instead of location.hash)
  // which allows this fragment-as-a-state to be referenced later, even after
  // window.location may have changed.
  // 
  // Note that event.fragment and event.getState are not defined according to
  // W3C (or any other) specification, but will still be available whether or
  // not the hashchange event exists natively in the browser, because of the
  // utility they provide.
  // 
  // The event.fragment property contains the output of <jQuery.param.fragment>
  // and the event.getState method is equivalent to the <jQuery.bbq.getState>
  // method.
  // 
  // > $(window).bind( 'hashchange', function( event ) {
  // >   var hash_str = event.fragment,
  // >     param_obj = event.getState(),
  // >     param_val = event.getState( 'param_name' ),
  // >     param_val_coerced = event.getState( 'param_name', true );
  // >   ...
  // > });
  // 
  // Usage in jQuery 1.3.2:
  // 
  // In jQuery 1.3.2, the event object cannot to be augmented as in jQuery 1.4+,
  // so the fragment state isn't bound to the event object and must instead be
  // parsed using the <jQuery.param.fragment> and <jQuery.bbq.getState> methods.
  // 
  // > $(window).bind( 'hashchange', function( event ) {
  // >   var hash_str = $.param.fragment(),
  // >     param_obj = $.bbq.getState(),
  // >     param_val = $.bbq.getState( 'param_name' ),
  // >     param_val_coerced = $.bbq.getState( 'param_name', true );
  // >   ...
  // > });
  // 
  // Additional Notes:
  // 
  // * Due to changes in the special events API, jQuery BBQ v1.2 or newer is
  //   required to enable the augmented event object in jQuery 1.4.2 and newer.
  // * See <jQuery hashchange event> for more detailed information.
  
  jq_event_special[ str_hashchange ] = $.extend( jq_event_special[ str_hashchange ], {
    
    // Augmenting the event object with the .fragment property and .getState
    // method requires jQuery 1.4 or newer. Note: with 1.3.2, everything will
    // work, but the event won't be augmented)
    add: function( handleObj ) {
      var old_handler;
      
      function new_handler(e) {
        // e.fragment is set to the value of location.hash (with any leading #
        // removed) at the time the event is triggered.
        var hash = e[ str_fragment ] = jq_param_fragment();
        
        // e.getState() works just like $.bbq.getState(), but uses the
        // e.fragment property stored on the event object.
        e.getState = function( key, coerce ) {
          return key === undefined || typeof key === 'boolean'
            ? jq_deparam( hash, key ) // 'key' really means 'coerce' here
            : jq_deparam( hash, coerce )[ key ];
        };
        
        old_handler.apply( this, arguments );
      };
      
      // This may seem a little complicated, but it normalizes the special event
      // .add method between jQuery 1.4/1.4.1 and 1.4.2+
      if ( $.isFunction( handleObj ) ) {
        // 1.4, 1.4.1
        old_handler = handleObj;
        return new_handler;
      } else {
        // 1.4.2+
        old_handler = handleObj.handler;
        handleObj.handler = new_handler;
      }
    }
    
  });
  
})(jQuery,this);

/*!
 * jQuery hashchange event - v1.2 - 2/11/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery hashchange event
//
// *Version: 1.2, Last updated: 2/11/2010*
// 
// Project Home - http://benalman.com/projects/jquery-hashchange-plugin/
// GitHub       - http://github.com/cowboy/jquery-hashchange/
// Source       - http://github.com/cowboy/jquery-hashchange/raw/master/jquery.ba-hashchange.js
// (Minified)   - http://github.com/cowboy/jquery-hashchange/raw/master/jquery.ba-hashchange.min.js (1.1kb)
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrate one way
// in which this plugin can be used.
// 
// hashchange event - http://benalman.com/code/projects/jquery-hashchange/examples/hashchange/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with, what browsers it has been tested in, and where the unit tests
// reside (so you can test it yourself).
// 
// jQuery Versions - 1.3.2, 1.4.1, 1.4.2
// Browsers Tested - Internet Explorer 6-8, Firefox 2-3.7, Safari 3-4, Chrome, Opera 9.6-10.1.
// Unit Tests      - http://benalman.com/code/projects/jquery-hashchange/unit/
// 
// About: Known issues
// 
// While this jQuery hashchange event implementation is quite stable and robust,
// there are a few unfortunate browser bugs surrounding expected hashchange
// event-based behaviors, independent of any JavaScript window.onhashchange
// abstraction. See the following examples for more information:
// 
// Chrome: Back Button - http://benalman.com/code/projects/jquery-hashchange/examples/bug-chrome-back-button/
// Firefox: Remote XMLHttpRequest - http://benalman.com/code/projects/jquery-hashchange/examples/bug-firefox-remote-xhr/
// WebKit: Back Button in an Iframe - http://benalman.com/code/projects/jquery-hashchange/examples/bug-webkit-hash-iframe/
// Safari: Back Button from a different domain - http://benalman.com/code/projects/jquery-hashchange/examples/bug-safari-back-from-diff-domain/
// 
// About: Release History
// 
// 1.2   - (2/11/2010) Fixed a bug where coming back to a page using this plugin
//         from a page on another domain would cause an error in Safari 4. Also,
//         IE6/7 Iframe is now inserted after the body (this actually works),
//         which prevents the page from scrolling when the event is first bound.
//         Event can also now be bound before DOM ready, but it won't be usable
//         before then in IE6/7.
// 1.1   - (1/21/2010) Incorporated document.documentMode test to fix IE8 bug
//         where browser version is incorrectly reported as 8.0, despite
//         inclusion of the X-UA-Compatible IE=EmulateIE7 meta tag.
// 1.0   - (1/9/2010) Initial Release. Broke out the jQuery BBQ event.special
//         window.onhashchange functionality into a separate plugin for users
//         who want just the basic event & back button support, without all the
//         extra awesomeness that BBQ provides. This plugin will be included as
//         part of jQuery BBQ, but also be available separately.

(function($,window,undefined){
  '$:nomunge'; // Used by YUI compressor.
  
  // Method / object references.
  var fake_onhashchange,
    jq_event_special = $.event.special,
    
    // Reused strings.
    str_location = 'location',
    str_hashchange = 'hashchange',
    str_href = 'href',
    
    // IE6/7 specifically need some special love when it comes to back-button
    // support, so let's do a little browser sniffing..
    browser = $.browser,
    mode = document.documentMode,
    is_old_ie = browser.msie && ( mode === undefined || mode < 8 ),
    
    // Does the browser support window.onhashchange? Test for IE version, since
    // IE8 incorrectly reports this when in "IE7" or "IE8 Compatibility View"!
    supports_onhashchange = 'on' + str_hashchange in window && !is_old_ie;
  
  // Get location.hash (or what you'd expect location.hash to be) sans any
  // leading #. Thanks for making this necessary, Firefox!
  function get_fragment( url ) {
    url = url || window[ str_location ][ str_href ];
    return url.replace( /^[^#]*#?(.*)$/, '$1' );
  };
  
  // Property: jQuery.hashchangeDelay
  // 
  // The numeric interval (in milliseconds) at which the <hashchange event>
  // polling loop executes. Defaults to 100.
  
  $[ str_hashchange + 'Delay' ] = 100;
  
  // Event: hashchange event
  // 
  // Fired when location.hash changes. In browsers that support it, the native
  // window.onhashchange event is used (IE8, FF3.6), otherwise a polling loop is
  // initialized, running every <jQuery.hashchangeDelay> milliseconds to see if
  // the hash has changed. In IE 6 and 7, a hidden Iframe is created to allow
  // the back button and hash-based history to work.
  // 
  // Usage:
  // 
  // > $(window).bind( 'hashchange', function(e) {
  // >   var hash = location.hash;
  // >   ...
  // > });
  // 
  // Additional Notes:
  // 
  // * The polling loop and Iframe are not created until at least one callback
  //   is actually bound to 'hashchange'.
  // * If you need the bound callback(s) to execute immediately, in cases where
  //   the page 'state' exists on page load (via bookmark or page refresh, for
  //   example) use $(window).trigger( 'hashchange' );
  // * The event can be bound before DOM ready, but since it won't be usable
  //   before then in IE6/7 (due to the necessary Iframe), recommended usage is
  //   to bind it inside a $(document).ready() callback.
  
  jq_event_special[ str_hashchange ] = $.extend( jq_event_special[ str_hashchange ], {
    
    // Called only when the first 'hashchange' event is bound to window.
    setup: function() {
      // If window.onhashchange is supported natively, there's nothing to do..
      if ( supports_onhashchange ) { return false; }
      
      // Otherwise, we need to create our own. And we don't want to call this
      // until the user binds to the event, just in case they never do, since it
      // will create a polling loop and possibly even a hidden Iframe.
      $( fake_onhashchange.start );
    },
    
    // Called only when the last 'hashchange' event is unbound from window.
    teardown: function() {
      // If window.onhashchange is supported natively, there's nothing to do..
      if ( supports_onhashchange ) { return false; }
      
      // Otherwise, we need to stop ours (if possible).
      $( fake_onhashchange.stop );
    }
    
  });
  
  // fake_onhashchange does all the work of triggering the window.onhashchange
  // event for browsers that don't natively support it, including creating a
  // polling loop to watch for hash changes and in IE 6/7 creating a hidden
  // Iframe to enable back and forward.
  fake_onhashchange = (function(){
    var self = {},
      timeout_id,
      iframe,
      set_history,
      get_history;
    
    // Initialize. In IE 6/7, creates a hidden Iframe for history handling.
    function init(){
      // Most browsers don't need special methods here..
      set_history = get_history = function(val){ return val; };
      
      // But IE6/7 do!
      if ( is_old_ie ) {
        
        // Create hidden Iframe after the end of the body to prevent initial
        // page load from scrolling unnecessarily.
        iframe = $('<iframe src="javascript:0"/>').hide().insertAfter( 'body' )[0].contentWindow;
        
        // Get history by looking at the hidden Iframe's location.hash.
        get_history = function() {
          return get_fragment( iframe.document[ str_location ][ str_href ] );
        };
        
        // Set a new history item by opening and then closing the Iframe
        // document, *then* setting its location.hash.
        set_history = function( hash, history_hash ) {
          if ( hash !== history_hash ) {
            var doc = iframe.document;
            doc.open().close();
            doc[ str_location ].hash = '#' + hash;
          }
        };
        
        // Set initial history.
        set_history( get_fragment() );
      }
    };
    
    // Start the polling loop.
    self.start = function() {
      // Polling loop is already running!
      if ( timeout_id ) { return; }
      
      // Remember the initial hash so it doesn't get triggered immediately.
      var last_hash = get_fragment();
      
      // Initialize if not yet initialized.
      set_history || init();
      
      // This polling loop checks every $.hashchangeDelay milliseconds to see if
      // location.hash has changed, and triggers the 'hashchange' event on
      // window when necessary.
      (function loopy(){
        var hash = get_fragment(),
          history_hash = get_history( last_hash );
        
        if ( hash !== last_hash ) {
          set_history( last_hash = hash, history_hash );
          
          $(window).trigger( str_hashchange );
          
        } else if ( history_hash !== last_hash ) {
          window[ str_location ][ str_href ] = window[ str_location ][ str_href ].replace( /#.*/, '' ) + '#' + history_hash;
        }
        
        timeout_id = setTimeout( loopy, $[ str_hashchange + 'Delay' ] );
      })();
    };
    
    // Stop the polling loop, but only if an IE6/7 Iframe wasn't created. In
    // that case, even if there are no longer any bound event handlers, the
    // polling loop is still necessary for back/next to work at all!
    self.stop = function() {
      if ( !iframe ) {
        timeout_id && clearTimeout( timeout_id );
        timeout_id = 0;
      }
    };
    
    return self;
  })();
  
})(jQuery,this);


/**
 * @license jQuery wizard plug-in 3.0.7 (18-SEPT-2012)
 *
 *
 * Copyright (c) 2012 Jan Sundman (jan.sundman[at]aland.net)
 *
 *
 * Licensed under the MIT licens:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 */


(function($){
	$.widget("ui.formwizard", {

		_init: function() {

			var wizard = this;
			var formOptionsSuccess = this.options.formOptions.success;
			var formOptionsComplete = this.options.formOptions.complete;
			var formOptionsBeforeSend = this.options.formOptions.beforeSend;
			var formOptionsBeforeSubmit = this.options.formOptions.beforeSubmit;
			var formOptionsBeforeSerialize = this.options.formOptions.beforeSerialize;
			this.options.formOptions = $.extend(this.options.formOptions,{
				success	: function(responseText, textStatus, xhr){
					if(formOptionsSuccess){
						formOptionsSuccess(responseText, textStatus, xhr);
					}
					if(wizard.options.formOptions && wizard.options.formOptions.resetForm || !wizard.options.formOptions){
						wizard._reset();
					}
				},
				complete : function(xhr, textStatus){
					if(formOptionsComplete){
						formOptionsComplete(xhr, textStatus);
					}
					wizard._enableNavigation();
				},
				beforeSubmit : function(arr, theForm, options) {
					if(formOptionsBeforeSubmit){
						var shouldSubmit = formOptionsBeforeSubmit(arr, theForm, options);
						if(!shouldSubmit)
							wizard._enableNavigation();
						return shouldSubmit;
					}
				},
				beforeSend : function(xhr) {
					if(formOptionsBeforeSend){
						var shouldSubmit = formOptionsBeforeSend(xhr);
						if(!shouldSubmit)
							wizard._enableNavigation();
						return shouldSubmit;
					}
				},
				beforeSerialize: function(form, options) {
					if(formOptionsBeforeSerialize){
						var shouldSubmit = formOptionsBeforeSerialize(form, options);
						if(!shouldSubmit)
							wizard._enableNavigation();
						return shouldSubmit;
					}
				}
			});

			if (this.options.historyEnabled) {
				$.bbq.removeState("_" + $(this.element).attr('id'));
			}

			this.steps = this.element.find(".step").hide();

			this.firstStep = this.steps.eq(0).attr("id");
			this.activatedSteps = new Array();
			this.isLastStep = false;
			this.previousStep = undefined;
			this.currentStep = this.steps.eq(0).attr("id");
			this.nextButton	= this.element.find(this.options.next)
					.click(function() {
						return wizard._next();
					});

			this.nextButtonInitinalValue = this.nextButton.val();
			this.nextButton.val(this.options.textNext);

				this.backButton	= this.element.find(this.options.back)
					.click(function() {
						wizard._back();return false;
					});

				this.backButtonInitinalValue = this.backButton.val();
				this.backButton.val(this.options.textBack);

			if(this.options.validationEnabled && jQuery().validate  == undefined){
				this.options.validationEnabled = false;
				if( (window['console'] !== undefined) ){
					console.log("%s", "validationEnabled option set, but the validation plugin is not included");
				}
			}else if(this.options.validationEnabled){
				this.element.validate(this.options.validationOptions);
			}
			if(this.options.formPluginEnabled && jQuery().ajaxSubmit == undefined){
				this.options.formPluginEnabled = false;
				if( (window['console'] !== undefined) ){
					console.log("%s", "formPluginEnabled option set but the form plugin is not included");
				}
			}

			if(this.options.disableInputFields == true){
				$(this.steps).find(":input:not('.wizard-ignore')").attr("disabled","disabled");
			}

			if(this.options.historyEnabled){
				$(window).bind('hashchange', undefined, function(event){
					var hashStep = event.getState( "_" + $(wizard.element).attr( 'id' )) || wizard.firstStep;
					if(hashStep !== wizard.currentStep){
						if(wizard.options.validationEnabled && hashStep === wizard._navigate(wizard.currentStep)){
							if(!wizard.element.valid()){
								wizard._updateHistory(wizard.currentStep);
								wizard.element.validate().focusInvalid();

								return false;
							}
						}
						if(hashStep !== wizard.currentStep)
							wizard._show(hashStep);
					}
				});
			}

			this.element.addClass("ui-formwizard");
			this.element.find(":input").addClass("ui-wizard-content");
			this.steps.addClass("ui-formwizard-content");
			this.backButton.addClass("ui-formwizard-button ui-wizard-content");
			this.nextButton.addClass("ui-formwizard-button ui-wizard-content");

			if(!this.options.disableUIStyles){
				this.element.addClass("ui-helper-reset ui-widget ui-widget-content ui-helper-reset ui-corner-all");
				this.element.find(":input").addClass("ui-helper-reset ui-state-default");
				this.steps.addClass("ui-helper-reset ui-corner-all");
				this.backButton.addClass("ui-helper-reset ui-state-default");
				this.nextButton.addClass("ui-helper-reset ui-state-default");
			}
			this._show(undefined);
			return $(this);
		},

		_next : function(){
			if(this.options.validationEnabled){
				if(!this.element.valid()){
					this.element.validate().focusInvalid();
					return false;
				}
			}

			if(this.options.remoteAjax != undefined){
				var options = this.options.remoteAjax[this.currentStep];
				var wizard = this;
				if(options !== undefined){
					var success = options.success;
					var beforeSend = options.beforeSend;
					var complete = options.complete;

					options = $.extend({},options,{
						success: function(data, statusText){
							if((success !== undefined && success(data, statusText)) || (success == undefined)){
								wizard._continueToNextStep();
							}
						},
						beforeSend : function(xhr){
							wizard._disableNavigation();
							if(beforeSend !== undefined)
								beforeSend(xhr);
							$(wizard.element).trigger('before_remote_ajax', {"currentStep" : wizard.currentStep});
						},
						complete : function(xhr, statusText){
							if(complete !== undefined)
								complete(xhr, statusText);
							$(wizard.element).trigger('after_remote_ajax', {"currentStep" : wizard.currentStep});
							wizard._enableNavigation();
						}
					})
					this.element.ajaxSubmit(options);
					return false;
				}
			}

			return this._continueToNextStep();
		},

		_back : function(){
			if(this.activatedSteps.length > 0){
				if(this.options.historyEnabled){
					this._updateHistory(this.activatedSteps[this.activatedSteps.length - 2]);
				}else{
					this._show(this.activatedSteps[this.activatedSteps.length - 2], true);
				}
			}
			return false;
		},

		_continueToNextStep : function(){
			if(this.isLastStep){
				for(var i = 0; i < this.activatedSteps.length; i++){
					this.steps.filter("#" + this.activatedSteps[i]).find(":input").not(".wizard-ignore").removeAttr("disabled");
				}
				if(!this.options.formPluginEnabled){
					return true;
				}else{
					this._disableNavigation();
					this.element.ajaxSubmit(this.options.formOptions);
					return false;
				}
			}

			var step = this._navigate(this.currentStep);
			if(step == this.currentStep){
				return false;
			}
			if(this.options.historyEnabled){
				this._updateHistory(step);
			}else{
				this._show(step, true);
			}
			return false;
		},

		_updateHistory : function(step){
			var state = {};
			state["_" + $(this.element).attr('id')] = step;
			$.bbq.pushState(state);
		},

		_disableNavigation : function(){
			this.nextButton.attr("disabled","disabled");
			this.backButton.attr("disabled","disabled");
			if(!this.options.disableUIStyles){
				this.nextButton.removeClass("ui-state-active").addClass("ui-state-disabled");
				this.backButton.removeClass("ui-state-active").addClass("ui-state-disabled");
			}
		},

		_enableNavigation : function(){
			if(this.isLastStep){
				this.nextButton.val(this.options.textSubmit);
			}else{
				this.nextButton.val(this.options.textNext);
			}

			if($.trim(this.currentStep) !== this.steps.eq(0).attr("id")){
				this.backButton.removeAttr("disabled");
				if(!this.options.disableUIStyles){
					this.backButton.removeClass("ui-state-disabled").addClass("ui-state-active");
				}
			}

			this.nextButton.removeAttr("disabled");
			if(!this.options.disableUIStyles){
				this.nextButton.removeClass("ui-state-disabled").addClass("ui-state-active");
			}
		},

		_animate : function(oldStep, newStep, stepShownCallback){
			this._disableNavigation();
			var old = this.steps.filter("#" + oldStep);
			var current = this.steps.filter("#" + newStep);
			old.find(":input").not(".wizard-ignore").attr("disabled","disabled");
			current.find(":input").not(".wizard-ignore").removeAttr("disabled");
			var wizard = this;
			old.animate(wizard.options.outAnimation, wizard.options.outDuration, wizard.options.easing, function(){
				current.animate(wizard.options.inAnimation, wizard.options.inDuration, wizard.options.easing, function(){
					if(wizard.options.focusFirstInput)
						current.find(":input:first").focus();
					wizard._enableNavigation();

					stepShownCallback.apply(wizard);
				});
				return;
			});
		},

		_checkIflastStep : function(step){
			this.isLastStep = false;
			if($("#" + step).hasClass(this.options.submitStepClass) || this.steps.filter(":last").attr("id") == step){
				this.isLastStep = true;
			}
		},

		_getLink : function(step){
			var link = undefined;
			var links = this.steps.filter("#" + step).find(this.options.linkClass);

			if(links != undefined){
				if(links.filter(":radio,:checkbox").size() > 0){
					link = links.filter(this.options.linkClass + ":checked").val();
				}else{
					link = $(links).val();
				}
			}
			return link;
		},

		_navigate : function(step){
			var link = this._getLink(step);
			if(link != undefined){
				if((link != "" && link != null && link != undefined) && this.steps.filter("#" + link).attr("id") != undefined){
					return link;
				}
				return this.currentStep;
			}else if(link == undefined && !this.isLastStep){
				var current   = this.steps.filter("#" + step);
				var nextIndex = this.steps.index( current ) + 1;
				var step1     = this.steps.eq( nextIndex ).attr("id");
				// var step1 =  this.steps.filter("#" + step).next().attr("id");
				return step1;
			}
		},

		_show : function(step){
			var backwards = false;
			var triggerStepShown = step !== undefined;
			if(step == undefined || step == ""){
					this.activatedSteps.pop();
					step = this.firstStep;
					this.activatedSteps.push(step);
			}else{
				if($.inArray(step, this.activatedSteps) > -1){
					backwards = true;
					this.activatedSteps.pop();
				}else {
					this.activatedSteps.push(step);
				}
			}

			if(this.currentStep !== step || step === this.firstStep){
				this.previousStep = this.currentStep;
				this._checkIflastStep(step);
				this.currentStep = step;
				var stepShownCallback = function(){if(triggerStepShown){$(this.element).trigger('step_shown', $.extend({"isBackNavigation" : backwards},this._state()));}}
				if(triggerStepShown){
					$(this.element).trigger('before_step_shown', $.extend({"isBackNavigation" : backwards},this._state()));
				}
				this._animate(this.previousStep, step, stepShownCallback);
			};


		},

	   _reset : function(){
			this.element.resetForm()
			$("label,:input,textarea",this).removeClass("error");
			for(var i = 0; i < this.activatedSteps.length; i++){
				this.steps.filter("#" + this.activatedSteps[i]).hide().find(":input").attr("disabled","disabled");
			}
			this.activatedSteps = new Array();
			this.previousStep = undefined;
			this.isLastStep = false;
			if(this.options.historyEnabled){
				this._updateHistory(this.firstStep);
			}else{
				this._show(this.firstStep);
			}

		},

		_state : function(state){
			var currentState = { "settings" : this.options,
				"activatedSteps" : this.activatedSteps,
				"isLastStep" : this.isLastStep,
				"isFirstStep" : this.currentStep === this.firstStep,
				"previousStep" : this.previousStep,
				"currentStep" : this.currentStep,
				"backButton" : this.backButton,
				"nextButton" : this.nextButton,
				"steps" : this.steps,
				"firstStep" : this.firstStep
			}

			if(state !== undefined)
				return currentState[state];

			return currentState;
		},

	  /*Methods*/

		show : function(step){
			if(this.options.historyEnabled){
				this._updateHistory(step);
			}else{
				this._show(step);
			}
		},

		state : function(state){
			return this._state(state);
		},

		reset : function(){
			this._reset();
		},

		next : function(){
			this._next();
		},

		back : function(){
			this._back();
		},

		destroy: function() {
			this.element.find("*").removeAttr("disabled").show();
			this.nextButton.unbind("click").val(this.nextButtonInitinalValue).removeClass("ui-state-disabled").addClass("ui-state-active");
			this.backButton.unbind("click").val(this.backButtonInitinalValue).removeClass("ui-state-disabled").addClass("ui-state-active");
			this.backButtonInitinalValue = undefined;
			this.nextButtonInitinalValue = undefined;
			this.activatedSteps = undefined;
			this.previousStep = undefined;
			this.currentStep = undefined;
			this.isLastStep = undefined;
			this.options = undefined;
			this.nextButton = undefined;
			this.backButton = undefined;
			this.formwizard = undefined;
			this.element = undefined;
			this.steps = undefined;
			this.firstStep = undefined;
		},

		update_steps : function(){
			this.steps = this.element.find(".step").addClass("ui-formwizard-content");
			this.firstStep = this.steps.eq(0).attr("id");
			this.steps.not("#" + this.currentStep).hide().find(":input").addClass("ui-wizard-content").attr("disabled","disabled");
			this._checkIflastStep(this.currentStep);
			this._enableNavigation();
			if(!this.options.disableUIStyles){
				this.steps.addClass("ui-helper-reset ui-corner-all");
				this.steps.find(":input").addClass("ui-helper-reset ui-state-default");
			}
		},

		options: {
	   		historyEnabled	: false,
			validationEnabled : false,
			validationOptions : undefined,
			formPluginEnabled : false,
			linkClass	: ".link",
			submitStepClass : "submit_step",
			back : ":reset",
			next : ":submit",
			textSubmit : 'Submit',
			textNext : 'Next',
			textBack : 'Back',
			remoteAjax : undefined,
			inAnimation : {opacity: 'show'},
			outAnimation: {opacity: 'hide'},
			inDuration : 400,
			outDuration: 400,
			easing: 'swing',
			focusFirstInput : false,
			disableInputFields : true,
			formOptions : { reset: true, success: function(data) { if( (window['console'] !== undefined) ){console.log("%s", "form submit successful");}},
			disableUIStyles : false
		}
   }
 });
})(jQuery);

$(document).ready(function(){
/* mobile-menu */

$('#primary-navigation').map(function() {
	var cl = 'mobile-menu-active';
	var mobilePoint = '<md';
	
	var $primaryNav = $(this),
		$body       = $('body'),
		$mainBtn    = $('#mobile-nav-toggler');

	$mainBtn.on( 'click', function(e) {
		e.preventDefault();

		toggleClasses( !$body.hasClass( cl ) );
	});

	$primaryNav.swipe({
		swipe: function( event, direction ) {
			if ( 'right' != direction )
				return;

			toggleClasses(false);
		},
	});

	$(window).resize(
		viewport.changed( function() {
			if ( viewport.is( mobilePoint ) ) 
				return;

			if ( !$body.hasClass( cl ) ) 
				return;

			toggleClasses(false);
		})
	);

	function toggleClasses( toggle ) {
		toggle = ( 'undefined' == typeof toggle ) ? true : toggle;

		$(this).toggleClass( cl, toggle );
		$body.toggleClass( cl, toggle );		
		// $primaryNav.toggleClass( cl, toggle );
	}
});

// цель, создать фссоциативный массив с размерами window мобильных утройств при наличии панели вкладок/адреной строки и в ситуации, когда она исчезает. И в зависимости от ориентации экрана.
// 
// Как высчитывать:
// загружается страница, у нее есть уже 2 параметра
// small height текучей ориентации и large height (current width) другой оринтации
// при скроллинге панель адресной строки исчезает и появляется large height current orientation. по ней можно Вычислиьть высоту панели адресной стрроки.
// Но тут может быть косяк в том плане, что пнель адресной строки может иметь разную высоту на разных ориентациях
// 
//size = {
//  0: {
//  	small height
//  	large height
//  },
//  90: {
//  	small height
//  	large height
//  }
//}


// (function(){

// })();


// var MobileWindowSize = function() {

// }
+function ($) {
	'use strict';

	var FormSteps = function ( main ) {
		this.$main    = $(main);
		this.$steps   = this.$main.find( this.shortcode( 'step' ) );
		this.$actions = this.$main.find( this.shortcode( 'action' ) );
		this.$navs    = $( this.shortcode( 'nav' ) );

		this.currentStep    = 0;
		this.startStep      = this.currentStep;
		this.activatedSteps = new Array();

		this.init();
	};

	FormSteps.prototype = {
		cls: {
			active: 'active',
			enabled: 'enabled'
		},
		codes: {
			step:   'data-step',
			nav:    'data-step-nav',
			action: 'data-step-action',
		},
		shortcode: function( key, val ) {
			val = undefined !== val ? '='+ val : '';

			return '['+ this.codes[ key ] + val + ']';
		},
		init: function() {
			this.initActions();
			this.initNav();
			this.initEvents();
			this.showFirst();
		},
		showFirst: function() {
			var first = this.$steps.eq( 0 ).data( 'step' );

			if ( this.currentStep != first ) {
				for ( var i = this.currentStep; i < first; i++ ) {
					this.$navs
						.filter( this.shortcode( 'nav', i ) )
						.addClass( this.cls.enabled )
						.data( 'stepDisabled', 1 );

					this.currentStep = i;
					this.canActivateStep( i );
				};

				this.currentStep = first;
				this.startStep   = first;
			};

			this.show( this.currentStep );
		},
		initActions: function() {
			var self = this;

			this.$actions.on( 'click', function( e ) {
				e.preventDefault();

				var $btn    = $(this);
				var current = $btn.closest( self.shortcode( 'step' ) ).data( 'step' );

				if ( +$btn.data( 'stepDisabled' ) ) {
					self.$main.trigger( 'step-disabled-'+ current );
					return;
				}

				var bind = $btn.data( 'stepAction' );

				self.$main.trigger( 'step-'+ current );

				if ( 'external' == bind )
					return;

				self.show( ++current );
			});
		},
		initNav: function() {
			var  self = this;

			this.$navs.on( 'click', function(e) {
				e.preventDefault();

				var $link  = $(this);
				var number = $link.data( 'stepNav' );

				if ( +$link.data( 'stepDisabled' ) )
					return;

				if ( !self.isActiveStep( number ) )
					return;

				if ( !$link.hasClass( self.cls.enabled ) ) 
					return;

				if ( number > self.currentStep )
					return;

				self.activateByNav( number );
			});
		},
		activateByNav: function( number ) {
			this.activateNav( number );

			if ( this.startStep == number ) 
				return this.activate( number );

			this.$steps
				.filter( this.shortcode( 'step', --number ) )
				.find( this.shortcode( 'action' ) )
				.trigger( 'click' );
		},
		activateNav: function( number ) {
			var $current = this.$navs.filter( this.shortcode( 'nav', number ) );
			var active   = this.cls.active;

			if ( $current.hasClass( active ) )
				return;

			this.$navs
				.filter( '.' + active )
				.removeClass( active );

			$current.addClass( active );

			if ( $current.hasClass( this.cls.enabled ) )
				return;

			$current.addClass( this.cls.enabled );
		},
		isActiveStep: function( number ) {
			return ( -1 !== $.inArray( number, this.activatedSteps ) );
		},
		canActivateStep: function( number ) {
			if ( this.isActiveStep( number ) ) 
				return true;

			if ( number != this.activatedSteps.length ) 
				return false;

			this.activatedSteps.push( number );

			return true;
		},
		activate: function( number ) {
			if ( !this.canActivateStep( number ) )
				return;

			var $current = this.$steps.filter( this.shortcode( 'step', number ) );

			this.$steps.filter( '.'+ this.cls.active ).removeClass( this.cls.active );

			$current.addClass( this.cls.active );

			this.currentStep = number;
		},
		initEvents: function() {
			var self = this;

			self.$main.on( 'next', function() {
				self.next();
			});
			self.$main.on( 'prev', function() {
				self.prev();
			});
			self.$main.on( 'show', function( e, number ) {
				self.show( number );
			});
		},
		show: function( number ) {
			this.activateNav( number );
			this.activate( number );
		},
		prev: function() {
			if ( 0 == this.currentStep ) 
				return;

			this.show( --this.currentStep );
		},
		next: function() {
			this.show( ++this.currentStep );
		},
	}

	function Plugin( options ) {
		this.each( function() {
			new FormSteps( this );
		});
	}

	$.fn.themeFormStaps = Plugin;

}(jQuery);
/* ========================================================================
 * Bootstrap: tab.js v3.3.6
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

+function ($) {
	'use strict';

	// TAB CLASS DEFINITION
	// ====================

	var Tab = function ( parentNav ) {
		this.$parentNav = $(parentNav);
		this.$nav       = this.getNav();
		this.$tabs      = null;
		this.fromTop    = 0;
		// this.isMobile   = viewport.is( this.mobilePoint );
		this.isMobile   = false;
	};

	Tab.prototype.$window = $(window);

	Tab.prototype.mobilePoint = '<=xs';

	Tab.prototype.init = function () {
		var self = this;

		self.$nav.on( 'click', actionOnClick );

		// on mobile version all tabs are closed
		if ( !self.isMobile )
			self.$nav.eq( 0 ).trigger( 'click' );

		// self.onWindowResize();

		function actionOnClick( e ) {
			e.preventDefault();

			self.actionOnClick( $(this) );
		}
	};

	Tab.prototype.getNav = function() {
		var self = this;

		self.target = '';

		return self.$parentNav.find( '.js-tab-nav' ).filter( function() {
			var target = $(this).data('target').replace( /[0-9]+$/i, '' );

			if ( 0 == self.target.length )
				self.target = target;
			
			return ( self.target == target ); 
		});
	};

	Tab.prototype.onWindowResize = function() {
		var self = this;

		$(window).resize(
			viewport.changed( function() {
				self.isMobile = viewport.is( self.mobilePoint );

				if ( self.isMobile )
					return;

				if ( self.$tabs.filter( '.active' ).length ) 
					return;

				// on switch from mobile to desktop if has not active tab
				// activate first
				self.$nav.eq( 0 ).trigger( 'click' );
			})
		);
	};

	Tab.prototype.actionOnClick = function( $link ) {
		var self = this;

		var selector = $link.data('target');

		// if ( !selector ) {
		// 	selector = $this.attr('href')
		// 	selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
		// }

		var $tab = !self.$tabs ? $( selector ) : self.$tabs.filter( selector );

		var doActivate = self.doActivate( $tab );

		self.setTabs( $tab );

		self.saveFromTop();

		self.deactive();

		// allow to cloase tab on mobile version,
		// all tabs can be closed on mobile
		if ( doActivate )
			self.activate( $tab );

		self.setFromTop();
	};

	Tab.prototype.doActivate = function( $tab ) {
		var self = this;

		if ( !self.isMobile ) 
			return true;

		if ( !$tab.hasClass( 'active' ) )
			return true;

		return false;
	};

	Tab.prototype.activate = function( $tab ) {
		this.$nav
			.filter( '[data-target="#'+ $tab.attr( 'id' ) +'"]' )
			.addClass( 'active' )
			.trigger( 'themeTab.activate' );

		$tab
			.addClass( 'active' )
			.trigger( 'themeTab.activate' );
	};

	/**
	 * Deactivate tabs and tab navs
	 */
	Tab.prototype.deactive = function() {
		var self = this;

		$.each( [ '$nav', '$tabs' ], function() {
			self[ this ]
				.filter( '.active' )
				.removeClass( 'active' )
				.trigger( 'themeTab.deactive' );
		});
	};

	Tab.prototype.saveFromTop = function() {
		this.fromTop = this.$window.scrollTop();
	};

	Tab.prototype.setFromTop = function() {
		this.$window.scrollTop( this.fromTop );
	};

	Tab.prototype.setTabs = function( $tab ) {
		this.$tabs = $tab.parent().find( '> .js-tab-item' );
	};


	// TAB PLUGIN DEFINITION
	// =====================

	function Plugin(option) {
		return this.each(function () {
			var data = new Tab(this);

			data.init();
		});
	}

	$.fn.themeTab = Plugin;

}(jQuery);
// $.ajaxSetup({
//     complete: function(){
//         $('body').find('select.shipping_method').select2();
//     }
// });

$('body.woocommerce-cart').each(function(){
	$body = $(this);

	init();

	$(document).ajaxSuccess( init );

	function init() {
		updateCart();
		// initSelect();
		shippingHandler();
		clientsDropdown();
	}

	function updateCart(){
		var $form = $body.find('.cart-form');

		$form.find('input[type="text"]').change(function(){
			$form.find('.update_cart').removeAttr('disabled').trigger('click');
		})
	}

	function initSelect(){
		var $select = $body.find('select.shipping_method');

		if( 0 == $select.length )
			return;

		// $select.select2();
	}

	function shippingHandler() {
		$('#js-theme-cart-shipping').each( function () {
			var $main      = $(this);
			var $dropdown  = $main.find( '#shipping_list' );

			if ( 0 == $dropdown.length ) 
				return;

			var $hidden    = $main.find( '.shipping_method' );
			var $install   = $main.find( 'input[name=need_to_install]' );
			var $quantity  = $main.find( '.product-quantity' );
			var hiddenData = hiddenClousure( $hidden );

			$dropdown.on( 'change', function () {
				var val = $(this).val();

				hiddenData.set( 'shipping', val );
			});

			$install.on( 'change', function () {
				hiddenData.set( 'install', $(this).val() );
			});	
		});
	}

	function clientsDropdown() {
		$('#js-theme-cart-clients').each( function() {
			var $main      = $(this);
			var $hidden    = $('#js-theme-cart-shipping .shipping_method');
			var $dropdown  = $main.find( '#cart-clients-list' );
			var hiddenData = hiddenClousure( $hidden );

			$dropdown.on( 'change', function () {
				var val = $(this).val();

				hiddenData.set( 'client_id', val );
			});
		});
	}

	function hiddenClousure( $hidden ) {
		var data = {};

		parseData();

		var h = function () {
			return data;
		}

		h.set = function( key, value ) {
			data[ key ] = value;

			updateValue();
		}

		h.checkMaxInstall = function( max ) {
			if ( data.install <= max ) 
				return;

			data.install = max;

			updateValue();
		}

		function parseData() {
			data = JSON.parse( $hidden.val() );
		}

		function updateValue() {
			var value = JSON.stringify( data );

			$hidden.val( value ).trigger( 'change' );
		}

		return h;
	}

});




$('body.woocommerce-checkout').each( function() {
	init();

	$(document).ajaxSuccess( init );

	function init() {
		paymentDropdown();
	}
	
	function paymentDropdown() {
		$('#payment').each( function() {
			var $main      = $(this);
			var $dropdown  = $main.find( '#payment_methods_dropdown' );
			var $radio     = $main.find( '[name=payment_method]' );
			var $lis       = $main.find( '.wc_payment_method' );

			setActiveSelect( $dropdown.val() );

			/**
			 * Делается такой костыль, из-за того,
			 * что система оплаты square привязана к payments верстке.
			 * Поэтому приходится делать отдельный дропдаун 
			 * и на его изменение делать клик на радио кнопку
			 */
			$dropdown.on( 'change', function () {
				var val = $(this).val();

				setActiveSelect( val );
			});

			function setActiveSelect( val ) {
				var $el = $lis.filter( '.payment_method_'+ val );

				$el.siblings().removeClass( 'active' );

				$el.addClass( 'active' ).find( '#payment_method_'+ val ).trigger( 'click' );
			}
		});
	}
});
function ValidatePart( name, codeName, $form ) {
	this.name          = name;
	this.codeName      = codeName;
	this.validated     = [];
	this.$form         = $form;
	this.$part         = $form.find('.woocommerce-'+ codeName +'-fields');
	this.$fields       = this.$part.find( '.input-text, select, input:checkbox' );
	this.requiredCount = this.$part.find( '.validate-required' ).length;
	this.errors        = {};

	this.init();
}

ValidatePart.prototype = {
	// fields: '.input-text, select, input:checkbox',
	init: function() {
		var self = this;

		self.$fields.on( 'blur change', function() {
			var $field = $(this);

			// устновлен setTimeout с задержкой 0 нарочно,
			// чтоб функция выполнилась самой последней в стэке функций
			// По другому говоря, чтоб сначала отработались все эвенты в других местах,
			// например woocommerce, а потом обрабатывалась здесь
			setTimeout( function() {
				self.validateField( $field );
			}, 0);
		});

		// self.$part.on( 'trigger-validation', function() {
		// 	self.triggerValidation( 'click-button' );
		// });
	},
	validateField: function( $field ) {
		var $parent = $field.closest( '.form-row' );
		var id      = $field.attr( 'id' );

		if ( !$parent.hasClass( 'validate-required' ) ) 
			return;

		this.validatePhone( $field );

		if ( $parent.hasClass( 'woocommerce-invalid' ) ) {
			this.setError( id, $field );
			
			if ( -1 === $.inArray( id, this.validated ) ) 
				return;

			this.triggerStatus( 'invalid' );

			this.validated.splice( this.validated.indexOf( id ), 1 );

			return;
		};

		if ( -1 === $.inArray( id, this.validated ) ) 
			this.validated.push( id );
			
		this.removeError( id );

		if ( !this.isValid() )
			return;

		this.triggerStatus( 'valid' );
	},
	setError: function( id, $field ) {
		var val  = $field.val();
		var name = id.replace( /(billing|shipping)_/, '' );
		var error;
		var callback;

		if ( !this.isSetErrorLabels() ) 
			return;
		
		if ( !val ) {
			this.errors[ id ] = this.getErrorLabel( name );
			return;
		}

		if ( 'email' == name ) {
			callback = this.isEmail;
		} else 
		if ( 'phone' == name ) {
			callback = this.isPhone;
		} /*else 
		if ( 'postcode' == name ) {
			callback = this.isPostcode;
		}*/

		if ( !callback( val ) ) {
			name += '_not_valid';
			
			this.errors[ id ] = this.getErrorLabel( name );
		};
	},
	isSetErrorLabels: function() {
		return ( 'undefined' != typeof themeCheckoutErrors ) ;
	},
	getErrorLabel: function( name ) {
		if ( !this.isSetErrorLabels() ) 
			return;

		return themeCheckoutErrors[ name ];
	},
	getErrors: function() {
		return this.errors;
	},
	removeError: function( id ) {
		delete this.errors[ id ];
	},
	validatePhone: function( $field ) {
		var $parent = $field.closest( '.form-row' );
		var val     = $field.val();

		if ( !val ) 
			return;

		if ( !$parent.hasClass( 'validate-phone' ) )
			return;

		if ( ! this.isPhone( val ) ) {
			$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-email' );
		} else {
			$parent.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' ).addClass( 'woocommerce-validated' );
		}
	},
	triggerStatus: function( status ) {
		this.$form.trigger( 'validate-part', [ this.name, status ] );
	},
	triggerValidation: function( status ) {
		this.$fields.each( function() {
			var $field  = $(this);
			var $parent = $field.closest( '.form-row' );

			if ( !$parent.hasClass( 'validate-required' ) ) 
					return;

			if ( 'onload' == status && !$field.val() )
				return;

			$field.trigger( 'change' );
		});
	},
	isValid: function() {
		return ( this.requiredCount == this.validated.length ) ;
	},
	isPhone: function( value ) {
		/* http://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript/5059082#5059082 */
		var pattern = new RegExp( /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\.\/0-9]*$/g );

		// php woocommerce reg exp
		// var pattern = new RegExp( /[\s\#0-9_\-\+\(\)]*$/g );
		
		return pattern.test( value );
	},
	isEmail: function( value ) {
		/* https://stackoverflow.com/questions/2855865/jquery-validate-e-mail-address-regex */
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

		return pattern.test( value );
	}
}


/**
 * Т.к. сложно создать checkout steps в woocommerce,
 * придумываем возможности как подстроить существующий функционал
 * под требования проекта.
 * В частности
 * 1) шаг - аунтификация - тут просто, если юзер залогинен то ее пропускаем, 
 * если нет, тогда при попытке залогиниться, страница перезагружается, 
 * т.к. для этих полей используется своя <form>
 * Если не гуд, то смотрим error messages. Если гуд, 
 * то показываем следующий шаг.
 * Надо учесть вариант с кэшем, что форма аунтификации может не исчезать, а просто скрываться.
 * 2) шаг - форма доставки. Тут сложнее. Тут весь checkout обернут в одну форму,
 * которая обрабатывается аяксом. Поэтому придуман хак такой:
 * в этот шаг добавлена <button>, которая по клику сабмитит форму.
 * По дефолту еще добавлен дополнительный input:hidden, 
 * который сообщает, на каком шаге сабмитится форма.
 * Т.к. форма обрабатывается аяксом, то глобально перехватываем событие аякса.
 * В нем смотрим полученную инфу в data.messages.
 * Если там находится кастомный спец ответ, тогда переходим на следующий шаг.
 * Если нет, тогда ничего не делаем. Отрабатывается стандартный функционал woocommerce
 * 3) шаг - окончание chcckout
 */
$('.js-checkout-steps').each( function() {
	var $main            = $(this);
	var $btnStepShipping = $main.find('.js-btn-step-shipping');
	var $form            = $main.find( 'form.woocommerce-checkout' );

	var $shippingMain = $btnStepShipping.closest( '[data-step]' );
	var shippingStep  = $shippingMain.data( 'step' );
	var $errors       = $form.find( '.woocommerce-error' );

	$main.themeFormStaps();

	$main.on( 'step-'+ shippingStep, createShippingHeaderMeta );
	// $main.on( 'step-disabled-'+ shippingStep, showFormErrors );

	validateForm();

	/**
	 * Shipping .section-header > .section-meta сообщение
	 */
	function createShippingHeaderMeta() {
		$shippingMain.find( '.section-meta' ).find( 'p' ).each( function() {
			var $p   = $(this);
			var keys = $p.data( 'pattern' ).split( ',' );
			var out  = [];
			var name = '';

			$.each( keys, function( i, key ) {
				var key   = $.trim( key );
				var value = $shippingMain.find( '#billing_' + key ).val();

				if ( !value ) 
					return;

				if ( 'first_name' == key ) {
					name = value;
					return;
				} else 
				if ( 'last_name' == key )
					value = name + ' ' + value;

				out.push( value );
			});

			$p.text( out.join( ', ' ) );
		});
	}


	function validateForm() {
		var isBtnClick = false;
		var $checkbox = $form.find( '#ship-to-different-address-checkbox' );

		var parts = {
			// !!! shipping и billing должны быть поменяны местами
			shipping: new ValidatePart( 'shipping', 'billing',  $form ),
			billing:  new ValidatePart( 'billing',  'shipping', $form ),
		}

		$form.on( 'validate-part', onValidatePart );

		$btnStepShipping.on( 'click', validateFormOnClick );

		$( document.body ).on( 'checkout_error', function() {
			isBtnClick = false;
		});

		doValidation( 'onload' );

		function onValidatePart() {
			if ( !isValid() )
				return;

			$btnStepShipping.removeClass( 'disabled' ).data( 'stepDisabled', 0 );

			if ( isBtnClick ) {
				// т.к. триггер blur field вызывается и при нажатии
				// кнопки place order,
				// и там могут быть свои шибки
				// то ошибки формы скрываем только при нажатии на кнопку process purchase
				hideErrors();

				// вызывается повторный клик кнопки,
				// т.к. если вставить инфу через crtl+p ,
				// то событие input onChange или onBlur обрабатывается
				// когда происходит событие вне поля или при написании текста клавиатурой.
				// Поэтому валидачия работала не корректно.
				$btnStepShipping
					.data( 'stepAction', '' )
					.trigger( 'click' )
					.data( 'stepAction', 'external' );
				isBtnClick = false;
			};
			// $btnStepShipping.addClass( 'disabled' ).data( 'stepDisabled', 1 );
		}

		function isValid() {
			var isValid = false;

			if ( !$checkbox.prop('checked') )
				isValid = parts.shipping.isValid();
			else
				isValid = ( parts.shipping.isValid() && parts.billing.isValid() );

			return isValid;
		}

		function validateFormOnClick() {
			isBtnClick = true;

			doValidation( 'btn-click' );
		}

		function showErrors() {
			var errors = getErrorsHtml();

			if ( !errors ) 
				return;

			// получается очень замудреная логика
			// ошибки могу генериться, например,
			// при нажатии кнопки place order,
			// т.е. когда в процессе checkout есть ошибки
			// поэтому предположительно сначала ищем
			// уже созданный блок ошибок
			if ( 0 == $errors.length )
				$errors = $form.find( '.woocommerce-error' );

			// если блок ошибко существует и он создан не тим скриптом,
			// то удаляем его, т.к. при валидации формы 
			// старые ошибки не нужны
			if ( 0 != $errors.length && !$errors.hasClass( 'js-errors' ) )
				hideErrors();

			// создаем свой блок ошибок
			if ( 0 == $errors.length ) {
				$errors = $('<ul class="woocommerce-error js-errors">');
				$form.prepend( $errors );
			}

			$errors.html( errors );
		}

		function isOnlyShipping() {
			return !$checkbox.prop('checked');
		}

		function doValidation( status ) {
			if ( isOnlyShipping() )
				parts.shipping.triggerValidation( status );
			else {
				parts.shipping.triggerValidation( status );
				parts.billing.triggerValidation( status );
			}

			// установлен таймаут для создания цепочки обратотки формы
			// сначала дефотлтный скрипт woocommerce обрабатывает формуб
			// потом обрабатывает checkout-validation.js
			// и потом вывод ошибок здесь
			setTimeout( function() {
				if ( isValid() ) {
					hideErrors();
				} else {
					showErrors();
				}
			}, 1);
		}
		function getErrors() {
			var errorList = {};

			$.each( parts, function( key, part ) {
				var errors = part.getErrors();
				var prefix = part.getErrorLabel( part.name );

				if ( isOnlyShipping() && 'billing' == part.name ) 
					return;

				if ( !errors ) 
					return;

				$.each( errors, function( key, label ) {
					errorList[ key ] = label.replace( 'PART', prefix );
				});
			});

			return errorList;
		}

		function getErrorsHtml() {
			var errors = getErrors();
			var html   = '';

			$.each( errors, function( key, label ) {
				html += '<li>'+ label +'</li>';
			});

			return html;
		}

		function hideErrors() {
			if ( 0 == $errors.length )
				$errors = $form.find( '.woocommerce-error' );

			if ( 0 == $errors.length ) 
				return;

			$errors.remove();
			$errors = [];
		}
	}

});
;(function () {
	var $body = $('body');
	var $wcMsgMain;
	var msgTimeout;
	
	$body.on('click', '.quantity span.btn', productQuantityBtnHandler );

	$body.on( 'adding_to_cart', function ( e, $btn, data ) {
		$btn.closest( '.quantity-block' ).addClass( 'loading' );
		$btn.closest( '.product' ).addClass( 'loading' );
	});

	$body.on( 'added_to_cart', function ( e, fragments, cart_hash, $btn ) {
		var $product  = $btn.closest( '.product' );
		var $qtyBlock = $btn.closest( '.quantity-block' );
		var min       = +$qtyBlock.find( 'input.qty' ).attr( 'min' );
		var max       = +$qtyBlock.find( 'input.qty' ).attr( 'max' );

		if ( fragments.cart_contents_count )
			$('.js-theme-cart-link').html( '<span>'+ fragments.cart_contents_count +'</span>' );

		if ( undefined !== fragments.product_cart_qty ) {
			if ( max == +fragments.product_cart_qty ) 
				$qtyBlock.hide();
		};

		$qtyBlock.removeClass( 'loading' );

		wcShopMsg( createMsg( $btn, $product ) );

		handleQuatityBlock( min, '', $qtyBlock );

		setTimeout( function () {
			$product.removeClass( 'loading' );
		}, 500 );
	});

	// $body.on( 'updated_wc_div', function() {
		
	// });

	function createMsg( $btn, $product ) {
		var name = $product.find( '.product-title a' ).text();
		var qty  = 1;

		if ( $btn.hasClass( 'ajax_add_to_cart' ) ) 
			qty = $btn.data( 'quantity' );
		else {
			qty = getParameterByName( 'quantity', $btn.attr( 'href' ) );
		}

		return qty + ' × "' + $.trim( name ) + '"';
	}

	function wcShopMsg( text ) {
		if ( undefined === $wcMsgMain ) 
			$wcMsgMain = $('#js-woocommerce-message');

		var $msg = $wcMsgMain.find( '.woocommerce-message' );

		$msg.text( text );

		var h = $msg.outerHeight();

		$wcMsgMain.addClass( 'active' ).height( h );

		clearTimeout( msgTimeout );

		msgTimeout = setTimeout( function () {
			$wcMsgMain.removeClass( 'active' ).height( 0 );
		}, 3000 );
	}

	function productQuantityBtnHandler() {
		var $button = $(this);
		var $parent = $button.closest( '.quantity-block' );
		var $input  = $parent.find( 'input.qty' );

		var max = +$input.attr('max');
		var min = +$input.attr('min');
		var qty = +$input.val() || min;

		if ( $button.hasClass( 'count-up' ) ) {
			if ( max != 0 && qty >= max )
				return;

			++qty;
		} else {
			if ( min >= qty ) 
				return;

			--qty;
		}

		handleQuatityBlock( qty, max, $parent );
	}

	function handleQuatityBlock( qty, max, $parent ) {
		var $btnUp     = $parent.find( '.count-up' );
		var $btnDown   = $parent.find( '.count-down' );
		var $input     = $parent.find( 'input.qty' );
		var $addToCart = $parent.find( '.button' );

		if ( 1 == qty ) {
			$btnDown.addClass( 'disable' );
			$btnUp.removeClass( 'disable' );
		}
		else
		if ( +max == qty ) {
			$btnUp.addClass( 'disable' );
			$btnDown.removeClass( 'disable' );
		}
		else {
			if ( $btnDown.hasClass( 'disable' ) ) 
				$btnDown.removeClass( 'disable' );

			if ( $btnUp.hasClass( 'disable' ) )
				$btnUp.removeClass( 'disable' );
		}

		$input.val( qty ).trigger( 'change' );

		handleAddToCartBtn( $addToCart, qty );
	}

	function handleAddToCartBtn( $btn, qty ) {
		if ( !$btn.length ) 
			return;

		if ( !$btn.hasClass( 'js-link-add-to-cart' ) ) 
			return;

		if ( $btn.hasClass( 'ajax_add_to_cart' ) ) {
			$btn.data( 'quantity', qty );
		} else {
			var href = $btn.attr( 'href' );

			href = updateQueryStringParameter( href, 'quantity', qty );

			$btn.attr( 'href', href );
		}
	}
})();
$('.front-filters-block-content').each( function() {
	if ( 'undefined' == typeof themeData ) 
		return;

	var $main = $(this);
	var $selects = $main.find( 'select' );
	var $btn = $main.find( 'button' );

	$btn.on( 'click', function(e) {
		e.preventDefault();

		var url  = themeData.shopUrl;
		var data = [];

		$selects.each( function() {
			var val = $(this).val();

			if ( !val ) 
				return;

			data.push( val );
		});

		if ( data ) 
			url += '?filters='+ data.join( '|' );

		window.location.href = url;
	});
});
$('.js-section-tabs').themeTab();

$('.js-shop-sidebar').each( function() {
	var $sidebar = $(this);
	var $toggler = $('.js-mobile-filters-toggler');
	var $widgets = $sidebar.children( '.widget-area' );
	var $html    = $('html');
	var $reset   = $('.berocket_aapf_reset_button');

	var h = $widgets.height();

	$widgets.css( 'position', 'relative' );

	$toggler.click( function(e) {
		e.preventDefault();

		if ( !$sidebar.hasClass( 'active' ) ) 
			return openBlock();

		closeBlock();
	});

	$reset.click( function() {
		closeBlock();
	})

	function closeBlock() {
		$sidebar.removeClass( 'active' ).css( 'height', 0 );

		$html.animate( { scrollTop : 0 }, 600 );
	}

	function openBlock() {
		$sidebar.addClass( 'active' ).css( 'height', h + 'px' );
	}
});


$('.mobile-top-filters').each( function() {
	var $main = $(this);

	setDots();

	$('body').on( 'module_wc_ajax_filter.before_update', function() {
		setDots();
	});

	function setDots() {
		var w   = $main.width();
		var sum = 0;
		var cl  = 'active-dots';

		$main.find( '.widget_berocket_aapf' ).each( function() {
			sum += $(this).width();

			if ( sum > w ) {
				if ( !$main.hasClass( cl ) )
					$main.addClass( cl );
			} else {
				$main.removeClass( cl );
			}
		});
	}
});


$('body').on('click', '.select-shipping label', function(){
	$(this).closest('.select-shipping').toggleClass('active');
});









(function(){
	if ( 'undefined' == typeof wcAjaxFilters ) 
		return;

	setOrderHandler();
	setLocation();

	$('body').on( 'module_wc_ajax_filter.after_update', function() {
		if ( 'function' != typeof $.cookie ) 
			return;

		setLocation();
		setOrderHandler();

		$(window).scrollTop( 0 );
	});

	function setOrderHandler() {
		$('#js-wc-order-by-price').click(function(e){
			var $el = $(this);

			$el.removeClass( $el.data('prevOrder') ).addClass( $el.data('order') );

			$el.next().val( $el.data('order') ).trigger('change');
		});
	}

	/**
	 * Save current url after update posts
	 * Used in single product back link
	 */
	function setLocation() {
		updateCookie( wcAjaxFilters.cookieKey, window.location.href, 365 );
	}
})();
});
$(window).on("load", function() {

});

})(jQuery, ResponsiveBootstrapToolkit);