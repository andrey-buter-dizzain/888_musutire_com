// $.ajaxSetup({
//     complete: function(){
//         $('body').find('select.shipping_method').select2();
//     }
// });

$('body.woocommerce-cart').each(function(){
	$body = $(this);

	init();

	$(document).ajaxSuccess( init );

	function init() {
		updateCart();
		// initSelect();
		shippingHandler();
		clientsDropdown();
	}

	function updateCart(){
		var $form = $body.find('.cart-form');

		$form.find('input[type="text"]').change(function(){
			$form.find('.update_cart').removeAttr('disabled').trigger('click');
		})
	}

	function initSelect(){
		var $select = $body.find('select.shipping_method');

		if( 0 == $select.length )
			return;

		// $select.select2();
	}

	function shippingHandler() {
		$('#js-theme-cart-shipping').each( function () {
			var $main      = $(this);
			var $dropdown  = $main.find( '#shipping_list' );

			if ( 0 == $dropdown.length ) 
				return;

			var $hidden    = $main.find( '.shipping_method' );
			var $install   = $main.find( 'input[name=need_to_install]' );
			var $quantity  = $main.find( '.product-quantity' );
			var hiddenData = hiddenClousure( $hidden );

			$dropdown.on( 'change', function () {
				var val = $(this).val();

				hiddenData.set( 'shipping', val );
			});

			$install.on( 'change', function () {
				hiddenData.set( 'install', $(this).val() );
			});	
		});
	}

	function clientsDropdown() {
		$('#js-theme-cart-clients').each( function() {
			var $main      = $(this);
			var $hidden    = $('#js-theme-cart-shipping .shipping_method');
			var $dropdown  = $main.find( '#cart-clients-list' );
			var hiddenData = hiddenClousure( $hidden );

			$dropdown.on( 'change', function () {
				var val = $(this).val();

				hiddenData.set( 'client_id', val );
			});
		});
	}

	function hiddenClousure( $hidden ) {
		var data = {};

		parseData();

		var h = function () {
			return data;
		}

		h.set = function( key, value ) {
			data[ key ] = value;

			updateValue();
		}

		h.checkMaxInstall = function( max ) {
			if ( data.install <= max ) 
				return;

			data.install = max;

			updateValue();
		}

		function parseData() {
			data = JSON.parse( $hidden.val() );
		}

		function updateValue() {
			var value = JSON.stringify( data );

			$hidden.val( value ).trigger( 'change' );
		}

		return h;
	}

});



