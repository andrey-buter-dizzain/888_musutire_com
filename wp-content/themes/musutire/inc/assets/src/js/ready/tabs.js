/* ========================================================================
 * Bootstrap: tab.js v3.3.6
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

+function ($) {
	'use strict';

	// TAB CLASS DEFINITION
	// ====================

	var Tab = function ( parentNav ) {
		this.$parentNav = $(parentNav);
		this.$nav       = this.getNav();
		this.$tabs      = null;
		this.fromTop    = 0;
		// this.isMobile   = viewport.is( this.mobilePoint );
		this.isMobile   = false;
	};

	Tab.prototype.$window = $(window);

	Tab.prototype.mobilePoint = '<=xs';

	Tab.prototype.init = function () {
		var self = this;

		self.$nav.on( 'click', actionOnClick );

		// on mobile version all tabs are closed
		if ( !self.isMobile )
			self.$nav.eq( 0 ).trigger( 'click' );

		// self.onWindowResize();

		function actionOnClick( e ) {
			e.preventDefault();

			self.actionOnClick( $(this) );
		}
	};

	Tab.prototype.getNav = function() {
		var self = this;

		self.target = '';

		return self.$parentNav.find( '.js-tab-nav' ).filter( function() {
			var target = $(this).data('target').replace( /[0-9]+$/i, '' );

			if ( 0 == self.target.length )
				self.target = target;
			
			return ( self.target == target ); 
		});
	};

	Tab.prototype.onWindowResize = function() {
		var self = this;

		$(window).resize(
			viewport.changed( function() {
				self.isMobile = viewport.is( self.mobilePoint );

				if ( self.isMobile )
					return;

				if ( self.$tabs.filter( '.active' ).length ) 
					return;

				// on switch from mobile to desktop if has not active tab
				// activate first
				self.$nav.eq( 0 ).trigger( 'click' );
			})
		);
	};

	Tab.prototype.actionOnClick = function( $link ) {
		var self = this;

		var selector = $link.data('target');

		// if ( !selector ) {
		// 	selector = $this.attr('href')
		// 	selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
		// }

		var $tab = !self.$tabs ? $( selector ) : self.$tabs.filter( selector );

		var doActivate = self.doActivate( $tab );

		self.setTabs( $tab );

		self.saveFromTop();

		self.deactive();

		// allow to cloase tab on mobile version,
		// all tabs can be closed on mobile
		if ( doActivate )
			self.activate( $tab );

		self.setFromTop();
	};

	Tab.prototype.doActivate = function( $tab ) {
		var self = this;

		if ( !self.isMobile ) 
			return true;

		if ( !$tab.hasClass( 'active' ) )
			return true;

		return false;
	};

	Tab.prototype.activate = function( $tab ) {
		this.$nav
			.filter( '[data-target="#'+ $tab.attr( 'id' ) +'"]' )
			.addClass( 'active' )
			.trigger( 'themeTab.activate' );

		$tab
			.addClass( 'active' )
			.trigger( 'themeTab.activate' );
	};

	/**
	 * Deactivate tabs and tab navs
	 */
	Tab.prototype.deactive = function() {
		var self = this;

		$.each( [ '$nav', '$tabs' ], function() {
			self[ this ]
				.filter( '.active' )
				.removeClass( 'active' )
				.trigger( 'themeTab.deactive' );
		});
	};

	Tab.prototype.saveFromTop = function() {
		this.fromTop = this.$window.scrollTop();
	};

	Tab.prototype.setFromTop = function() {
		this.$window.scrollTop( this.fromTop );
	};

	Tab.prototype.setTabs = function( $tab ) {
		this.$tabs = $tab.parent().find( '> .js-tab-item' );
	};


	// TAB PLUGIN DEFINITION
	// =====================

	function Plugin(option) {
		return this.each(function () {
			var data = new Tab(this);

			data.init();
		});
	}

	$.fn.themeTab = Plugin;

}(jQuery);