+function ($) {
	'use strict';

	var FormSteps = function ( main ) {
		this.$main    = $(main);
		this.$steps   = this.$main.find( this.shortcode( 'step' ) );
		this.$actions = this.$main.find( this.shortcode( 'action' ) );
		this.$navs    = $( this.shortcode( 'nav' ) );

		this.currentStep    = 0;
		this.startStep      = this.currentStep;
		this.activatedSteps = new Array();

		this.init();
	};

	FormSteps.prototype = {
		cls: {
			active: 'active',
			enabled: 'enabled'
		},
		codes: {
			step:   'data-step',
			nav:    'data-step-nav',
			action: 'data-step-action',
		},
		shortcode: function( key, val ) {
			val = undefined !== val ? '='+ val : '';

			return '['+ this.codes[ key ] + val + ']';
		},
		init: function() {
			this.initActions();
			this.initNav();
			this.initEvents();
			this.showFirst();
		},
		showFirst: function() {
			var first = this.$steps.eq( 0 ).data( 'step' );

			if ( this.currentStep != first ) {
				for ( var i = this.currentStep; i < first; i++ ) {
					this.$navs
						.filter( this.shortcode( 'nav', i ) )
						.addClass( this.cls.enabled )
						.data( 'stepDisabled', 1 );

					this.currentStep = i;
					this.canActivateStep( i );
				};

				this.currentStep = first;
				this.startStep   = first;
			};

			this.show( this.currentStep );
		},
		initActions: function() {
			var self = this;

			this.$actions.on( 'click', function( e ) {
				e.preventDefault();

				var $btn    = $(this);
				var current = $btn.closest( self.shortcode( 'step' ) ).data( 'step' );

				if ( +$btn.data( 'stepDisabled' ) ) {
					self.$main.trigger( 'step-disabled-'+ current );
					return;
				}

				var bind = $btn.data( 'stepAction' );

				self.$main.trigger( 'step-'+ current );

				if ( 'external' == bind )
					return;

				self.show( ++current );
			});
		},
		initNav: function() {
			var  self = this;

			this.$navs.on( 'click', function(e) {
				e.preventDefault();

				var $link  = $(this);
				var number = $link.data( 'stepNav' );

				if ( +$link.data( 'stepDisabled' ) )
					return;

				if ( !self.isActiveStep( number ) )
					return;

				if ( !$link.hasClass( self.cls.enabled ) ) 
					return;

				if ( number > self.currentStep )
					return;

				self.activateByNav( number );
			});
		},
		activateByNav: function( number ) {
			this.activateNav( number );

			if ( this.startStep == number ) 
				return this.activate( number );

			this.$steps
				.filter( this.shortcode( 'step', --number ) )
				.find( this.shortcode( 'action' ) )
				.trigger( 'click' );
		},
		activateNav: function( number ) {
			var $current = this.$navs.filter( this.shortcode( 'nav', number ) );
			var active   = this.cls.active;

			if ( $current.hasClass( active ) )
				return;

			this.$navs
				.filter( '.' + active )
				.removeClass( active );

			$current.addClass( active );

			if ( $current.hasClass( this.cls.enabled ) )
				return;

			$current.addClass( this.cls.enabled );
		},
		isActiveStep: function( number ) {
			return ( -1 !== $.inArray( number, this.activatedSteps ) );
		},
		canActivateStep: function( number ) {
			if ( this.isActiveStep( number ) ) 
				return true;

			if ( number != this.activatedSteps.length ) 
				return false;

			this.activatedSteps.push( number );

			return true;
		},
		activate: function( number ) {
			if ( !this.canActivateStep( number ) )
				return;

			var $current = this.$steps.filter( this.shortcode( 'step', number ) );

			this.$steps.filter( '.'+ this.cls.active ).removeClass( this.cls.active );

			$current.addClass( this.cls.active );

			this.currentStep = number;
		},
		initEvents: function() {
			var self = this;

			self.$main.on( 'next', function() {
				self.next();
			});
			self.$main.on( 'prev', function() {
				self.prev();
			});
			self.$main.on( 'show', function( e, number ) {
				self.show( number );
			});
		},
		show: function( number ) {
			this.activateNav( number );
			this.activate( number );
		},
		prev: function() {
			if ( 0 == this.currentStep ) 
				return;

			this.show( --this.currentStep );
		},
		next: function() {
			this.show( ++this.currentStep );
		},
	}

	function Plugin( options ) {
		this.each( function() {
			new FormSteps( this );
		});
	}

	$.fn.themeFormStaps = Plugin;

}(jQuery);