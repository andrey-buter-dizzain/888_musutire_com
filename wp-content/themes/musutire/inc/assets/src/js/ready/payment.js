$('body.woocommerce-checkout').each( function() {
	init();

	$(document).ajaxSuccess( init );

	function init() {
		paymentDropdown();
	}
	
	function paymentDropdown() {
		$('#payment').each( function() {
			var $main      = $(this);
			var $dropdown  = $main.find( '#payment_methods_dropdown' );
			var $radio     = $main.find( '[name=payment_method]' );
			var $lis       = $main.find( '.wc_payment_method' );

			setActiveSelect( $dropdown.val() );

			/**
			 * Делается такой костыль, из-за того,
			 * что система оплаты square привязана к payments верстке.
			 * Поэтому приходится делать отдельный дропдаун 
			 * и на его изменение делать клик на радио кнопку
			 */
			$dropdown.on( 'change', function () {
				var val = $(this).val();

				setActiveSelect( val );
			});

			function setActiveSelect( val ) {
				var $el = $lis.filter( '.payment_method_'+ val );

				$el.siblings().removeClass( 'active' );

				$el.addClass( 'active' ).find( '#payment_method_'+ val ).trigger( 'click' );
			}
		});
	}
});