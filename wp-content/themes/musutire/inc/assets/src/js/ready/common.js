$('.js-section-tabs').themeTab();

$('.js-shop-sidebar').each( function() {
	var $sidebar = $(this);
	var $toggler = $('.js-mobile-filters-toggler');
	var $widgets = $sidebar.children( '.widget-area' );
	var $html    = $('html');
	var $reset   = $('.berocket_aapf_reset_button');

	var h = $widgets.height();

	$widgets.css( 'position', 'relative' );

	$toggler.click( function(e) {
		e.preventDefault();

		if ( !$sidebar.hasClass( 'active' ) ) 
			return openBlock();

		closeBlock();
	});

	$reset.click( function() {
		closeBlock();
	})

	function closeBlock() {
		$sidebar.removeClass( 'active' ).css( 'height', 0 );

		$html.animate( { scrollTop : 0 }, 600 );
	}

	function openBlock() {
		$sidebar.addClass( 'active' ).css( 'height', h + 'px' );
	}
});


$('.mobile-top-filters').each( function() {
	var $main = $(this);

	setDots();

	$('body').on( 'module_wc_ajax_filter.before_update', function() {
		setDots();
	});

	function setDots() {
		var w   = $main.width();
		var sum = 0;
		var cl  = 'active-dots';

		$main.find( '.widget_berocket_aapf' ).each( function() {
			sum += $(this).width();

			if ( sum > w ) {
				if ( !$main.hasClass( cl ) )
					$main.addClass( cl );
			} else {
				$main.removeClass( cl );
			}
		});
	}
});


$('body').on('click', '.select-shipping label', function(){
	$(this).closest('.select-shipping').toggleClass('active');
});








