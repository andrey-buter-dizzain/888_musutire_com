/**
 * Т.к. сложно создать checkout steps в woocommerce,
 * придумываем возможности как подстроить существующий функционал
 * под требования проекта.
 * В частности
 * 1) шаг - аунтификация - тут просто, если юзер залогинен то ее пропускаем, 
 * если нет, тогда при попытке залогиниться, страница перезагружается, 
 * т.к. для этих полей используется своя <form>
 * Если не гуд, то смотрим error messages. Если гуд, 
 * то показываем следующий шаг.
 * Надо учесть вариант с кэшем, что форма аунтификации может не исчезать, а просто скрываться.
 * 2) шаг - форма доставки. Тут сложнее. Тут весь checkout обернут в одну форму,
 * которая обрабатывается аяксом. Поэтому придуман хак такой:
 * в этот шаг добавлена <button>, которая по клику сабмитит форму.
 * По дефолту еще добавлен дополнительный input:hidden, 
 * который сообщает, на каком шаге сабмитится форма.
 * Т.к. форма обрабатывается аяксом, то глобально перехватываем событие аякса.
 * В нем смотрим полученную инфу в data.messages.
 * Если там находится кастомный спец ответ, тогда переходим на следующий шаг.
 * Если нет, тогда ничего не делаем. Отрабатывается стандартный функционал woocommerce
 * 3) шаг - окончание chcckout
 */
$('.js-checkout-steps').each( function() {
	var $main            = $(this);
	var $btnStepShipping = $main.find('.js-btn-step-shipping');
	var $form            = $main.find( 'form.woocommerce-checkout' );

	var $shippingMain = $btnStepShipping.closest( '[data-step]' );
	var shippingStep  = $shippingMain.data( 'step' );
	var $errors       = $form.find( '.woocommerce-error' );

	$main.themeFormStaps();

	$main.on( 'step-'+ shippingStep, createShippingHeaderMeta );
	// $main.on( 'step-disabled-'+ shippingStep, showFormErrors );

	validateForm();

	/**
	 * Shipping .section-header > .section-meta сообщение
	 */
	function createShippingHeaderMeta() {
		$shippingMain.find( '.section-meta' ).find( 'p' ).each( function() {
			var $p   = $(this);
			var keys = $p.data( 'pattern' ).split( ',' );
			var out  = [];
			var name = '';

			$.each( keys, function( i, key ) {
				var key   = $.trim( key );
				var value = $shippingMain.find( '#billing_' + key ).val();

				if ( !value ) 
					return;

				if ( 'first_name' == key ) {
					name = value;
					return;
				} else 
				if ( 'last_name' == key )
					value = name + ' ' + value;

				out.push( value );
			});

			$p.text( out.join( ', ' ) );
		});
	}


	function validateForm() {
		var isBtnClick = false;
		var $checkbox = $form.find( '#ship-to-different-address-checkbox' );

		var parts = {
			// !!! shipping и billing должны быть поменяны местами
			shipping: new ValidatePart( 'shipping', 'billing',  $form ),
			billing:  new ValidatePart( 'billing',  'shipping', $form ),
		}

		$form.on( 'validate-part', onValidatePart );

		$btnStepShipping.on( 'click', validateFormOnClick );

		$( document.body ).on( 'checkout_error', function() {
			isBtnClick = false;
		});

		doValidation( 'onload' );

		function onValidatePart() {
			if ( !isValid() )
				return;

			$btnStepShipping.removeClass( 'disabled' ).data( 'stepDisabled', 0 );

			if ( isBtnClick ) {
				// т.к. триггер blur field вызывается и при нажатии
				// кнопки place order,
				// и там могут быть свои шибки
				// то ошибки формы скрываем только при нажатии на кнопку process purchase
				hideErrors();

				// вызывается повторный клик кнопки,
				// т.к. если вставить инфу через crtl+p ,
				// то событие input onChange или onBlur обрабатывается
				// когда происходит событие вне поля или при написании текста клавиатурой.
				// Поэтому валидачия работала не корректно.
				$btnStepShipping
					.data( 'stepAction', '' )
					.trigger( 'click' )
					.data( 'stepAction', 'external' );
				isBtnClick = false;
			};
			// $btnStepShipping.addClass( 'disabled' ).data( 'stepDisabled', 1 );
		}

		function isValid() {
			var isValid = false;

			if ( !$checkbox.prop('checked') )
				isValid = parts.shipping.isValid();
			else
				isValid = ( parts.shipping.isValid() && parts.billing.isValid() );

			return isValid;
		}

		function validateFormOnClick() {
			isBtnClick = true;

			doValidation( 'btn-click' );
		}

		function showErrors() {
			var errors = getErrorsHtml();

			if ( !errors ) 
				return;

			// получается очень замудреная логика
			// ошибки могу генериться, например,
			// при нажатии кнопки place order,
			// т.е. когда в процессе checkout есть ошибки
			// поэтому предположительно сначала ищем
			// уже созданный блок ошибок
			if ( 0 == $errors.length )
				$errors = $form.find( '.woocommerce-error' );

			// если блок ошибко существует и он создан не тим скриптом,
			// то удаляем его, т.к. при валидации формы 
			// старые ошибки не нужны
			if ( 0 != $errors.length && !$errors.hasClass( 'js-errors' ) )
				hideErrors();

			// создаем свой блок ошибок
			if ( 0 == $errors.length ) {
				$errors = $('<ul class="woocommerce-error js-errors">');
				$form.prepend( $errors );
			}

			$errors.html( errors );
		}

		function isOnlyShipping() {
			return !$checkbox.prop('checked');
		}

		function doValidation( status ) {
			if ( isOnlyShipping() )
				parts.shipping.triggerValidation( status );
			else {
				parts.shipping.triggerValidation( status );
				parts.billing.triggerValidation( status );
			}

			// установлен таймаут для создания цепочки обратотки формы
			// сначала дефотлтный скрипт woocommerce обрабатывает формуб
			// потом обрабатывает checkout-validation.js
			// и потом вывод ошибок здесь
			setTimeout( function() {
				if ( isValid() ) {
					hideErrors();
				} else {
					showErrors();
				}
			}, 1);
		}
		function getErrors() {
			var errorList = {};

			$.each( parts, function( key, part ) {
				var errors = part.getErrors();
				var prefix = part.getErrorLabel( part.name );

				if ( isOnlyShipping() && 'billing' == part.name ) 
					return;

				if ( !errors ) 
					return;

				$.each( errors, function( key, label ) {
					errorList[ key ] = label.replace( 'PART', prefix );
				});
			});

			return errorList;
		}

		function getErrorsHtml() {
			var errors = getErrors();
			var html   = '';

			$.each( errors, function( key, label ) {
				html += '<li>'+ label +'</li>';
			});

			return html;
		}

		function hideErrors() {
			if ( 0 == $errors.length )
				$errors = $form.find( '.woocommerce-error' );

			if ( 0 == $errors.length ) 
				return;

			$errors.remove();
			$errors = [];
		}
	}

});