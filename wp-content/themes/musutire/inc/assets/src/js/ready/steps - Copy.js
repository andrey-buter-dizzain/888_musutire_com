+function ($) {
	'use strict';

	var FormSteps = function ( main ) {
		this.$main  = $(main);
		this.$steps = this.$main.find('[data-step]');
		this.$nav   = $('[data-step-nav]');
		this.currentStep = 1;
		this.activatedSteps = new Array( this.currentStep );

		this.init();
	};

	FormSteps.prototype = {
		cl: 'active',
		init: function() {
			// this.initEvents();
			this.initNav();
			this.show( this.currentStep );
		},
		prev: function() {
			if ( 0 == this.currentStep ) 
				return;

			this.show( --this.currentStep );
		},
		next: function() {
			this.show( ++this.currentStep );
		},
		show: function( number ) {
			this.getCurrentStepNav( number ).eq( 0 ).trigger( 'click' );
		},
		initEvents: function() {
			var self = this;

			self.$main.on( 'next', function() {
				self.next();
			});
			self.$main.on( 'prev', function() {
				self.prev();
			});
			self.$main.on( 'show', function( e, number ) {
				self.show( number );
			});
		},
		activateStep: function( number ) {
			var $current = this.$steps.filter( '[data-step='+ number +']' );

			if ( !this.isActivateStep( number ) )
				return false;

			this.$steps.filter( '.'+ this.cl ).removeClass( this.cl );

			$current.addClass( this.cl );

			this.currentStep = number;

			return true;
		},
		isActivateStep: function( number ) {
			if ( -1 !== $.inArray( number, this.activatedSteps ) ) 
				return true;

			if ( number != this.activatedSteps.length ) 
				return false;

			this.activatedSteps.push( number );

			return true;
		},
		initNav: function() {
			var  self = this;

			self.$nav.on( 'click', function(e) {
				e.preventDefault();

				var $link  = $(this);
				var number = $link.data( 'stepNav' );
				var bind   = $link.data( 'stepBind' );

				if ( !self.isActivateStep( number ) )
					return;

				if ( bind ) {
					$( bind ).trigger( 'click' );
				} else {
					self.activateStep( number );
				}

				self.$nav.filter( self.cl ).removeClass( self.cl );
				self.getCurrentStepNav( number ).addClass( self.cl );

				if ( $link.hasClass( 'enabled' ) )
					return;

				$link.addClass( 'enabled' );
			});
		},
		getCurrentStepNav: function( number ) {
			return this.$nav.filter( '[data-step-nav='+ number +']' );
		}
	}

	function Plugin(option) {
		this.each( function() {
			new FormSteps( this );
		});
	}

	$.fn.themeFormStaps = Plugin;

}(jQuery);