function ValidatePart( name, codeName, $form ) {
	this.name          = name;
	this.codeName      = codeName;
	this.validated     = [];
	this.$form         = $form;
	this.$part         = $form.find('.woocommerce-'+ codeName +'-fields');
	this.$fields       = this.$part.find( '.input-text, select, input:checkbox' );
	this.requiredCount = this.$part.find( '.validate-required' ).length;
	this.errors        = {};

	this.init();
}

ValidatePart.prototype = {
	// fields: '.input-text, select, input:checkbox',
	init: function() {
		var self = this;

		self.$fields.on( 'blur change', function() {
			var $field = $(this);

			// устновлен setTimeout с задержкой 0 нарочно,
			// чтоб функция выполнилась самой последней в стэке функций
			// По другому говоря, чтоб сначала отработались все эвенты в других местах,
			// например woocommerce, а потом обрабатывалась здесь
			setTimeout( function() {
				self.validateField( $field );
			}, 0);
		});

		// self.$part.on( 'trigger-validation', function() {
		// 	self.triggerValidation( 'click-button' );
		// });
	},
	validateField: function( $field ) {
		var $parent = $field.closest( '.form-row' );
		var id      = $field.attr( 'id' );

		if ( !$parent.hasClass( 'validate-required' ) ) 
			return;

		this.validatePhone( $field );

		if ( $parent.hasClass( 'woocommerce-invalid' ) ) {
			this.setError( id, $field );
			
			if ( -1 === $.inArray( id, this.validated ) ) 
				return;

			this.triggerStatus( 'invalid' );

			this.validated.splice( this.validated.indexOf( id ), 1 );

			return;
		};

		if ( -1 === $.inArray( id, this.validated ) ) 
			this.validated.push( id );
			
		this.removeError( id );

		if ( !this.isValid() )
			return;

		this.triggerStatus( 'valid' );
	},
	setError: function( id, $field ) {
		var val  = $field.val();
		var name = id.replace( /(billing|shipping)_/, '' );
		var error;
		var callback;

		if ( !this.isSetErrorLabels() ) 
			return;
		
		if ( !val ) {
			this.errors[ id ] = this.getErrorLabel( name );
			return;
		}

		if ( 'email' == name ) {
			callback = this.isEmail;
		} else 
		if ( 'phone' == name ) {
			callback = this.isPhone;
		} /*else 
		if ( 'postcode' == name ) {
			callback = this.isPostcode;
		}*/

		if ( !callback( val ) ) {
			name += '_not_valid';
			
			this.errors[ id ] = this.getErrorLabel( name );
		};
	},
	isSetErrorLabels: function() {
		return ( 'undefined' != typeof themeCheckoutErrors ) ;
	},
	getErrorLabel: function( name ) {
		if ( !this.isSetErrorLabels() ) 
			return;

		return themeCheckoutErrors[ name ];
	},
	getErrors: function() {
		return this.errors;
	},
	removeError: function( id ) {
		delete this.errors[ id ];
	},
	validatePhone: function( $field ) {
		var $parent = $field.closest( '.form-row' );
		var val     = $field.val();

		if ( !val ) 
			return;

		if ( !$parent.hasClass( 'validate-phone' ) )
			return;

		if ( ! this.isPhone( val ) ) {
			$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-email' );
		} else {
			$parent.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' ).addClass( 'woocommerce-validated' );
		}
	},
	triggerStatus: function( status ) {
		this.$form.trigger( 'validate-part', [ this.name, status ] );
	},
	triggerValidation: function( status ) {
		this.$fields.each( function() {
			var $field  = $(this);
			var $parent = $field.closest( '.form-row' );

			if ( !$parent.hasClass( 'validate-required' ) ) 
					return;

			if ( 'onload' == status && !$field.val() )
				return;

			$field.trigger( 'change' );
		});
	},
	isValid: function() {
		return ( this.requiredCount == this.validated.length ) ;
	},
	isPhone: function( value ) {
		/* http://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript/5059082#5059082 */
		var pattern = new RegExp( /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\.\/0-9]*$/g );

		// php woocommerce reg exp
		// var pattern = new RegExp( /[\s\#0-9_\-\+\(\)]*$/g );
		
		return pattern.test( value );
	},
	isEmail: function( value ) {
		/* https://stackoverflow.com/questions/2855865/jquery-validate-e-mail-address-regex */
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

		return pattern.test( value );
	}
}

