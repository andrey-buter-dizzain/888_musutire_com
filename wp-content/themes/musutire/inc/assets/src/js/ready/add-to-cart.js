;(function () {
	var $body = $('body');
	var $wcMsgMain;
	var msgTimeout;
	
	$body.on('click', '.quantity span.btn', productQuantityBtnHandler );

	$body.on( 'adding_to_cart', function ( e, $btn, data ) {
		$btn.closest( '.quantity-block' ).addClass( 'loading' );
		$btn.closest( '.product' ).addClass( 'loading' );
	});

	$body.on( 'added_to_cart', function ( e, fragments, cart_hash, $btn ) {
		var $product  = $btn.closest( '.product' );
		var $qtyBlock = $btn.closest( '.quantity-block' );
		var min       = +$qtyBlock.find( 'input.qty' ).attr( 'min' );
		var max       = +$qtyBlock.find( 'input.qty' ).attr( 'max' );

		if ( fragments.cart_contents_count )
			$('.js-theme-cart-link').html( '<span>'+ fragments.cart_contents_count +'</span>' );

		if ( undefined !== fragments.product_cart_qty ) {
			if ( max == +fragments.product_cart_qty ) 
				$qtyBlock.hide();
		};

		$qtyBlock.removeClass( 'loading' );

		wcShopMsg( createMsg( $btn, $product ) );

		handleQuatityBlock( min, '', $qtyBlock );

		setTimeout( function () {
			$product.removeClass( 'loading' );
		}, 500 );
	});

	// $body.on( 'updated_wc_div', function() {
		
	// });

	function createMsg( $btn, $product ) {
		var name = $product.find( '.product-title a' ).text();
		var qty  = 1;

		if ( $btn.hasClass( 'ajax_add_to_cart' ) ) 
			qty = $btn.data( 'quantity' );
		else {
			qty = getParameterByName( 'quantity', $btn.attr( 'href' ) );
		}

		return qty + ' × "' + $.trim( name ) + '"';
	}

	function wcShopMsg( text ) {
		if ( undefined === $wcMsgMain ) 
			$wcMsgMain = $('#js-woocommerce-message');

		var $msg = $wcMsgMain.find( '.woocommerce-message' );

		$msg.text( text );

		var h = $msg.outerHeight();

		$wcMsgMain.addClass( 'active' ).height( h );

		clearTimeout( msgTimeout );

		msgTimeout = setTimeout( function () {
			$wcMsgMain.removeClass( 'active' ).height( 0 );
		}, 3000 );
	}

	function productQuantityBtnHandler() {
		var $button = $(this);
		var $parent = $button.closest( '.quantity-block' );
		var $input  = $parent.find( 'input.qty' );

		var max = +$input.attr('max');
		var min = +$input.attr('min');
		var qty = +$input.val() || min;

		if ( $button.hasClass( 'count-up' ) ) {
			if ( max != 0 && qty >= max )
				return;

			++qty;
		} else {
			if ( min >= qty ) 
				return;

			--qty;
		}

		handleQuatityBlock( qty, max, $parent );
	}

	function handleQuatityBlock( qty, max, $parent ) {
		var $btnUp     = $parent.find( '.count-up' );
		var $btnDown   = $parent.find( '.count-down' );
		var $input     = $parent.find( 'input.qty' );
		var $addToCart = $parent.find( '.button' );

		if ( 1 == qty ) {
			$btnDown.addClass( 'disable' );
			$btnUp.removeClass( 'disable' );
		}
		else
		if ( +max == qty ) {
			$btnUp.addClass( 'disable' );
			$btnDown.removeClass( 'disable' );
		}
		else {
			if ( $btnDown.hasClass( 'disable' ) ) 
				$btnDown.removeClass( 'disable' );

			if ( $btnUp.hasClass( 'disable' ) )
				$btnUp.removeClass( 'disable' );
		}

		$input.val( qty ).trigger( 'change' );

		handleAddToCartBtn( $addToCart, qty );
	}

	function handleAddToCartBtn( $btn, qty ) {
		if ( !$btn.length ) 
			return;

		if ( !$btn.hasClass( 'js-link-add-to-cart' ) ) 
			return;

		if ( $btn.hasClass( 'ajax_add_to_cart' ) ) {
			$btn.data( 'quantity', qty );
		} else {
			var href = $btn.attr( 'href' );

			href = updateQueryStringParameter( href, 'quantity', qty );

			$btn.attr( 'href', href );
		}
	}
})();