/* mobile-menu */

$('#primary-navigation').map(function() {
	var cl = 'mobile-menu-active';
	var mobilePoint = '<md';
	
	var $primaryNav = $(this),
		$body       = $('body'),
		$mainBtn    = $('#mobile-nav-toggler');

	$mainBtn.on( 'click', function(e) {
		e.preventDefault();

		toggleClasses( !$body.hasClass( cl ) );
	});

	$primaryNav.swipe({
		swipe: function( event, direction ) {
			if ( 'right' != direction )
				return;

			toggleClasses(false);
		},
	});

	$(window).resize(
		viewport.changed( function() {
			if ( viewport.is( mobilePoint ) ) 
				return;

			if ( !$body.hasClass( cl ) ) 
				return;

			toggleClasses(false);
		})
	);

	function toggleClasses( toggle ) {
		toggle = ( 'undefined' == typeof toggle ) ? true : toggle;

		$(this).toggleClass( cl, toggle );
		$body.toggleClass( cl, toggle );		
		// $primaryNav.toggleClass( cl, toggle );
	}
});
