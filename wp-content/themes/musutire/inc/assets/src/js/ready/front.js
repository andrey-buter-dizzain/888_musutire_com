$('.front-filters-block-content').each( function() {
	if ( 'undefined' == typeof themeData ) 
		return;

	var $main = $(this);
	var $selects = $main.find( 'select' );
	var $btn = $main.find( 'button' );

	$btn.on( 'click', function(e) {
		e.preventDefault();

		var url  = themeData.shopUrl;
		var data = [];

		$selects.each( function() {
			var val = $(this).val();

			if ( !val ) 
				return;

			data.push( val );
		});

		if ( data ) 
			url += '?filters='+ data.join( '|' );

		window.location.href = url;
	});
});