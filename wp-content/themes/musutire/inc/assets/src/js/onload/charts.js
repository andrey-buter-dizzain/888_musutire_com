var ThemeChart = function( el, data ) {
	this.el = el;
	this.data = data;
}

ThemeChart.prototype.init = function() {
	var ctx = this.el.getContext("2d");
	// console.log(this.getArgs())

	new Chart( ctx ).Bar( this.getArgs(), {
		responsive : true,
		barValueSpacing : 20,
		scaleShowVerticalLines: false,
		tooltipTemplate: "<%= value %>",
		scaleSteps: 5,
		scaleStepWidth: 15,
		// scaleOverride: true
	});
}

ThemeChart.prototype.getArgs = function() {
	// this.data = { [year] => [value] }
	var labels = _.keys( this.data );
	var data = _.values( this.data );

	return {
		labels : labels,
		datasets : [
			{
				fillColor:       "#8fb6cc",
				strokeColor:     "#8fb6cc",
				highlightFill:   "#4185ac",
				highlightStroke: "#4185ac",
				data: data
			}
		],
	}
}


$('.canvas-chart').each( function( i ) {
	var chart = new ThemeChart( this, themeCanvasData[i] );

	chart.init();
});
