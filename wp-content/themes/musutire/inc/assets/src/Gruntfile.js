module.exports = function(grunt) {
	// include for ES6 Promises for postCss 
	var Promise = require('es6-promise').Promise;
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Maybe move to package.json
		include_files: {
			js: [
				'bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.min.js',
				'bower_components/jquery-touchswipe/jquery.touchSwipe.min.js',

				'js/meta/jquery-open.js',

				// preload
				'js/pre-load.js',
				'js/functions.js',

				'js/libs/bbq.js', // used by jquery.form.wizard.js
				'js/libs/jquery.form.wizard.js',
				
				// onReady
				'js/meta/jquery-open-ready.js',
				'js/ready/primary-navigation.js',
				'js/ready/mobile-window-size.js',
				'js/ready/steps.js',
				'js/ready/tabs.js',
				'js/ready/cart-shipping.js',
				'js/ready/payment.js',
				'js/ready/checkout-validation.js',
				'js/ready/checkout.js',
				'js/ready/add-to-cart.js',
				'js/ready/front.js',
				'js/ready/common.js',
				'<%= pkg.srcModules %>/woocommerce-ajax-filters/assets/common.js',

				'js/meta/jquery-close-ready.js',

				// onLoad
				'js/meta/jquery-open-onload.js',
				'js/onload/common.js',
				'js/meta/jquery-close-onload.js',

				// load after
				'js/after-load.js',

				// cloase jquery
				'js/meta/jquery-close.js',
			],
			scss: [
				'scss/screen.scss'
			],
			js_admin: [
				'bower_components/angular/angular.min.js',
				'<%= pkg.srcModules %>/repairshopr/assets/js/common.js'
			]
		},

		sass: {
			dist: {
				options: {
					// noCache: true
				},
				files: {
					'<%= pkg.build.css %>/styles.css': '<%= include_files.scss %>',
				}
			},
			dist_compressed: {
				options: {
					// noCache: true,
					style: 'compressed'
				},
				files: {
					'<%= pkg.build.css %>/styles.min.css': '<%= pkg.build.css %>/styles.css',
				}
			}
		},

		postcss: {
			options: {
				map: true,
				// We need to `freeze` browsers versions for testing purposes.
				// browsers: ['last 2 versions', 'opera 12', 'ie 8', 'ie 9']
				processors: [
					require('pixrem')(),
					require('postcss-sorting')({ /* options */ }),
					require('autoprefixer')({browsers: ['last 2 versions', 'opera 12', 'ie 8', 'ie 9']}),
				]
			},
			// prefix the specified file
			// single_file: {
			// 	src: '<%= pkg.build.css %>/styles.css',
			// 	dest: '<%= pkg.build.css %>/styles.css'
			// }
			// multiple_files: {
			// 	expand: true,
			// 	flatten: true,
			// 	src:  '<%= pkg.build.css %>/*.css',
			// 	dest: '<%= pkg.build.css %>/'
			// },
			dist: {
				src: '../css/*.css'
			}
		},

		concat: {
			dist: {
				files: {
					'<%= pkg.build.js %>/common.js': '<%= include_files.js %>',
					'<%= pkg.buildAdmin.js %>/common.js': '<%= include_files.js_admin %>'
				}
				// files: {
				// 	'<%= pkg.build.js %>/common.es6.js': '<%= include_files.js %>',
				// 	'<%= pkg.buildAdmin.js %>/common.es6.js': '<%= include_files.js_admin %>'
				// }
			}
		},

		babel: {
			options: {
				sourceMap: true,
				presets: ['es2015']
			},
			dist: {
				files: {
					'<%= pkg.build.js %>/common.js': '<%= pkg.build.js %>/common.es6.js',
					'<%= pkg.buildAdmin.js %>/common.js': '<%= pkg.buildAdmin.js %>/common.es6.js'
				}
			}
		},

		uglify: {
			my_target: {
				options: {
					mangle: false
				},
				files: {
					'<%= pkg.build.js %>/common.min.js': '<%= pkg.build.js %>/common.js',
					'<%= pkg.buildAdmin.js %>/common.min.js': '<%= pkg.buildAdmin.js %>/common.js'
				}
			}
		},

		responsive_images: {
			options: {
				engine: 'im',
				separator: '',
				units: {
					percentage: ''
				}
			},
			myTask: {
				options: {
					sizes: [
						{
							width: '50%',
							height: '50%',
							name: '@1x',
						}
					],
					// engine: 'im'
				},
				files: [{
					expand: true,
					src: ['**.{jpg,gif,png}'],
					cwd: 'images/icons_2x/',
					dest: 'images/_temp/icons_1x'
				}]
			}
		},

		sprite:{
			// common: {
			// 	src: 'images/common/*.png',
			// 	dest: '../images/sprites/common.png',
			// 	imgPath: '../images/sprites/common.png',
			// 	destCss: 'scss/sprites/common.scss',
			// 	cssFormat: 'scss_maps',
			// 	algorithm: 'binary-tree',
			// 	padding: 2,
			// 	cssOpts: {
			// 		functions: false
			// 	},
			// 	cssSpritesheetName: 'common',
			// },
			icons_1x: {
				src: 'images/_temp/icons_1x/*.png',
				dest: '../images/sprites/icons_1x.png',
				destCss: 'scss/sprites/icons_1x.scss',
				cssFormat: 'scss_maps',
				algorithm: 'binary-tree',
				padding: 2,
				cssOpts: {
					functions: false
				},
				cssSpritesheetName: 'icons-1x'
			},
			icons_2x: {
				src: 'images/icons_2x/*.png',
				dest: '../images/sprites/icons_2x.png',
				destCss: 'scss/sprites/icons_2x.scss',
				cssFormat: 'scss_maps',
				algorithm: 'binary-tree',
				padding: 2,
				cssOpts: {
					functions: false
				},
				cssSpritesheetName: 'icons-2x'
			},
		},

		clean: [
			'images/_temp'
		], //removes old data
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'images/svg',
					src: ['*.svg'],
					dest: 'images/_temp/svg/min',
					ext: '.svg'
				}]
			}
		},
		svgcss: {
			toCrlf: {
				options: {
					csstemplate: 'images/svg-core/scss-map-template2.hbs',
					previewhtml: null,
				},
				files: {
					'scss/sprites/svg-to-css.scss': ['images/_temp/svg/min/*.svg']
				}
			}
		},
		
		watch: {
			grunt: { files: ['Gruntfile.js'] },
			sass: {
				files: ['scss/**/*.scss'],
				tasks: ['sass-handle', 'notify:watch'],
				// options: {
				// 	livereload: true,
				// 	// livereload: {
				// 	// 	port: 35729
				// 	// }
				// }
			},
			js: {
				files: [
					'js/**/*.js',
					'<%= pkg.srcModules %>/woocommerce-ajax-filters/assets/*.js',					
					'<%= pkg.srcModules %>/repairshopr/assets/js/**/*.js',
				],
				tasks: ['js-handle', 'notify:def'],
				options: {
					livereload: true,
				}
			},
			png: {
				files: ['images/**/*.png'],
				tasks: ['clean:images','responsive_images','sprite'],
				options: {
					livereload: true,
				}
			},
			livereload: {
				options: { 
					livereload: true 
				},
				files: ['<%= pkg.build.css %>/*.css'],
			},
		},

		notify: {
			def: {
				options: {
					message: 'Task Complete', //required
				}
			},
			watch: {
				options: {
					title: 'Task Complete',  // optional
					message: 'SASS and Uglify finished running', //required
				}
			},
		},

	});

	// Load in `grunt-spritesmith`
	// grunt.loadNpmTasks('grunt-spritesmith');
	// grunt.loadNpmTasks('grunt-responsive-images');

	// grunt.loadNpmTasks('grunt-postcss');

	// grunt.loadNpmTasks('grunt-contrib-sass');
	// grunt.loadNpmTasks('grunt-contrib-watch');
	// grunt.loadNpmTasks('grunt-contrib-concat');
	// grunt.loadNpmTasks('grunt-contrib-uglify');
	// grunt.loadNpmTasks('grunt-contrib-clean');

	// grunt.loadNpmTasks('grunt-svgmin');
	// grunt.loadNpmTasks('grunt-svg-css');

	// grunt.loadNpmTasks('grunt-notify');

	require("load-grunt-tasks")(grunt);

	grunt.registerTask('sass-handle', ['sass', 'postcss'/*, 'cssmin'*/]);
	grunt.registerTask('js-handle', ['concat', /*'babel',*/ 'uglify']);

	grunt.registerTask('png', ['clean:images','responsive_images','sprite', 'notify:def']);
	grunt.registerTask('svg', ['clean', 'svgmin', 'svgcss', 'notify:def']);

	grunt.registerTask('build', ['sass-handle', 'js-handle', 'notify:def']);

};