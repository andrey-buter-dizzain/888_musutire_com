<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area wrapper full-width">

		<?php get_theme_part( 'content', '404' ) ?>

	</div>
	
<?php get_footer(); ?>
