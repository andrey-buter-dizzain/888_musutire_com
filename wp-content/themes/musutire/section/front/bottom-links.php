<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$links = get_post_meta( get_the_ID(), 'front-bottom-links', true );

if ( !$links )
	return;

?>
<div class="front-bottom-links">
	<div class="wrapper"><?php
		foreach ( $links as $link ): 
			?><a class="link-item" href="<?php echo get_permalink( $link['page'] ) ?>">
				<span class="icon <?php echo $link['icon'] ?>"></span>
				<span class="title">
					<?php echo $link['title'] ?> &rsaquo;
				</span>
			</a><?php 
		endforeach;
	 ?></div>
</div>