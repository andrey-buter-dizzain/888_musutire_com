<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$social = get_theme_option( 'social-links' );

if ( !$social )
	return;

?>
<div class="front-social-block">
	<?php foreach ( $social as $item ): ?>
		<a class="social-item <?php echo $item['icon'] ?>" target="_blank" href="<?php echo $item['href'] ?>" title="<?php echo $item['title'] ?>"></a>
	<?php endforeach ?>
</div>