<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_post_meta( get_the_ID(), 'front-dropdowns', true );

if ( !$data )
	return;

wp_localize_script( 'common-js', 'themeData', array(
	'shopUrl' => get_permalink( woocommerce_get_page_id( 'shop' ) )
) );

$title = get_post_meta( get_the_ID(), 'front-dropdowns-title', true );

?>
<div class="front-filters-block">
	<?php if ( $title ): ?>
		<div class="block-title"><?php echo $title ?></div>
	<?php endif ?>

	<div class="front-filters-block-content">
		<div class="dropdown-filters count-<?php echo count( $data ) ?>">
			<div class="dropdown-filters-inn">
				<?php foreach ( $data as $item ): 
					$terms = get_terms( $item['taxonomy'], array(
						'hide_empty' => true,
					) );

					if ( is_wp_error( $terms ) OR !is_array( $terms ) )
						continue;
				?>
					<div class="filter-item">
						<select name="<?php echo $item['taxonomy'] ?>" id="<?php echo $item['taxonomy'] ?>">
							<option value=""><?php echo $item['title'] ?></option>
							<?php foreach ( $terms as $term ): ?>
								<option value="<?php echo "{$item['taxonomy']}[{$term->term_id}]" ?>"><?php echo $term->name ?></option>
							<?php endforeach ?>
						</select>
					</div>
				<?php endforeach ?>
			</div>
		</div>

		<button><?php _e( 'Find tires', 'musutire' ) ?></button>
	</div>
</div>