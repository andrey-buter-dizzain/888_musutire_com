<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$filters = module_wc_filters_get_terms();

if ( !$filters )
	return;

?>

<div id="shop-top-filters" class="single-view">
	<div class="wrapper shop-top-filters-sidebar">
		<?php foreach ( $filters as $terms ) : ?>
			<div class="berocket_aapf_widget">
				<?php foreach ( $terms as $term ): ?>
					<span>
						<label for="" class="berocket_checked">
							<?php echo $term->name; ?>
						</label>
					</span>
				<?php endforeach ?>
			</div>
		<?php endforeach ?>
	</div>
</div>