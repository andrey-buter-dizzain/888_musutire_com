<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

$class = is_account_page() ? 'active' : '';

?>
<a href="<?php echo wc_get_page_permalink( 'myaccount' ) ?>" class="login-button <?php echo $class ?>">
	<?php if ( is_user_logged_in() ): ?>
		<span><?php echo wp_get_current_user()->data->display_name ?></span>
	<?php endif ?>
</a>