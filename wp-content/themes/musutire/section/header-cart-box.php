<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

if ( !function_exists( 'is_cart' ) )
	return;

$active = ( is_cart() || is_checkout() ) ? 'active' : '';

$count = WC()->cart->get_cart_contents_count();
	
?>
<div class="cart-box-header <?php echo $active; ?>">
	<a class="js-theme-cart-link" href="<?php echo wc_get_cart_url(); ?>">
		<?php if ( $count ) : ?>
			<span><?php echo $count; ?></span>
		<?php endif; ?>
	</a>
</div>