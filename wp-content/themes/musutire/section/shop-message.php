<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( !function_exists( 'is_shop' ) )
	return;

if ( !is_shop() )
	return;

$count = WC()->cart->get_cart_contents_count();

?>
<div id="js-woocommerce-message" class="woocommerce-message-fixed">
	<div class="wrapper">
		<div class="woocommerce-message">

		</div>
		<a class="js-theme-cart-link cart-link" href="<?php echo wc_get_cart_url(); ?>">
			<?php if ( $count ) : ?>
				<span><?php echo $count; ?></span>
			<?php endif; ?>
		</a>
	</div>
</div>